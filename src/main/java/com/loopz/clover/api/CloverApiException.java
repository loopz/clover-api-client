package com.loopz.clover.api;

public class CloverApiException extends Exception {

    private final int statusCode;
    private final CloverErrorResponse errorResponse;

    public CloverApiException(int statusCode, CloverErrorResponse errorResponse) {
        super(errorResponse.getMessage());
        this.statusCode = statusCode;
        this.errorResponse = errorResponse;
    }

    public CloverApiException(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
        this.errorResponse = null;
    }

    public CloverApiException(int statusCode, Throwable cause) {
        super(cause);
        this.statusCode = statusCode;
        this.errorResponse = null;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public CloverErrorResponse getErrorResponse() {
        return errorResponse;
    }

    @Override
    public String toString() {
        return "CloverApiException{" +
                "statusCode=" + statusCode +
                ", errorResponse=" + errorResponse +
                '}';
    }
}
