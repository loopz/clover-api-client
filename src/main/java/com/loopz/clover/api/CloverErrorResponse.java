package com.loopz.clover.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CloverErrorResponse.Builder.class)
public class CloverErrorResponse {

    private final String message;
    private final CloverCommerceErrorResponse commerceErrorResponse;

    private CloverErrorResponse(Builder builder) {
        message = builder.message;
        commerceErrorResponse = builder.commerceErrorResponse;
    }


    public static CloverErrorResponse unknown() {
        return CloverErrorResponse.builder().message("Unknown error").build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getMessage() {
        return message;
    }

    public CloverCommerceErrorResponse getCommerceErrorResponse() {
        return commerceErrorResponse;
    }

    @Override
    public String toString() {
        return "CloverErrorResponse{" +
                "message='" + message + '\'' +
                ", commerceErrorResponse=" + commerceErrorResponse +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String message;
        private CloverCommerceErrorResponse commerceErrorResponse;

        private Builder() {
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        @JsonProperty("error")
        public Builder commerceErrorResponse(CloverCommerceErrorResponse val) {
            commerceErrorResponse = val;
            return this;
        }

        public CloverErrorResponse build() {
            return new CloverErrorResponse(this);
        }
    }
}
