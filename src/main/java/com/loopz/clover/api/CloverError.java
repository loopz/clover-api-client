package com.loopz.clover.api;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(builder = CloverError.Builder.class)
public class CloverError {

    private final String error;
    private final String description;
    private final List<String> fields;

    private CloverError(Builder builder) {
        error = builder.error;
        description = builder.description;
        fields = builder.fields;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getError() {
        return error;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "OmnivoreError[" +
                "error='" + error + '\'' +
                ", description='" + description + '\'' +
                ", fields=" + fields +
                ']';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String error;
        private String description;
        private List<String> fields = new ArrayList<>();

        private Builder() {
        }

        public Builder error(String error) {
            this.error = error;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder fields(List<String> fields) {
            this.fields = fields;
            return this;
        }

        public CloverError build() {
            return new CloverError(this);
        }
    }
}
