package com.loopz.clover.api;

public class CloverApiUrls {

    public static final String SANDBOX = "https://apisandbox.dev.clover.com/";
    public static final String PRODUCTION_US = "https://api.clover.com/";
    public static final String PRODUCTION_EU = "https://api.eu.clover.com/";

    private CloverApiUrls(){}
}
