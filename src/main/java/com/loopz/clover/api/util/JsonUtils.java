package com.loopz.clover.api.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import java.io.StringWriter;

public class JsonUtils {

    private static ObjectMapper OBJECT_MAPPER;

    public static ObjectMapper getObjectMapper() {
        if (OBJECT_MAPPER == null) {
            OBJECT_MAPPER = new ObjectMapper();
            OBJECT_MAPPER.registerModule(new JodaModule());
            OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            OBJECT_MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            OBJECT_MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
            OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }
        return OBJECT_MAPPER;
    }

    public static <T> String toJSON(T pojo) {
        try {
            if(pojo.getClass().equals(String.class)){
                return (String)pojo;
            }
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, pojo);
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJSON(String json, Class<T> pojoClass) {
        try {
            return getObjectMapper().readValue(json, pojoClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJSON(JsonNode jsonNode, Class<T> pojoClass) {
        try {
            return getObjectMapper().treeToValue(jsonNode, pojoClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T fromJSON(JsonNode jsonNode, TypeReference<T> pojoClassReference) {
        try {
            return getObjectMapper().readerFor(pojoClassReference).readValue(jsonNode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
