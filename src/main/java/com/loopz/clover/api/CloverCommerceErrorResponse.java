package com.loopz.clover.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CloverCommerceErrorResponse.Builder.class)
public class CloverCommerceErrorResponse {

    private final String message;
    private final String code;
    private final String chargeId;
    private final String declineCode;

    private CloverCommerceErrorResponse(Builder builder) {
        message = builder.message;
        code = builder.code;
        chargeId = builder.chargeId;
        declineCode = builder.declineCode;
    }

    public static CloverCommerceErrorResponse unknown() {
        return CloverCommerceErrorResponse.builder().message("Unknown error").code("unknown_error").build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getMessage() {
        return message;
    }

	public String getCode() {
		return code;
	}

	public String getChargeId() {
		return chargeId;
	}

	public String getDeclineCode() {
		return declineCode;
	}

	@Override
	public String toString() {
		return "CloverErrorResponse{" +
				"message='" + message + '\'' +
				", code='" + code + '\'' +
				", chargeId='" + chargeId + '\'' +
				", declineCode='" + declineCode + '\'' +
				'}';
	}

	@JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String message;
        private String code;
        private String chargeId;
        private String declineCode;

        private Builder() {
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Builder code(String val) {
            code = val;
            return this;
        }

        @JsonProperty("charge")
        public Builder chargeId(String val) {
            chargeId = val;
            return this;
        }

        public Builder declineCode(String val) {
            declineCode = val;
            return this;
        }

        public CloverCommerceErrorResponse build() {
            return new CloverCommerceErrorResponse(this);
        }
    }
}
