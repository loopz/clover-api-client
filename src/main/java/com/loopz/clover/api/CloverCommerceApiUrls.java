package com.loopz.clover.api;

public class CloverCommerceApiUrls {

    public static final String SANDBOX = "https://scl-sandbox.dev.clover.com/";
    public static final String PRODUCTION_US = "https://scl.clover.com/";
    public static final String PRODUCTION_EU = "https://scl.clover.com/";

    private CloverCommerceApiUrls(){}
}
