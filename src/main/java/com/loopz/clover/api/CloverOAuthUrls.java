package com.loopz.clover.api;

public class CloverOAuthUrls {

    public static final String SANDBOX = "https://sandbox.dev.clover.com/oauth";
    public static final String PRODUCTION_US = "https://www.clover.com/oauth";
    public static final String PRODUCTION_EU = "https://eu.clover.com/oauth";

    private CloverOAuthUrls(){}
}
