package com.loopz.clover.api;

public class CloverTokenApiUrls {

    public static final String SANDBOX = "https://token-sandbox.dev.clover.com/";
    public static final String PRODUCTION_US = "https://token.clover.com/";
    public static final String PRODUCTION_EU = "https://token.clover.com/";

    private CloverTokenApiUrls(){}
}
