package com.loopz.clover.api;

import com.loopz.clover.api.client.ApiTokenAuthenticationStrategy;
import com.loopz.clover.api.client.CloverApiClient;

public class CloverApi {

    private static String API_TOKEN = null;
    private static String APP_ID = null;
    private static String METERED_ID = null;
    private static String ROOT_URL = CloverApiUrls.SANDBOX;

    public static String getApiToken() {
        return API_TOKEN;
    }

    public static void setApiToken(String apiToken) {
        API_TOKEN = apiToken;
    }

    public static String getRootUrl() {
        return ROOT_URL;
    }

    public static void setRootUrl(String rootUrl) {
        ROOT_URL = rootUrl;
    }

    public static String getAppId() {
        return APP_ID;
    }

    public static void setAppId(String appId) {
        APP_ID = appId;
    }

    public static String getMeteredId() {
        return METERED_ID;
    }

    public static void setMeteredId(String meteredId) {
        METERED_ID = meteredId;
    }

    public static CloverApiClient client() {
        return new CloverApiClient(ROOT_URL, getAuthStrategy());
    }

    private static ApiTokenAuthenticationStrategy getAuthStrategy() {
        return new ApiTokenAuthenticationStrategy(CloverApi.getApiToken());
    }

}
