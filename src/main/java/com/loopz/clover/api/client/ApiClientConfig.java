package com.loopz.clover.api.client;

public class ApiClientConfig {

    private final int connectTimeout;
    private final int writeTimeout;
    private final int readTimeout;

    private ApiClientConfig(Builder builder) {
        connectTimeout = builder.connectTimeout;
        writeTimeout = builder.writeTimeout;
        readTimeout = builder.readTimeout;
    }

    public static ApiClientConfig getDefaultConfig() {
        return new Builder().build();
    }

    public static ApiClientConfig.Builder getDefaultConfigBuilder() {
        return new Builder();
    }

    public static Builder builder() {
        return new Builder();
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public int getWriteTimeout() {
        return writeTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public static final class Builder {

        private int connectTimeout = 10000;
        private int writeTimeout = 15000;
        private int readTimeout = 15000;

        public Builder() {
        }

        public Builder connectTimeout(int val) {
            connectTimeout = val;
            return this;
        }

        public Builder writeTimeout(int val) {
            writeTimeout = val;
            return this;
        }

        public Builder readTimeout(int val) {
            readTimeout = val;
            return this;
        }

        public ApiClientConfig build() {
            return new ApiClientConfig(this);
        }
    }
}
