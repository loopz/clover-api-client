package com.loopz.clover.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Applies the Clover API Token to the given request.
 */
public class ApiTokenAuthenticationStrategy implements AuthenticationStrategy {

    private final String apiToken;

    public ApiTokenAuthenticationStrategy(String apiToken) {
        if(apiToken == null || apiToken.isEmpty()) {
            throw new IllegalArgumentException("API Token cannot be empty");
        }
        this.apiToken = apiToken;
    }

    @Override
    public void applyCredentials(OkHttpClient.Builder clientBuilder, Request.Builder requestBuilder) {
        requestBuilder.addHeader("Authorization", "Bearer " + apiToken);
    }

    @Override
    public String getAuthenticatingUserName() {
        return "api-token";
    }
}
