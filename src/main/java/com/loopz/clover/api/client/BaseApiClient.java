package com.loopz.clover.api.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopz.clover.api.CloverApiException;
import com.loopz.clover.api.CloverErrorResponse;
import com.loopz.clover.api.util.JsonUtils;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

public abstract class BaseApiClient {

    protected LogWrapper logWrapper;

    private ApiClientConfig clientConfig = ApiClientConfig.getDefaultConfig();
    private ApiClientRequestInvoker clientRequestInvoker = new ApiClientRequestInvoker();

    protected static final int STATUS_OK = 200;
    protected static final int STATUS_NO_CONTENT = 204;
    private static final int RETRY_BACKOFF_SEED = 1000;
    private static final int MAX_ATTEMPTS = 8;

    private static final MediaType APPLICATION_JSON = MediaType.get("application/json");
    private static final MediaType TEXT_PLAIN = MediaType.get("text/plain");
    private static final MediaType TEXT_HTML = MediaType.get("text/html");

    protected String rootPath;
    private AuthenticationStrategy authStrategy;
    private IdempotencyKeyStrategy idempotencyKeyStrategy;
    private ObjectMapper objectMapper;

    private static OkHttpClient httpClient;

    public BaseApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        if (rootPath == null) {
            throw new IllegalArgumentException("rootPath cannot be null");
        }
        setAuthStrategy(authStrategy);
        this.rootPath = rootPath;
        this.authStrategy = authStrategy;
        this.idempotencyKeyStrategy = new NoopIdempotencyKeyStrategy();
        setLogger(new SLF4JWrapper(getClass().getSimpleName()));
    }

    public AuthenticationStrategy getAuthStrategy() {
        return authStrategy;
    }

    public void setAuthStrategy(AuthenticationStrategy authStrategy) {
        if (authStrategy == null) {
            throw new IllegalArgumentException("AuthenticationStrategy cannot be null");
        }
        this.authStrategy = authStrategy;
    }

    public void setIdempotencyKeyStrategy(IdempotencyKeyStrategy idempotencyKeyStrategy) {
        this.idempotencyKeyStrategy = idempotencyKeyStrategy;
    }

    public ApiClientConfig getClientConfig() {
        return clientConfig;
    }

    public void setClientConfig(ApiClientConfig clientConfig) {
        this.clientConfig = clientConfig;
    }

    public ApiClientRequestInvoker getClientRequestInvoker() {
        return clientRequestInvoker;
    }

    public void setClientRequestInvoker(ApiClientRequestInvoker clientRequestInvoker) {
        this.clientRequestInvoker = clientRequestInvoker;
    }

    public void execute(Request.Builder requestBuilder) throws CloverApiException {
        execute(requestBuilder, null);
    }

    public <T> T execute(Request.Builder requestBuilder, Class<T> targetType) throws CloverApiException {
        T responseObject = null;

        OkHttpClient.Builder clientBuilder = getHttpClientBuilder();

        authStrategy.applyCredentials(clientBuilder, requestBuilder);
        idempotencyKeyStrategy.applyKey(requestBuilder);

        Response response = null;
        OkHttpClient httpClient = clientBuilder.build();
        try {
            response = newCall(httpClient, requestBuilder.build());
        } catch (IOException e) {
            logWrapper.error("An exception occurred trying to call an API endpoint.", e);
            throw new CloverApiException(500, e);
        }
        try {
            int statusCode = response.code();
            switch (statusCode) {
                case STATUS_OK:
                    responseObject = readResponse(response, targetType);
                    break;
                case STATUS_NO_CONTENT:
                    break;
                default:
                    throw new CloverApiException(statusCode, readCloverErrorResponse(response));
            }
            return responseObject;
        } finally {
            try {
                response.close();
            } catch(Exception e) {};
        }
    }

    private OkHttpClient.Builder getHttpClientBuilder() {
        if(httpClient == null) {
            httpClient = new OkHttpClient.Builder()
                    .addInterceptor(getLoggingInterceptor())
                    .connectTimeout(clientConfig.getConnectTimeout(), TimeUnit.MILLISECONDS)
                    .readTimeout(clientConfig.getReadTimeout(), TimeUnit.MILLISECONDS)
                    .writeTimeout(clientConfig.getWriteTimeout(), TimeUnit.MILLISECONDS)
                    .retryOnConnectionFailure(true)
                    .build();
        }

        return httpClient.newBuilder();
    }

    private Response newCall(OkHttpClient httpClient, Request request) throws CloverApiException, IOException {
        int attempts = 1;
        do {
            try {
                Response response = clientRequestInvoker.invoke(httpClient, request);
                if (isRetryableErrorResponse(response)) {
                    if (attempts < MAX_ATTEMPTS) {
                        sleep(response, attempts);
                    }
                    attempts++;
                } else {
                    return response;
                }
            } catch (SocketTimeoutException e) {
                if (attempts < MAX_ATTEMPTS) {
                    logWrapper.error("A timeout occurred trying to call an API endpoint. Attempt " + attempts, e);
                    sleep(null, attempts);
                }
                attempts++;
            } catch (IOException e) {
                throw e;
            }
        } while (attempts <= MAX_ATTEMPTS);
        throw new CloverApiException(500, "Too many attempts.");
    }

    protected boolean isRetryableErrorResponse(Response response) {
        // If too many requests error is received, retry
        if (response.code() == 425 || response.code() == 429) {
            return true;
        }
        return false;
    }

    private void sleep(Response response, int attempts) {
        long timeToSleep = Math.round(RETRY_BACKOFF_SEED * Math.pow(2, attempts - 1));
        // Check if a retry after header is present in the response
        if(response != null && response.header("retry-after") != null) {
            try {
                timeToSleep = 1000 * Integer.parseInt(response.header("retry-after"));
            } catch(Exception e) {

            }
        }
        if (timeToSleep > 30000) {
            timeToSleep = 30000;
        }

        try {
            logWrapper.info("Sleeping for " + timeToSleep + " msec before attempting request again.");
            Thread.sleep(timeToSleep);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }


    private CloverErrorResponse readCloverErrorResponse(Response response) {
        String content = null;
        try {
            content = response.body().string();
            return fromJSON(content, CloverErrorResponse.class);
        } catch (Exception e) {
            if (content != null && !content.isEmpty()) {
                return CloverErrorResponse.builder().message(content).build();
            }
            CloverErrorResponse cloverErrorResponse = CloverErrorResponse.unknown();
            return cloverErrorResponse;
        }
    }

    private <T> T readResponse(Response response, Class<T> targetType) throws CloverApiException {
        T responseObject = null;
        try {
            String content = response.body().string();
            if (targetType != null) {
                if (isContentTypeJson(response)) {
                    responseObject = fromJSON(content, targetType);
                } else if (isContentTypeText(response)) {
                    responseObject = (T) content;
                }
            }
        } catch (IOException e) {
            throw new CloverApiException(500, e);
        }
        return responseObject;
    }

    private boolean isContentTypeJson(Response response) {
        String contentTypeHeader = response.header("Content-Type");
        return contentTypeHeader.contains(APPLICATION_JSON.toString());
    }

    private boolean isContentTypeText(Response response) {
        String contentTypeHeader = response.header("Content-Type");
        return contentTypeHeader.contains(TEXT_PLAIN.toString()) || contentTypeHeader.contains(TEXT_HTML.toString());
    }

    protected HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String s) {
                logWrapper.info(s);
            }
        });
        logger.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return logger;
    }

    protected Request.Builder createHttpGet(String tokenizedUrl, Object... params) {
        return new Request.Builder().url(encodeParameters(tokenizedUrl, params)).get();
    }

    protected <T> PutRequestWithBody createHttpPut(String tokenizedUrl, Object... params) {
        return new PutRequestWithBody(encodeParameters(tokenizedUrl, params));
    }

    protected Request.Builder createHttpDelete(String tokenizedUrl, Object... params) {
        return new Request.Builder().url(encodeParameters(tokenizedUrl, params)).delete();
    }

    protected <T> PostRequestWithBody createHttpPost(String tokenizedUrl, Object... params) {
        return new PostRequestWithBody(encodeParameters(tokenizedUrl, params));
    }

    protected <T> String toJSON(T dto) {
        try {
            if (dto.getClass().equals(String.class)) {
                return (String) dto;
            }
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, dto);
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected <T> T fromJSON(String json, Class<T> dtoClass) {
        try {
            return getObjectMapper().readValue(json, dtoClass);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String encodeParameters(String tokenizedUrl, Object... params) {
        Object[] encodedParams = new Object[params.length];
        int len = params.length;
        for (int i = 0; i < len; i++) {
            try {
                encodedParams[i] = URLEncoder.encode(String.valueOf(params[i]), "UTF-8");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return MessageFormat.format(tokenizedUrl, encodedParams);
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = JsonUtils.getObjectMapper();
        }
        return objectMapper;
    }

    public void setLogger(LogWrapper logWrapper) {
        this.logWrapper = logWrapper;
    }

    public interface LogWrapper {

        public void info(String message);

        public void debug(String message);

        public void debug(String message, Throwable e);

        public void error(String message, Throwable e);

    }

    public abstract class RequestWithBody {

        protected final Request.Builder requestBuilder;

        public RequestWithBody(String url) {
            requestBuilder = new Request.Builder()
                    .url(url);
        }

        public abstract <T> Request.Builder body(T content);

        public abstract Request.Builder empty();

    }

    public class PutRequestWithBody extends RequestWithBody {

        public PutRequestWithBody(String url) {
            super(url);
        }

        @Override
        public <T> Request.Builder body(T content) {
            return requestBuilder.put(RequestBody.create(APPLICATION_JSON, toJSON(content)));
        }

        @Override
        public Request.Builder empty() {
            return requestBuilder.put(RequestBody.create(null, new byte[]{}));
        }
    }

    public class PostRequestWithBody extends RequestWithBody {

        public PostRequestWithBody(String url) {
            super(url);
        }

        @Override
        public <T> Request.Builder body(T content) {
            return requestBuilder.post(RequestBody.create(APPLICATION_JSON, toJSON(content)));
        }

        @Override
        public Request.Builder empty() {
            return requestBuilder.post(RequestBody.create(null, new byte[]{}));
        }
    }

    public class SLF4JWrapper implements LogWrapper {

        private String tag;
        private Logger log;

        public SLF4JWrapper(String tag) {
            this.tag = tag;
        }

        private Logger getLog() {
            if (log == null) {
                log = LoggerFactory.getLogger(tag);
            }
            return log;
        }

        @Override
        public void info(String message) {
            getLog().info(message);
        }

        @Override
        public void debug(String message) {
            getLog().debug(message);
        }

        @Override
        public void debug(String message, Throwable e) {
            getLog().debug(message, e);
        }

        @Override
        public void error(String message, Throwable e) {
            getLog().error(message, e);
        }
    }
}
