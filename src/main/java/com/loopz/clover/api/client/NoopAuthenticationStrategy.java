package com.loopz.clover.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class NoopAuthenticationStrategy implements AuthenticationStrategy {

    @Override
    public void applyCredentials(OkHttpClient.Builder clientBuilder, Request.Builder requestBuilder) {

    }

    @Override
    public String getAuthenticatingUserName() {
        return null;
    }
}
