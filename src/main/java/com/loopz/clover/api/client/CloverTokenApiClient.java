package com.loopz.clover.api.client;

import com.loopz.clover.api.CloverApiException;
import com.loopz.clover.api.model.token.CreateTokenRequest;
import com.loopz.clover.api.model.token.TokenizedCard;
import okhttp3.Request;

public class CloverTokenApiClient extends BaseApiClient {

    public CloverTokenApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        super(rootPath, authStrategy);
    }

    public TokenizedCard createToken(CreateTokenRequest createTokenRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/tokens").body(createTokenRequest);
        return execute(request, TokenizedCard.class);
    }

}
