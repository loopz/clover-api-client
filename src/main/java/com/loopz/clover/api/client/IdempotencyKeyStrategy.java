package com.loopz.clover.api.client;

import okhttp3.Request;

public interface IdempotencyKeyStrategy {

    String HEADER_IDEMPOTENCY_KEY = "X-Idempotency-Key";

    void applyKey(Request.Builder requestBuilder);

}
