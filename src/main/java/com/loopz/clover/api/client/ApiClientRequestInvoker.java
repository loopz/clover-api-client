package com.loopz.clover.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class ApiClientRequestInvoker {

    public Response invoke(OkHttpClient httpClient, Request request) throws IOException {
        return httpClient.newCall(request).execute();
    }

}
