package com.loopz.clover.api.client;

import com.loopz.clover.api.model.oauth.AccessToken;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CloverOAuthClient extends BaseApiClient {

    public CloverOAuthClient(String rootPath) {
        super(rootPath, new NoopAuthenticationStrategy());
    }

    public AccessToken getAccessToken(String clientId, String clientSecret, String authToken) {
        Request.Builder get = createHttpGet(rootPath + "/token?client_id={0}&client_secret={1}&code={2}", clientId, clientSecret, authToken);

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .retryOnConnectionFailure(false);

        Response response = null;
        OkHttpClient httpClient = clientBuilder.build();
        try {
            response = httpClient.newCall(get.build()).execute();
        } catch (IOException e) {
            logWrapper.error("get access token failed.", e);
            return null;
        }
        int statusCode = response.code();
        switch (statusCode) {
            case STATUS_OK:
                try {
                    String content = response.body().string();
                    return fromJSON(content, AccessToken.class);
                } catch (IOException e) {
                    logWrapper.error("access token deserialization failed.", e);
                    return null;
                }
            default:
                return null;
        }
    }
}
