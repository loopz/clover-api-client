package com.loopz.clover.api.client;

import com.loopz.clover.api.CloverApiException;
import com.loopz.clover.api.model.CloverApiUrlBuilder;
import com.loopz.clover.api.model.ecommerce.charge.CaptureChargeRequest;
import com.loopz.clover.api.model.ecommerce.charge.Charge;
import com.loopz.clover.api.model.ecommerce.charge.ChargeExpandableField;
import com.loopz.clover.api.model.ecommerce.charge.CreateChargeRequest;
import com.loopz.clover.api.model.ecommerce.order.Order;
import com.loopz.clover.api.model.ecommerce.order.PayForOrderRequest;
import com.loopz.clover.api.model.ecommerce.refund.CreateRefundRequest;
import com.loopz.clover.api.model.ecommerce.refund.Refund;
import okhttp3.Request;

public class CloverCommerceApiClient extends BaseApiClient {

    public CloverCommerceApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        super(rootPath, authStrategy);
    }

    public Charge createCharge(CreateChargeRequest createChargeRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/charges").body(createChargeRequest);
        return execute(request, Charge.class);
    }

    public Charge getCharge(String chargeId, ChargeExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v1/charges/{0}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, chargeId);
        return execute(request, Charge.class);
    }

    public Charge captureCharge(String chargeId, CaptureChargeRequest captureChargeRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/charges/{0}/capture", chargeId).body(captureChargeRequest);
        return execute(request, Charge.class);
    }

    public Order payForOrder(String orderId, PayForOrderRequest payForOrderRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/orders/{0}/pay", orderId).body(payForOrderRequest);
        return execute(request, Order.class);
    }

    public Refund createRefund(CreateRefundRequest createRefundRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/refunds").body(createRefundRequest);
        return execute(request, Refund.class);
    }

    public Refund getRefund(String refundId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v1/refunds/{0}", refundId);
        return execute(request, Refund.class);
    }

    public void createReturnWithoutOrderItems(String orderId) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v1/orders/{0}/returns", orderId).empty();
        execute(request);
    }


}
