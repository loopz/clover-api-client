package com.loopz.clover.api.client;

import okhttp3.Request;

public class NoopIdempotencyKeyStrategy implements IdempotencyKeyStrategy {
    @Override
    public void applyKey(Request.Builder requestBuilder) {
        // noop
    }
}
