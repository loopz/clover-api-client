package com.loopz.clover.api.client;

import com.fasterxml.jackson.databind.JsonNode;
import com.loopz.clover.api.CloverApi;
import com.loopz.clover.api.CloverApiException;
import com.loopz.clover.api.model.CloverApiUrlBuilder;
import com.loopz.clover.api.model.apikey.ApiKey;
import com.loopz.clover.api.model.billing.BillingInfo;
import com.loopz.clover.api.model.billing.MeteredBillingEvent;
import com.loopz.clover.api.model.billing.MeteredBillingEventList;
import com.loopz.clover.api.model.customer.*;
import com.loopz.clover.api.model.device.DeviceList;
import com.loopz.clover.api.model.employee.EmployeeList;
import com.loopz.clover.api.model.item.*;
import com.loopz.clover.api.model.lineitem.AddLineItemRequest;
import com.loopz.clover.api.model.lineitem.LineItem;
import com.loopz.clover.api.model.merchant.Gateway;
import com.loopz.clover.api.model.merchant.Merchant;
import com.loopz.clover.api.model.merchant.MerchantExpandableField;
import com.loopz.clover.api.model.merchant.MerchantProperties;
import com.loopz.clover.api.model.notification.AppNotification;
import com.loopz.clover.api.model.notification.AppNotificationRequest;
import com.loopz.clover.api.model.order.*;
import com.loopz.clover.api.model.ordertype.OrderTypeExpandableField;
import com.loopz.clover.api.model.ordertype.OrderTypeList;
import com.loopz.clover.api.model.pay.MerchantPayKey;
import com.loopz.clover.api.model.pay.PayRequest;
import com.loopz.clover.api.model.pay.PayResponse;
import com.loopz.clover.api.model.payment.*;
import com.loopz.clover.api.model.refund.Refund;
import com.loopz.clover.api.model.refund.RefundExpandableField;
import com.loopz.clover.api.model.refund.RefundList;
import com.loopz.clover.api.model.tender.CreateTenderRequest;
import com.loopz.clover.api.model.tender.Tender;
import com.loopz.clover.api.model.tender.TenderList;
import com.loopz.clover.api.model.tender.UpdateTenderRequest;
import okhttp3.Request;

import java.util.Collections;
import java.util.List;

public class CloverApiClient extends BaseApiClient {

    public CloverApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        super(rootPath, authStrategy);
    }

    public BillingInfo getBillingInfo(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/apps/{0}/merchants/{1}/billing_info", CloverApi.getAppId(), merchantId);
        return execute(request, BillingInfo.class);
    }

    // Merchants

    public Merchant getMerchant(String merchantId, MerchantExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, Merchant.class);
    }

    public Gateway getMerchantGateway(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/gateway", merchantId);
        return execute(request, Gateway.class);
    }

    public MerchantProperties getMerchantProperties(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/properties", merchantId);
        return execute(request, MerchantProperties.class);
    }

    // Orders

    public Order getOrder(String merchantId, String orderId, OrderExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/orders/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId, orderId);
        return execute(request, Order.class);
    }

    public OrderList getOrders(String merchantId, List<OrderFilterQuery> filterQueries, OrderExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/orders")
                .addFields(expandableFields)
                .addFilterQueries(filterQueries)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, OrderList.class);
    }

    public OrderList getOrders(String merchantId, OrderExpandableField... expandableFields) throws CloverApiException {
        return getOrders(merchantId, Collections.<OrderFilterQuery>emptyList(), expandableFields);
    }


    public Order createOrder(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/orders", merchantId)
                .empty();
        return execute(request, Order.class);
    }

    public Order updateOrder(String merchantId, String orderId, UpdateOrderRequest updateOrderRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/orders/{1}", merchantId, orderId)
                .body(updateOrderRequest);
        return execute(request, Order.class);
    }

    public void deleteOrder(String merchantId, String orderId) throws CloverApiException {
        Request.Builder request = createHttpDelete(rootPath + "v3/merchants/{0}/orders/{1}", merchantId, orderId);
        execute(request);
    }

    public Payment createOrderPayment(String merchantId, String orderId, OrderPaymentRequest orderPaymentRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/orders/{1}/payments", merchantId, orderId)
                .body(orderPaymentRequest);
        return execute(request, Payment.class);
    }

    public LineItem addLineItemToOrder(String merchantId, String orderId, AddLineItemRequest addLineItemRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/orders/{1}/line_items", merchantId, orderId)
                .body(addLineItemRequest);
        return execute(request, LineItem.class);
    }

    public OrderTypeList getOrderTypes(String merchantId, OrderTypeExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/order_types")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, OrderTypeList.class);
    }

    // Metered Billing

    public MeteredBillingEvent createMeteredBillingEvent(String merchantId, int count) throws CloverApiException {
        return createMeteredBillingEvent(merchantId, CloverApi.getMeteredId(), count);
    }

    public MeteredBillingEvent createMeteredBillingEvent(String merchantId, String meteredId, int count) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/apps/{0}/merchants/{1}/metereds/{2}?count={3}", CloverApi.getAppId(), merchantId, meteredId, count)
                .empty();
        return execute(request, MeteredBillingEvent.class);
    }

    public MeteredBillingEvent getMeteredBillingEvent(String merchantId, String eventId) throws CloverApiException {
        return getMeteredBillingEvent(merchantId, CloverApi.getMeteredId(), eventId);
    }

    public MeteredBillingEvent getMeteredBillingEvent(String merchantId, String meteredId, String eventId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/apps/{0}/merchants/{1}/metereds/{2}/events/{3}", CloverApi.getAppId(), merchantId, meteredId, eventId);
        return execute(request, MeteredBillingEvent.class);
    }

    public MeteredBillingEventList getMeteredBillingEvents(String merchantId, String meteredId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/apps/{0}/merchants/{1}/metereds/{2}", CloverApi.getAppId(), merchantId, meteredId);
        return execute(request, MeteredBillingEventList.class);
    }

    public void deleteMeteredBillingEvent(String merchantId, String eventId) throws CloverApiException {
        deleteMeteredBillingEvent(merchantId, CloverApi.getMeteredId(), eventId);
    }

    public void deleteMeteredBillingEvent(String merchantId, String meteredId, String eventId) throws CloverApiException {
        Request.Builder request = createHttpDelete(rootPath + "v3/apps/{0}/merchants/{1}/metereds/{2}/events/{3}", CloverApi.getAppId(), merchantId, meteredId, eventId);
        execute(request);
    }

    // Developer Pay API

    public MerchantPayKey getMerchantPayKey(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v2/merchant/{0}/pay/key", merchantId);
        return execute(request, MerchantPayKey.class);
    }

    public PayResponse pay(String merchantId, PayRequest payRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v2/merchant/{0}/pay", merchantId)
                .body(payRequest);
        return execute(request, PayResponse.class);
    }

    // Payments

    public PaymentList getPayments(String merchantId, List<PaymentFilterQuery> filterQueries, PaymentExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/payments")
                .addFields(expandableFields)
                .addFilterQueries(filterQueries)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, PaymentList.class);
    }

    public PaymentList getPayments(String merchantId, PaymentExpandableField... expandableFields) throws CloverApiException {
        return getPayments(merchantId, Collections.<PaymentFilterQuery>emptyList(), expandableFields);
    }

    public Payment getPayment(String merchantId, String paymentId, PaymentExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/payments/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId, paymentId);
        return execute(request, Payment.class);
    }

    public Payment updatePaymentEmployee(String merchantId, String paymentId, UpdatePaymentEmployeeRequest updatePaymentEmployeeRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/payments/{1}", merchantId, paymentId).body(updatePaymentEmployeeRequest);
        return execute(request, Payment.class);
    }

    public RefundList getRefunds(String merchantId, RefundExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/refunds")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, RefundList.class);
    }

    public Refund getRefund(String merchantId, String refundId, RefundExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/refunds/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId, refundId);
        return execute(request, Refund.class);
    }

    // Tenders

    public Tender getTender(String merchantId, String tenderId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/tenders/{1}", merchantId, tenderId);
        return execute(request, Tender.class);
    }

    public TenderList getTenders(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/tenders", merchantId);
        return execute(request, TenderList.class);
    }

    public Tender createTender(String merchantId, CreateTenderRequest createTenderRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/tenders", merchantId).body(createTenderRequest);
        return execute(request, Tender.class);
    }

    public Tender updateTender(String merchantId, String tenderId, UpdateTenderRequest updateTenderRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/tenders/{1}", merchantId, tenderId).body(updateTenderRequest);
        return execute(request, Tender.class);
    }

    public void deleteTender(String merchantId, String tenderId) throws CloverApiException {
        Request.Builder request = createHttpDelete(rootPath + "v3/merchants/{0}/tenders/{1}", merchantId, tenderId);
        execute(request);
    }

    // Employees

    public EmployeeList getEmployees(String merchantId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/employees", merchantId);
        return execute(request, EmployeeList.class);
    }

    // Customers
    public Customer getCustomer(String merchantId, String customerId, CustomerExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/customers/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId, customerId);
        return execute(request, Customer.class);
    }

    public CustomerList getCustomers(String merchantId, List<CustomerFilterQuery> filterQueries, CustomerExpandableField... expandableFields) throws CloverApiException {
        return getCustomers(merchantId, 1000, 0, filterQueries, expandableFields);
    }

    public CustomerList getCustomers(String merchantId, int limit, int offset, List<CustomerFilterQuery> filterQueries, CustomerExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/customers")
                .addFields(expandableFields)
                .addFilterQueries(filterQueries)
                .limit(limit)
                .offset(offset)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, CustomerList.class);
    }

    public Customer createCustomer(String merchantId, CreateCustomerRequest createCustomerRequest, CustomerExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/customers")
                .addFields(expandableFields)
                .build();

        Request.Builder post = createHttpPost(url, merchantId).body(createCustomerRequest);
        return execute(post, Customer.class);
    }

    public Customer updateCustomer(String merchantId, String customerId, UpdateCustomerRequest updateCustomerRequest, CustomerExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/customers/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder post = createHttpPost(url, merchantId, customerId).body(updateCustomerRequest);
        return execute(post, Customer.class);
    }

    public void deleteCustomer(String merchantId, String customerId) throws CloverApiException {
        Request.Builder delete = createHttpDelete(rootPath + "v3/merchants/{0}/customers/{1}", merchantId, customerId);
        execute(delete);
    }

    // Items

    public Item getItem(String merchantId, String itemId, ItemExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/items/{1}")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId, itemId);
        return execute(request, Item.class);
    }

    public ItemList getItems(String merchantId, ItemExpandableField... expandableFields) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/items")
                .addFields(expandableFields)
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, ItemList.class);
    }

    public Item createItem(String merchantId, CreateItemRequest createItemRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/items", merchantId).body(createItemRequest);
        return execute(request, Item.class);
    }

    public Item updateItem(String merchantId, String itemId, UpdateItemRequest updateItemRequest) throws CloverApiException {
        Request.Builder request = createHttpPut(rootPath + "v3/merchants/{0}/items/{1}", merchantId, itemId).body(updateItemRequest);
        return execute(request, Item.class);
    }

    public void deleteItem(String merchantId, String itemId) throws CloverApiException {
        Request.Builder request = createHttpDelete(rootPath + "v3/merchants/{0}/items/{1}", merchantId, itemId);
        execute(request);
    }

    public ItemStock getItemStock(String merchantId, String itemId) throws CloverApiException {
        Request.Builder request = createHttpGet(rootPath + "v3/merchants/{0}/item_stocks/{1}", merchantId, itemId);
        return execute(request, ItemStock.class);
    }

    public ItemStock updateItemStock(String merchantId, String itemId, UpdateItemStockRequest updateItemStockRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/merchants/{0}/item_stocks/{1}", merchantId, itemId).body(updateItemStockRequest);
        return execute(request, ItemStock.class);
    }

    public DeviceList listDevices(String merchantId) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "v3/merchants/{0}/devices")
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, DeviceList.class);
    }

    public ApiKey getApiKey(String merchantId) throws CloverApiException {
        String url = new CloverApiUrlBuilder(rootPath + "pakms/apikey")
                .build();

        Request.Builder request = createHttpGet(url, merchantId);
        return execute(request, ApiKey.class);
    }

    // Notifications

    public AppNotification createAppNotification(String merchantId, AppNotificationRequest appNotificationRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/apps/{0}/merchants/{1}/notifications", CloverApi.getAppId(), merchantId).body(appNotificationRequest);
        return execute(request, AppNotification.class);
    }

    public AppNotification createDeviceNotification(String deviceId, AppNotificationRequest appNotificationRequest) throws CloverApiException {
        Request.Builder request = createHttpPost(rootPath + "v3/apps/{0}/devices/{1}/notifications", CloverApi.getAppId(), deviceId).body(appNotificationRequest);
        return execute(request, AppNotification.class);
    }

}
