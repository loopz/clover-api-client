package com.loopz.clover.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Applies the Clover API Key header to the given request.
 */
public class ApiKeyAuthenticationStrategy implements AuthenticationStrategy {

    private final String apiKey;

    public ApiKeyAuthenticationStrategy(String apiKey) {
        if(apiKey == null || apiKey.isEmpty()) {
            throw new IllegalArgumentException("API Key cannot be empty");
        }
        this.apiKey = apiKey;
    }

    @Override
    public void applyCredentials(OkHttpClient.Builder clientBuilder, Request.Builder requestBuilder) {
        requestBuilder.addHeader("apikey", apiKey);
    }

    @Override
    public String getAuthenticatingUserName() {
        return "api-key";
    }
}
