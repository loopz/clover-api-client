package com.loopz.clover.api.model.lineitem;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class LineItemList {

    private final List<LineItem> elements;

    @JsonCreator
    public LineItemList(@JsonProperty("elements") List<LineItem> elements) {
        this.elements = elements;
    }

    public List<LineItem> getElements() {
        return elements;
    }
}
