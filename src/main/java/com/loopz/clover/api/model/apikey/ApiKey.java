package com.loopz.clover.api.model.apikey;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = ApiKey.Builder.class)
public class ApiKey {

    private final boolean active;
    private final String apiKey;
    private final String developerAppUuid;
    private final String merchantUuid;
    private final DateTime createdTime;
    private final DateTime modifiedTime;

    private ApiKey(Builder builder) {
        active = builder.active;
        apiKey = builder.apiKey;
        developerAppUuid = builder.developerAppUuid;
        merchantUuid = builder.merchantUuid;
        createdTime = builder.createdTime;
        modifiedTime = builder.modifiedTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public boolean isActive() {
        return active;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getDeveloperAppUuid() {
        return developerAppUuid;
    }

    public String getMerchantUuid() {
        return merchantUuid;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private boolean active;
        private String apiKey;
        private String developerAppUuid;
        private String merchantUuid;
        private DateTime createdTime;
        private DateTime modifiedTime;

        private Builder() {
        }

        public Builder active(boolean val) {
            active = val;
            return this;
        }

        @JsonProperty("apiAccessKey")
        public Builder apiKey(String val) {
            apiKey = val;
            return this;
        }

        public Builder developerAppUuid(String val) {
            developerAppUuid = val;
            return this;
        }

        public Builder merchantUuid(String val) {
            merchantUuid = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public ApiKey build() {
            return new ApiKey(this);
        }
    }
}
