package com.loopz.clover.api.model.order;

import com.loopz.clover.api.model.FilterQuery;
import com.loopz.clover.api.model.QueryOperators;

public class OrderFilterQuery extends FilterQuery<OrderFilterQueryField> {

    public OrderFilterQuery(OrderFilterQueryField field, QueryOperators operator, Object value) {
        super(field, operator, value);
    }
}
