package com.loopz.clover.api.model.category;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class CategoryList {

    private final List<Category> elements;

    @JsonCreator
    public CategoryList(@JsonProperty("elements") List<Category> elements) {
        this.elements = elements;
    }

    public List<Category> getElements() {
        return elements;
    }
}
