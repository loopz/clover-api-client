package com.loopz.clover.api.model.ecommerce.charge;

import com.loopz.clover.api.model.ExpandableField;

public enum ChargeExpandableField implements ExpandableField {

    SOURCE("source"),
    METADATA("metadata");

    private final String field;

    ChargeExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
