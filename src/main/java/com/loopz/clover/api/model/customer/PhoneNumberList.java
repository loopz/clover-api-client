package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PhoneNumberList {

    private final List<PhoneNumber> elements;

    @JsonCreator
    public PhoneNumberList(@JsonProperty("elements") List<PhoneNumber> elements) {
        this.elements = elements;
    }

    public List<PhoneNumber> getElements() {
        return elements;
    }
}
