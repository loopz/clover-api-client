package com.loopz.clover.api.model.category;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.item.ItemList;
import org.joda.time.DateTime;

/**
 "modifiedTime": "long",
 "deleted": false,
 "sortOrder": "int",
 "name": "",
 "id": "",
 "items": "--Expandable Field--"
 */
@JsonDeserialize(builder = Category.Builder.class)
public class Category {

    private final String id;
    private final String name;
    private final boolean deleted;
    private final DateTime modifiedTime;
    private final int sortOrder;
    private final ItemList items;

    private Category(Builder builder) {
        id = builder.id;
        name = builder.name;
        deleted = builder.deleted;
        modifiedTime = builder.modifiedTime;
        sortOrder = builder.sortOrder;
        items = builder.items;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public ItemList getItems() {
        return items;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private boolean deleted;
        private DateTime modifiedTime;
        private int sortOrder;
        private ItemList items;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder deleted(boolean val) {
            deleted = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder sortOrder(int val) {
            sortOrder = val;
            return this;
        }

        public Builder items(ItemList val) {
            items = val;
            return this;
        }

        public Category build() {
            return new Category(this);
        }
    }
}
