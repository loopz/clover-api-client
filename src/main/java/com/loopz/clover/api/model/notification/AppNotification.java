package com.loopz.clover.api.model.notification;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.app.AppReference;

@JsonDeserialize(builder = AppNotification.Builder.class)
public class AppNotification {

    private final AppReference app;
    private final String event;
    private final Long timeToLive;
    private final String data;

    private AppNotification(Builder builder) {
        app = builder.app;
        event = builder.event;
        timeToLive = builder.timeToLive;
        data = builder.data;
    }

    public static Builder builder() {
        return new Builder();
    }

    public AppReference getApp() {
        return app;
    }

    public String getEvent() {
        return event;
    }

    public Long getTimeToLive() {
        return timeToLive;
    }

    public String getData() {
        return data;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private AppReference app;
        private String event;
        private Long timeToLive;
        private String data;

        private Builder() {
        }

        public Builder app(AppReference val) {
            app = val;
            return this;
        }

        public Builder event(String val) {
            event = val;
            return this;
        }

        public Builder timeToLive(Long val) {
            timeToLive = val;
            return this;
        }

        public Builder data(String val) {
            data = val;
            return this;
        }

        public AppNotification build() {
            return new AppNotification(this);
        }
    }
}
