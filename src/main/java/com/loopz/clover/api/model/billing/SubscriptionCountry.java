package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = SubscriptionCountry.Builder.class)
public class SubscriptionCountry {

    private final String id;
    private final String name;
    private final Long amount;
    private final String country;
    private final String description;
    private final boolean active;

    private SubscriptionCountry(Builder builder) {
        id = builder.id;
        name = builder.name;
        amount = builder.amount;
        country = builder.country;
        description = builder.description;
        active = builder.active;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getAmount() {
        return amount;
    }

    public String getCountry() {
        return country;
    }

    public String getDescription() {
        return description;
    }

    public boolean isActive() {
        return active;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private Long amount;
        private String country;
        private String description;
        private boolean active;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder active(boolean val) {
            active = val;
            return this;
        }

        public SubscriptionCountry build() {
            return new SubscriptionCountry(this);
        }
    }
}
