package com.loopz.clover.api.model.item;

public enum PriceType {

    FIXED,
    VARIABLE,
    PER_UNIT;

}
