package com.loopz.clover.api.model.refund;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class RefundList {

    private List<Refund> elements;

    @JsonCreator
    public RefundList(@JsonProperty("elements") List<Refund> elements) {
        this.elements = elements;
    }

    public List<Refund> getElements() {
        return elements;
    }
}
