package com.loopz.clover.api.model.ordertype;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = OrderType.Builder.class)
public class OrderType {

    private final String id;
    private final String labelKey;
    private final String label;
    private final boolean taxable;
    private final boolean isDefault;
    private final boolean filterCategories;
    private final Long fee;
    private final Long minOrderAmount;
    private final Long maxOrderAmount;
    private final Long maxRadius;
    private final Long avgOrderTime;
    private final String hoursAvailable;
    private final String customerIdMethod;
    private final boolean isDeleted;
    private final String systemOrderTypeId;

    private OrderType(Builder builder) {
        id = builder.id;
        labelKey = builder.labelKey;
        label = builder.label;
        taxable = builder.taxable;
        isDefault = builder.isDefault;
        filterCategories = builder.filterCategories;
        fee = builder.fee;
        minOrderAmount = builder.minOrderAmount;
        maxOrderAmount = builder.maxOrderAmount;
        maxRadius = builder.maxRadius;
        avgOrderTime = builder.avgOrderTime;
        hoursAvailable = builder.hoursAvailable;
        customerIdMethod = builder.customerIdMethod;
        isDeleted = builder.isDeleted;
        systemOrderTypeId = builder.systemOrderTypeId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public String getLabel() {
        return label;
    }

    public boolean isTaxable() {
        return taxable;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public boolean isFilterCategories() {
        return filterCategories;
    }

    public Long getFee() {
        return fee;
    }

    public Long getMinOrderAmount() {
        return minOrderAmount;
    }

    public Long getMaxOrderAmount() {
        return maxOrderAmount;
    }

    public Long getMaxRadius() {
        return maxRadius;
    }

    public Long getAvgOrderTime() {
        return avgOrderTime;
    }

    public String getHoursAvailable() {
        return hoursAvailable;
    }

    public String getCustomerIdMethod() {
        return customerIdMethod;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public String getSystemOrderTypeId() {
        return systemOrderTypeId;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String labelKey;
        private String label;
        private boolean taxable;
        private boolean isDefault;
        private boolean filterCategories;
        private Long fee;
        private Long minOrderAmount;
        private Long maxOrderAmount;
        private Long maxRadius;
        private Long avgOrderTime;
        private String hoursAvailable;
        private String customerIdMethod;
        private boolean isDeleted;
        private String systemOrderTypeId;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder labelKey(String val) {
            labelKey = val;
            return this;
        }

        public Builder label(String val) {
            label = val;
            return this;
        }

        public Builder taxable(boolean val) {
            taxable = val;
            return this;
        }

        public Builder isDefault(boolean val) {
            isDefault = val;
            return this;
        }

        public Builder filterCategories(boolean val) {
            filterCategories = val;
            return this;
        }

        public Builder fee(Long val) {
            fee = val;
            return this;
        }

        public Builder minOrderAmount(Long val) {
            minOrderAmount = val;
            return this;
        }

        public Builder maxOrderAmount(Long val) {
            maxOrderAmount = val;
            return this;
        }

        public Builder maxRadius(Long val) {
            maxRadius = val;
            return this;
        }

        public Builder avgOrderTime(Long val) {
            avgOrderTime = val;
            return this;
        }

        public Builder hoursAvailable(String val) {
            hoursAvailable = val;
            return this;
        }

        public Builder customerIdMethod(String val) {
            customerIdMethod = val;
            return this;
        }

        public Builder isDeleted(boolean val) {
            isDeleted = val;
            return this;
        }

        public Builder systemOrderTypeId(String val) {
            systemOrderTypeId = val;
            return this;
        }

        public OrderType build() {
            return new OrderType(this);
        }
    }
}
