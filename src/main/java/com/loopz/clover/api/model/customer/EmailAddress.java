package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = EmailAddress.Builder.class)
public class EmailAddress {

    private final String id;
    private final String emailAddress;
    private final DateTime verifiedTime;
    private final boolean primaryEmail;

    private EmailAddress(Builder builder) {
        id = builder.id;
        emailAddress = builder.emailAddress;
        verifiedTime = builder.verifiedTime;
        primaryEmail = builder.primaryEmail;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public DateTime getVerifiedTime() {
        return verifiedTime;
    }

    public boolean isPrimaryEmail() {
        return primaryEmail;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String emailAddress;
        private DateTime verifiedTime;
        private boolean primaryEmail;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder emailAddress(String val) {
            emailAddress = val;
            return this;
        }

        public Builder verifiedTime(DateTime val) {
            verifiedTime = val;
            return this;
        }

        public Builder primaryEmail(boolean val) {
            primaryEmail = val;
            return this;
        }

        public EmailAddress build() {
            return new EmailAddress(this);
        }
    }
}
