package com.loopz.clover.api.model.pay;

public enum CVVResultType {

    SUCCESS,
    FAILURE,
    NOT_PROCESSED,
    NOT_PRESENT;

}
