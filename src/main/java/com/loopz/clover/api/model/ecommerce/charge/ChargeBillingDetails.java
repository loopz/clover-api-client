package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.ecommerce.order.PayForOrderRequest;

@JsonDeserialize(builder = ChargeBillingDetails.Builder.class)
public class ChargeBillingDetails {

    private final String line1;
    private final String line2;
    private final String postalCode;
    private final String state;
    private final String email;
    private final String name;
    private final String phone;

    private ChargeBillingDetails(Builder builder) {
        line1 = builder.line1;
        line2 = builder.line2;
        postalCode = builder.postalCode;
        state = builder.state;
        email = builder.email;
        name = builder.name;
        phone = builder.phone;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getLine1() {
        return line1;
    }

    public String getLine2() {
        return line2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getState() {
        return state;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "ChargeBillingDetails{" +
                "line1='" + line1 + '\'' +
                ", line2='" + line2 + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", state='" + state + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String line1;
        private String line2;
        private String postalCode;
        private String state;
        private String email;
        private String name;
        private String phone;

        public Builder() {
        }

        public Builder line1(String val) {
            line1 = val;
            return this;
        }

        public Builder line2(String val) {
            line2 = val;
            return this;
        }

        @JsonProperty("postal_code")
        public Builder postalCode(String val) {
            postalCode = val;
            return this;
        }

        public Builder state(String val) {
            state = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public ChargeBillingDetails build() {
            return new ChargeBillingDetails(this);
        }
    }
}
