package com.loopz.clover.api.model.item;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ItemList {

    private final List<Item> elements;

    @JsonCreator
    public ItemList(@JsonProperty("elements") List<Item> elements) {
        this.elements = elements;
    }

    public List<Item> getElements() {
        return elements;
    }
}
