package com.loopz.clover.api.model.ecommerce.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = OrderLineItemTaxRate.Builder.class)
public class OrderLineItemTaxRate {

    private final String id;
    private final String name;
    private final String type;

    private OrderLineItemTaxRate(Builder builder) {
        id = builder.id;
        name = builder.name;
        type = builder.type;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {

        private String id;
        private String name;
        private String type;

        private Builder() {
        }

        @JsonProperty("tax_rate_uuid")
        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public OrderLineItemTaxRate build() {
            return new OrderLineItemTaxRate(this);
        }
    }
}
