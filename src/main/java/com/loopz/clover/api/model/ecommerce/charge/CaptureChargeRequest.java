package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CaptureChargeRequest.Builder.class)
public class CaptureChargeRequest {

    private final Long amount;
    private final String receiptEmail;

    private CaptureChargeRequest(Builder builder) {
        amount = builder.amount;
        receiptEmail = builder.receiptEmail;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getAmount() {
        return amount;
    }

    @JsonProperty("receipt_email")
    public String getReceiptEmail() {
        return receiptEmail;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Long amount;
        private String receiptEmail;

        public Builder() {
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder receiptEmail(String val) {
            receiptEmail = val;
            return this;
        }

        public CaptureChargeRequest build() {
            return new CaptureChargeRequest(this);
        }
    }
}
