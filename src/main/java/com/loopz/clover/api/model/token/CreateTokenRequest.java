package com.loopz.clover.api.model.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateTokenRequest {

    private final Card card;

    @JsonCreator
    public CreateTokenRequest(@JsonProperty("card") Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }
}
