package com.loopz.clover.api.model.tender;

public class UpdateTenderRequest {

    private final String label;
    private final String labelKey;
    private final boolean opensCashDrawer;
    private final boolean supportsTipping;
    private final boolean enabled;
    private final boolean editable;
    private final boolean visible;
    private final String instructions;

    private UpdateTenderRequest(Builder builder) {
        label = builder.label;
        labelKey = builder.labelKey;
        opensCashDrawer = builder.opensCashDrawer;
        supportsTipping = builder.supportsTipping;
        enabled = builder.enabled;
        editable = builder.editable;
        visible = builder.visible;
        instructions = builder.instructions;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getLabel() {
        return label;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public boolean isOpensCashDrawer() {
        return opensCashDrawer;
    }

    public boolean isSupportsTipping() {
        return supportsTipping;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getInstructions() {
        return instructions;
    }

    public static final class Builder {
        private String label;
        private String labelKey;
        private boolean opensCashDrawer;
        private boolean supportsTipping;
        private boolean enabled;
        private boolean editable;
        private boolean visible;
        private String instructions;

        private Builder() {
        }

        public Builder label(String val) {
            label = val;
            return this;
        }

        public Builder labelKey(String val) {
            labelKey = val;
            return this;
        }

        public Builder opensCashDrawer(boolean val) {
            opensCashDrawer = val;
            return this;
        }

        public Builder supportsTipping(boolean val) {
            supportsTipping = val;
            return this;
        }

        public Builder enabled(boolean val) {
            enabled = val;
            return this;
        }

        public Builder editable(boolean val) {
            editable = val;
            return this;
        }

        public Builder visible(boolean val) {
            visible = val;
            return this;
        }

        public Builder instructions(String val) {
            instructions = val;
            return this;
        }

        public UpdateTenderRequest build() {
            return new UpdateTenderRequest(this);
        }
    }
}
