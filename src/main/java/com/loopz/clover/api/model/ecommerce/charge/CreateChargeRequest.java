package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

public class CreateChargeRequest {

    private final long amount;
    private final String currency;
    private final boolean capture;
    private final String description;
    private final ChargeInputType inputType;
    private final String externalReferenceId;
    private final String receiptEmail;
    private final String source;
    private final StoredCredentials storedCredentials;
    private final long taxAmount;
    private final long tipAmount;
    private final ChargeCustomTender tender;

    private CreateChargeRequest(Builder builder) {
        amount = builder.amount;
        currency = builder.currency;
        capture = builder.capture;
        description = builder.description;
        inputType = builder.inputType;
        externalReferenceId = builder.externalReferenceId;
        receiptEmail = builder.receiptEmail;
        source = builder.source;
        storedCredentials = builder.storedCredentials;
        taxAmount = builder.taxAmount;
        tipAmount = builder.tipAmount;
        tender = builder.tender;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    @JsonProperty("is_capture")
    public boolean isCapture() {
        return capture;
    }

    public String getDescription() {
        return description;
    }

    @JsonProperty("ecomind")
    public ChargeInputType getInputType() {
        return inputType;
    }

    @JsonProperty("external_reference_id")
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    @JsonProperty("receipt_email")
    public String getReceiptEmail() {
        return receiptEmail;
    }

    public String getSource() {
        return source;
    }

    @JsonProperty("stored_credentials")
    public StoredCredentials getStoredCredentials() {
        return storedCredentials;
    }

    @JsonProperty("tax_amount")
    public long getTaxAmount() {
        return taxAmount;
    }

    @JsonProperty("tip_amount")
    public long getTipAmount() {
        return tipAmount;
    }

    public ChargeCustomTender getTender() {
        return tender;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private long amount;
        private String currency;
        private boolean capture;
        private String description;
        private ChargeInputType inputType;
        private String externalReferenceId;
        private String receiptEmail;
        private String source;
        private StoredCredentials storedCredentials;
        private long taxAmount;
        private long tipAmount;
        private ChargeCustomTender tender;

        public Builder() {
        }

        public Builder amount(long val) {
            amount = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public Builder capture(boolean val) {
            capture = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder inputType(ChargeInputType val) {
            inputType = val;
            return this;
        }

        public Builder externalReferenceId(String val) {
            externalReferenceId = val;
            return this;
        }

        public Builder receiptEmail(String val) {
            receiptEmail = val;
            return this;
        }

        public Builder source(String val) {
            source = val;
            return this;
        }

        public Builder storedCredentials(StoredCredentials val) {
            storedCredentials = val;
            return this;
        }

        public Builder taxAmount(long val) {
            taxAmount = val;
            return this;
        }

        public Builder tipAmount(long val) {
            tipAmount = val;
            return this;
        }

        public Builder tender(ChargeCustomTender val) {
            tender = val;
            return this;
        }

        public CreateChargeRequest build() {
            return new CreateChargeRequest(this);
        }
    }
}
