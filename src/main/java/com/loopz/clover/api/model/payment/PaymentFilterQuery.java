package com.loopz.clover.api.model.payment;

import com.loopz.clover.api.model.FilterQuery;
import com.loopz.clover.api.model.QueryOperators;

public class PaymentFilterQuery extends FilterQuery<PaymentFilterQueryField> {

    public PaymentFilterQuery(PaymentFilterQueryField field, QueryOperators operator, Object value) {
        super(field, operator, value);
    }
}
