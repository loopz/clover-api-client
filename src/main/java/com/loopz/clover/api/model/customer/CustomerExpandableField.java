package com.loopz.clover.api.model.customer;

import com.loopz.clover.api.model.ExpandableField;

public enum CustomerExpandableField implements ExpandableField {

    ADDRESSES("addresses"),
    EMAIL_ADDRESSES("emailAddresses"),
    PHONE_NUMBERS("phoneNumbers"),
    CARDS("cards"),
    ORDERS("orders"),
    METADATA("metadata");

    private final String field;

    CustomerExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
