package com.loopz.clover.api.model;

public enum QueryOperators {

    EQUALS("="),
    NOT("!="),
    GREATER_THAN(">"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN("<"),
    LESS_THAN_OR_EQUAL("<=");

    private final String operator;

    QueryOperators(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }
}
