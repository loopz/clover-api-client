package com.loopz.clover.api.model.item;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 {
 "itemGroup": {
 "id": ""
 },
 "modifiedTime": "long",
 "code": "",
 "cost": "long",
 "hidden": false,
 "unitName": "",
 "priceType": "",
 "alternateName": "",
 "isRevenue": false,
 "modifierGroups": "--Expandable Field--",
 "tags": "--Expandable Field--",
 "taxRates": "--Expandable Field--",
 "itemStock": "--Expandable Field--",
 "price": "long",
 "options": "--Expandable Field--",
 "name": "",
 "id": "",
 "categories": "--Expandable Field--",
 "sku": "",
 "deletedTime": "long",
 "defaultTaxRates": false,
 "stockCount": "long",
 "priceWithoutVat": "long"
 }
 */
@JsonDeserialize(builder = UpdateItemRequest.Builder.class)
public class UpdateItemRequest {

    private final String name;
    private final String alternateName;
    private final String unitName;
    private final PriceType priceType;
    private final String code;
    private final String sku;
    private final Long price;
    private final boolean hidden;
    private final Long priceWithoutVat;
    private final boolean isRevenue;
    private final boolean defaultTaxRates;
    private final Long stockCount;

    private UpdateItemRequest(Builder builder) {
        name = builder.name;
        alternateName = builder.alternateName;
        unitName = builder.unitName;
        priceType = builder.priceType;
        code = builder.code;
        sku = builder.sku;
        price = builder.price;
        hidden = builder.hidden;
        priceWithoutVat = builder.priceWithoutVat;
        isRevenue = builder.isRevenue;
        defaultTaxRates = builder.defaultTaxRates;
        stockCount = builder.stockCount;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getName() {
        return name;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public String getUnitName() {
        return unitName;
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public String getCode() {
        return code;
    }

    public String getSku() {
        return sku;
    }

    public Long getPrice() {
        return price;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Long getPriceWithoutVat() {
        return priceWithoutVat;
    }

    @JsonProperty("isRevenue")
    public boolean isRevenue() {
        return isRevenue;
    }

    public boolean isDefaultTaxRates() {
        return defaultTaxRates;
    }

    public Long getStockCount() {
        return stockCount;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String name;
        private String alternateName;
        private String unitName;
        private PriceType priceType;
        private String code;
        private String sku;
        private Long price;
        private boolean hidden;
        private Long priceWithoutVat;
        private boolean isRevenue;
        private boolean defaultTaxRates;
        private Long stockCount;

        private Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder alternateName(String val) {
            alternateName = val;
            return this;
        }

        public Builder unitName(String val) {
            unitName = val;
            return this;
        }

        public Builder priceType(PriceType val) {
            priceType = val;
            return this;
        }

        public Builder code(String val) {
            code = val;
            return this;
        }

        public Builder sku(String val) {
            sku = val;
            return this;
        }

        public Builder price(Long val) {
            price = val;
            return this;
        }

        public Builder hidden(boolean val) {
            hidden = val;
            return this;
        }

        public Builder priceWithoutVat(Long val) {
            priceWithoutVat = val;
            return this;
        }

        public Builder isRevenue(boolean val) {
            isRevenue = val;
            return this;
        }

        public Builder defaultTaxRates(boolean val) {
            defaultTaxRates = val;
            return this;
        }

        public Builder stockCount(Long val) {
            stockCount = val;
            return this;
        }

        public UpdateItemRequest build() {
            return new UpdateItemRequest(this);
        }
    }
}
