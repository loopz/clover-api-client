package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = Charge.Builder.class)
public class Charge {

    private final String id;
    private final Long amount;
    private final Long taxAmount;
    private final Long tipAmount;
    private final Long amountRefunded;
    private final ChargeBillingDetails billingDetails;
    private final boolean captured;
    private final DateTime created;
    private final String currency;
    private final String description;
    private final String externalReferenceId;
    private final String authCode;
    private final String refNum;
    private final String failureCode;
    private final String failureMessage;
    private final String warningMessage;
    private final boolean livemode;
    private final boolean paid;
    private final String paymentMethod;
    private final String receiptEmail;
    private final String receiptNumber;
    private final String receiptUrl;
    private final boolean refunded;
    private final ChargeSource source;
    private final String orderId;

    private Charge(Builder builder) {
        id = builder.id;
        amount = builder.amount;
        taxAmount = builder.taxAmount;
        tipAmount = builder.tipAmount;
        amountRefunded = builder.amountRefunded;
        billingDetails = builder.billingDetails;
        captured = builder.captured;
        created = builder.created;
        currency = builder.currency;
        description = builder.description;
        externalReferenceId = builder.externalReferenceId;
        authCode = builder.authCode;
        refNum = builder.refNum;
        failureCode = builder.failureCode;
        failureMessage = builder.failureMessage;
        warningMessage = builder.warningMessage;
        livemode = builder.livemode;
        paid = builder.paid;
        paymentMethod = builder.paymentMethod;
        receiptEmail = builder.receiptEmail;
        receiptNumber = builder.receiptNumber;
        receiptUrl = builder.receiptUrl;
        refunded = builder.refunded;
        source = builder.source;
        orderId = builder.orderId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public Long getTipAmount() {
        return tipAmount;
    }

    public Long getAmountRefunded() {
        return amountRefunded;
    }

    public ChargeBillingDetails getBillingDetails() {
        return billingDetails;
    }

    public boolean isCaptured() {
        return captured;
    }

    public DateTime getCreated() {
        return created;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDescription() {
        return description;
    }

    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public String getRefNum() {
        return refNum;
    }

    public String getFailureCode() {
        return failureCode;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public boolean isLivemode() {
        return livemode;
    }

    public boolean isPaid() {
        return paid;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public String getReceiptUrl() {
        return receiptUrl;
    }

    public boolean isRefunded() {
        return refunded;
    }

    public ChargeSource getSource() {
        return source;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    public String toString() {
        return "Charge{" +
                "id='" + id + '\'' +
                ", amount=" + amount +
                ", taxAmount=" + taxAmount +
                ", tipAmount=" + tipAmount +
                ", amountRefunded=" + amountRefunded +
                ", billingDetails=" + billingDetails +
                ", captured=" + captured +
                ", created=" + created +
                ", currency='" + currency + '\'' +
                ", description='" + description + '\'' +
                ", externalReferenceId='" + externalReferenceId + '\'' +
                ", authCode='" + authCode + '\'' +
                ", refNum='" + refNum + '\'' +
                ", failureCode='" + failureCode + '\'' +
                ", failureMessage='" + failureMessage + '\'' +
                ", warningMessage='" + warningMessage + '\'' +
                ", livemode=" + livemode +
                ", paid=" + paid +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", receiptEmail='" + receiptEmail + '\'' +
                ", receiptNumber='" + receiptNumber + '\'' +
                ", receiptUrl='" + receiptUrl + '\'' +
                ", refunded=" + refunded +
                ", source=" + source +
                ", orderId='" + orderId + '\'' +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private Long amount;
        private Long taxAmount;
        private Long tipAmount;
        private Long amountRefunded;
        private ChargeBillingDetails billingDetails;
        private boolean captured;
        private DateTime created;
        private String currency;
        private String description;
        private String externalReferenceId;
        private String authCode;
        private String refNum;
        private String failureCode;
        private String failureMessage;
        private String warningMessage;
        private boolean livemode;
        private boolean paid;
        private String paymentMethod;
        private String receiptEmail;
        private String receiptNumber;
        private String receiptUrl;
        private boolean refunded;
        private ChargeSource source;
        private String orderId;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        @JsonProperty("tax_amount")
        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        @JsonProperty("tip_amount")
        public Builder tipAmount(Long val) {
            tipAmount = val;
            return this;
        }

        @JsonProperty("amount_refunded")
        public Builder amountRefunded(Long val) {
            amountRefunded = val;
            return this;
        }

        @JsonProperty("billing_details")
        public Builder billingDetails(ChargeBillingDetails val) {
            billingDetails = val;
            return this;
        }

        public Builder captured(boolean val) {
            captured = val;
            return this;
        }

        public Builder created(DateTime val) {
            created = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        @JsonProperty("external_reference_id")
        public Builder externalReferenceId(String val) {
            externalReferenceId = val;
            return this;
        }

        @JsonProperty("auth_code")
        public Builder authCode(String val) {
            authCode = val;
            return this;
        }

        @JsonProperty("ref_num")
        public Builder refNum(String val) {
            refNum = val;
            return this;
        }

        @JsonProperty("failure_code")
        public Builder failureCode(String val) {
            failureCode = val;
            return this;
        }

        @JsonProperty("failure_message")
        public Builder failureMessage(String val) {
            failureMessage = val;
            return this;
        }

        @JsonProperty("warning_message")
        public Builder warningMessage(String val) {
            warningMessage = val;
            return this;
        }

        public Builder livemode(boolean val) {
            livemode = val;
            return this;
        }

        public Builder paid(boolean val) {
            paid = val;
            return this;
        }

        @JsonProperty("payment_method")
        public Builder paymentMethod(String val) {
            paymentMethod = val;
            return this;
        }

        @JsonProperty("receipt_email")
        public Builder receiptEmail(String val) {
            receiptEmail = val;
            return this;
        }

        @JsonProperty("receipt_number")
        public Builder receiptNumber(String val) {
            receiptNumber = val;
            return this;
        }

        @JsonProperty("receipt_url")
        public Builder receiptUrl(String val) {
            receiptUrl = val;
            return this;
        }

        public Builder refunded(boolean val) {
            refunded = val;
            return this;
        }

        public Builder source(ChargeSource val) {
            source = val;
            return this;
        }

        @JsonProperty("order")
        public Builder orderId(String val) {
            orderId = val;
            return this;
        }

        public Charge build() {
            return new Charge(this);
        }
    }
}
