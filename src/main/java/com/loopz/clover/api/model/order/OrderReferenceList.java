package com.loopz.clover.api.model.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OrderReferenceList {

    private final List<OrderReference> elements;

    @JsonCreator
    public OrderReferenceList(@JsonProperty("elements") List<OrderReference> elements) {
        this.elements = elements;
    }

    public List<OrderReference> getElements() {
        return elements;
    }
}
