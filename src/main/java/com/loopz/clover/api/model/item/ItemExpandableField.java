package com.loopz.clover.api.model.item;

import com.loopz.clover.api.model.ExpandableField;

public enum ItemExpandableField implements ExpandableField {

    TAGS("tags"),
    CATEGORIES("categories"),
    TAX_RATES("taxRates"),
    MODIFIER_GROUPS("modifierGroups"),
    ITEM_STOCK("itemStock"),
    OPTIONS("options");

    private final String field;

    ItemExpandableField(String field) {
        this.field = field;
    }


    @Override
    public String getField() {
        return field;
    }
}
