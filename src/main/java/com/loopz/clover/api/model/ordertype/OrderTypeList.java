package com.loopz.clover.api.model.ordertype;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.clover.api.model.order.Order;

import java.util.List;

public class OrderTypeList {

    private final List<OrderType> elements;

    @JsonCreator
    public OrderTypeList(@JsonProperty("elements") List<OrderType> elements) {
        this.elements = elements;
    }

    public List<OrderType> getElements() {
        return elements;
    }
}
