package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public enum ChargeInputType {

    CUSTOMER("ecom"),
    MERCHANT("moto");

    private final String id;

    @JsonCreator
    ChargeInputType(@JsonProperty String id) {
        this.id = id;
    }

    @JsonValue
    public String getId() {
        return id;
    }
}
