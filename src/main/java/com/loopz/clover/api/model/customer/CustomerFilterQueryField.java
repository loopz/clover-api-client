package com.loopz.clover.api.model.customer;

import com.loopz.clover.api.model.FilterQueryField;

public enum CustomerFilterQueryField implements FilterQueryField {

    EMAIL_ADDRESS("emailAddress"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    ID("id"),
    PHONE_NUMBER("phoneNumber"),
    FULL_NAME("fullName"),
    CUSTOMER_SINCE("customerSince"),
    MARKETING_ALLOWED("marketingAllowed"),
    DELETED_TIME("deletedTime");

    private final String field;

    CustomerFilterQueryField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
