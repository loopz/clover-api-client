package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class EmailAddressList {

    private final List<EmailAddress> elements;

    @JsonCreator
    public EmailAddressList(@JsonProperty("elements") List<EmailAddress> elements) {
        this.elements = elements;
    }

    public List<EmailAddress> getElements() {
        return elements;
    }
}
