package com.loopz.clover.api.model.payment;

import com.loopz.clover.api.model.FilterQueryField;

public enum PaymentFilterQueryField implements FilterQueryField {

    EXTERNAL_REFERENCE_ID("externalReferenceId"),
    EXTERNAL_PAYMENT_ID("externalPaymentId"),
    LAST_4("cardTransaction.last4"),
    CASH_TENDERED("cashTendered"),
    DEVICE_ID("device.id"),
    OFFLINE("offline"),
    CARD_TRANSACTION_NUMBER("cardTransaction.transactionNo"),
    CARD_TRANSACTION_CAPTURED("cardTransaction.captured"),
    CARD_TRANSACTION_CURRENCY("cardTransaction.currency"),
    REFUND_ID("paymentRefundId"),
    VOIDED("voided"),
    ID("id"),
    TENDER_ID("tender.id"),
    ORDER_TYPE_ID("orderType.databaseId"),
    EMPLOYEE_ID("employee.id"),
    ORDER_MODIFIED_TIME("order.modifiedTime"),
    AMOUNT("amount"),
    TAX_AMOUNT("taxAmount"),
    TIP_AMOUNT("tipAmount"),
    CARD_TYPE("cardType"),
    ORDER_ID("order.id"),
    CLIENT_CREATED_TIME("clientCreatedTime"),
    CREATED_TIME("createdTime"),
    MODIFIED_TIME("modifiedTime"),
    DELETED_TIME("deletedTime");

    private final String field;

    PaymentFilterQueryField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
