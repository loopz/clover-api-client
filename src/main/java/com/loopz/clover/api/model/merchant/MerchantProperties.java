package com.loopz.clover.api.model.merchant;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * merchantRef
 * String	merchantRef.id
 * Unique identifier
 * <p>
 * String	defaultCurrency
 * Boolean	tipsEnabled
 * Integer	maxTipPercentage
 * String	receiptProperties
 * Integer	summaryHour
 * Integer	signatureThreshold
 * Boolean	hasDefaultEmployee
 * Integer	tipRateDefault
 * Boolean	onPaperTipSignatures
 * Boolean	noSignatureProgramEligible
 * Boolean	autoLogout
 * String	orderTitle
 * Integer	orderTitleMax
 * Boolean	resetOnReportingTime
 * Boolean	notesOnOrders
 * Boolean	deleteOrders
 * Boolean	removeTaxEnabled
 * Boolean	groupLineItems
 * Boolean	alternateInventoryNames
 * Boolean	autoPrint
 * String	hardwareProfile
 * Boolean	infoleaseSuppressBilling
 * Boolean	infoleaseSuppressPlanBilling
 * String	shippingAddress
 * Boolean	marketingEnabled
 * String	marketingPreferenceText
 * Integer	bankMarker
 * String	supportPhone
 * String	supportEmail
 * Boolean	manualCloseout
 * Boolean	manualCloseoutPerDevice
 * String	autoCloseoutTimezone
 * Boolean	showCloseoutOrders
 * Boolean	sendCloseoutEmail
 * Boolean	stayInCategory
 * String	locale
 * The locale of the merchant.
 * <p>
 * String	timezone
 * Boolean	vat
 * Whether this merchant is in a VAT country
 * <p>
 * String	vatTaxName
 * The VAT tax name that is shown on receipts
 * <p>
 * String	appBillingSystem
 * Temporary while we are switching US billing systems
 * <p>
 * String	abaAccountNumber
 * The ABA Account Number. Supplied by First Data.
 * <p>
 * String	ddaAccountNumber
 * The Masked DDA Account Number. Supplied by First Data.
 * <p>
 * Boolean	trackStock
 * Boolean	updateStock
 * Boolean	allowClockOutWithOpenOrders
 * Boolean	logInClockInPrompt
 * String	accountType
 * String	businessTypeCode
 * The business type of the merchant
 * <p>
 * Integer	pinLength
 * Boolean	cashBackEnabled
 * Whether cash back is enabled for this merchant
 * <p>
 * String	cashBackOptions
 * List of cash back possible cash back amounts
 * <p>
 * Integer	maxCashBack
 * The maximum amount of cash back that the customer can select.
 * <p>
 * String	hierarchy
 * Merchant hierarchy (Business, Bank, Agent, Corp, Chain). Supplied by First Data.
 * <p>
 * Boolean	hasConsented
 * Whether or not merchant has consented to infolease billing
 * <p>
 * String	merchantBoardingStatus
 * This is used to identify if the merchant is self-boarded
 * <p>
 * Boolean	alwaysRequireSignature
 * Whether we always require a signature for most transactions (excluding contactless under cvm limit)
 * <p>
 * Boolean	printedFirstDataReceiptLogoEnabled
 * Whether we display the First Data receipt logo
 * <p>
 * String	privacyPolicyMode
 * Designates the privacy policy mode for this merchant (EU_GDPR_STRICT, EU_GDPR_FLEX, US_HIPPA)
 * <p>
 * String	merchantPrivacyPolicyUrl
 * Merchant-specified privacy policy url
 * <p>
 * Boolean	disablePrintTaxesPaymentOnReceipts
 */

@JsonDeserialize(builder = MerchantProperties.Builder.class)
public class MerchantProperties {

    private final String defaultCurrency;
    private final Boolean tipsEnabled;
    private final Integer maxTipPercentage;
    private final String receiptProperties;
    private final Integer summaryHour;
    private final Long signatureThreshold;
    private final Boolean hasDefaultEmployee;
    private final Integer tipRateDefault;
    private final Boolean onPaperTipSignatures;
    private final Boolean noSignatureProgramEligible;
    private final Boolean autoLogout;
    private final String orderTitle;
    private final Integer orderTitleMax;
    private final Boolean resetOnReportingTime;
    private final Boolean notesOnOrders;
    private final Boolean deleteOrders;
    private final Boolean removeTaxEnabled;
    private final Boolean groupLineItems;
    private final Boolean alternateInventoryNames;
    private final Boolean autoPrint;
    private final String hardwareProfile;
    private final Boolean infoleaseSuppressBilling;
    private final Boolean infoleaseSuppressPlanBilling;
    private final String shippingAddress;
    private final Boolean marketingEnabled;
    private final String marketingPreferenceText;
    private final Integer bankMarker;
    private final String supportPhone;
    private final String supportEmail;
    private final Boolean manualCloseout;
    private final Boolean manualCloseoutPerDevice;
    private final String autoCloseoutTimezone;
    private final Boolean showCloseoutOrders;
    private final Boolean sendCloseoutEmail;
    private final Boolean stayInCategory;
    private final String locale;
    private final String timezone;
    private final Boolean vat;
    private final String vatTaxName;
    private final String appBillingSystem;
    private final String abaAccountNumber;
    private final String ddaAccountNumber;
    private final Boolean trackStock;
    private final Boolean updateStock;
    private final Boolean allowClockOutWithOpenOrders;
    private final Boolean logInClockInPrompt;
    private final String accountType;
    private final String businessTypeCode;
    private final Integer pinLength;
    private final Boolean cashBackEnabled;
    private final String cashBackOptions;
    private final Integer maxCashBack;
    private final String hierarchy;
    private final Boolean hasConsented;
    private final String merchantBoardingStatus;
    private final Boolean alwaysRequireSignature;
    private final Boolean printedFirstDataReceiptLogoEnabled;
    private final String privacyPolicyMode;
    private final String merchantPrivacyPolicyUrl;
    private final Boolean disablePrintTaxesPaymentOnReceipts;

    private MerchantProperties(Builder builder) {
        defaultCurrency = builder.defaultCurrency;
        tipsEnabled = builder.tipsEnabled;
        maxTipPercentage = builder.maxTipPercentage;
        receiptProperties = builder.receiptProperties;
        summaryHour = builder.summaryHour;
        signatureThreshold = builder.signatureThreshold;
        hasDefaultEmployee = builder.hasDefaultEmployee;
        tipRateDefault = builder.tipRateDefault;
        onPaperTipSignatures = builder.onPaperTipSignatures;
        noSignatureProgramEligible = builder.noSignatureProgramEligible;
        autoLogout = builder.autoLogout;
        orderTitle = builder.orderTitle;
        orderTitleMax = builder.orderTitleMax;
        resetOnReportingTime = builder.resetOnReportingTime;
        notesOnOrders = builder.notesOnOrders;
        deleteOrders = builder.deleteOrders;
        removeTaxEnabled = builder.removeTaxEnabled;
        groupLineItems = builder.groupLineItems;
        alternateInventoryNames = builder.alternateInventoryNames;
        autoPrint = builder.autoPrint;
        hardwareProfile = builder.hardwareProfile;
        infoleaseSuppressBilling = builder.infoleaseSuppressBilling;
        infoleaseSuppressPlanBilling = builder.infoleaseSuppressPlanBilling;
        shippingAddress = builder.shippingAddress;
        marketingEnabled = builder.marketingEnabled;
        marketingPreferenceText = builder.marketingPreferenceText;
        bankMarker = builder.bankMarker;
        supportPhone = builder.supportPhone;
        supportEmail = builder.supportEmail;
        manualCloseout = builder.manualCloseout;
        manualCloseoutPerDevice = builder.manualCloseoutPerDevice;
        autoCloseoutTimezone = builder.autoCloseoutTimezone;
        showCloseoutOrders = builder.showCloseoutOrders;
        sendCloseoutEmail = builder.sendCloseoutEmail;
        stayInCategory = builder.stayInCategory;
        locale = builder.locale;
        timezone = builder.timezone;
        vat = builder.vat;
        vatTaxName = builder.vatTaxName;
        appBillingSystem = builder.appBillingSystem;
        abaAccountNumber = builder.abaAccountNumber;
        ddaAccountNumber = builder.ddaAccountNumber;
        trackStock = builder.trackStock;
        updateStock = builder.updateStock;
        allowClockOutWithOpenOrders = builder.allowClockOutWithOpenOrders;
        logInClockInPrompt = builder.logInClockInPrompt;
        accountType = builder.accountType;
        businessTypeCode = builder.businessTypeCode;
        pinLength = builder.pinLength;
        cashBackEnabled = builder.cashBackEnabled;
        cashBackOptions = builder.cashBackOptions;
        maxCashBack = builder.maxCashBack;
        hierarchy = builder.hierarchy;
        hasConsented = builder.hasConsented;
        merchantBoardingStatus = builder.merchantBoardingStatus;
        alwaysRequireSignature = builder.alwaysRequireSignature;
        printedFirstDataReceiptLogoEnabled = builder.printedFirstDataReceiptLogoEnabled;
        privacyPolicyMode = builder.privacyPolicyMode;
        merchantPrivacyPolicyUrl = builder.merchantPrivacyPolicyUrl;
        disablePrintTaxesPaymentOnReceipts = builder.disablePrintTaxesPaymentOnReceipts;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public Boolean getTipsEnabled() {
        return tipsEnabled;
    }

    public Integer getMaxTipPercentage() {
        return maxTipPercentage;
    }

    public String getReceiptProperties() {
        return receiptProperties;
    }

    public Integer getSummaryHour() {
        return summaryHour;
    }

    public Long getSignatureThreshold() {
        return signatureThreshold;
    }

    public Boolean getHasDefaultEmployee() {
        return hasDefaultEmployee;
    }

    public Integer getTipRateDefault() {
        return tipRateDefault;
    }

    public Boolean getOnPaperTipSignatures() {
        return onPaperTipSignatures;
    }

    public Boolean getNoSignatureProgramEligible() {
        return noSignatureProgramEligible;
    }

    public Boolean getAutoLogout() {
        return autoLogout;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public Integer getOrderTitleMax() {
        return orderTitleMax;
    }

    public Boolean getResetOnReportingTime() {
        return resetOnReportingTime;
    }

    public Boolean getNotesOnOrders() {
        return notesOnOrders;
    }

    public Boolean getDeleteOrders() {
        return deleteOrders;
    }

    public Boolean getRemoveTaxEnabled() {
        return removeTaxEnabled;
    }

    public Boolean getGroupLineItems() {
        return groupLineItems;
    }

    public Boolean getAlternateInventoryNames() {
        return alternateInventoryNames;
    }

    public Boolean getAutoPrint() {
        return autoPrint;
    }

    public String getHardwareProfile() {
        return hardwareProfile;
    }

    public Boolean getInfoleaseSuppressBilling() {
        return infoleaseSuppressBilling;
    }

    public Boolean getInfoleaseSuppressPlanBilling() {
        return infoleaseSuppressPlanBilling;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public Boolean getMarketingEnabled() {
        return marketingEnabled;
    }

    public String getMarketingPreferenceText() {
        return marketingPreferenceText;
    }

    public Integer getBankMarker() {
        return bankMarker;
    }

    public String getSupportPhone() {
        return supportPhone;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public Boolean getManualCloseout() {
        return manualCloseout;
    }

    public Boolean getManualCloseoutPerDevice() {
        return manualCloseoutPerDevice;
    }

    public String getAutoCloseoutTimezone() {
        return autoCloseoutTimezone;
    }

    public Boolean getShowCloseoutOrders() {
        return showCloseoutOrders;
    }

    public Boolean getSendCloseoutEmail() {
        return sendCloseoutEmail;
    }

    public Boolean getStayInCategory() {
        return stayInCategory;
    }

    public String getLocale() {
        return locale;
    }

    public String getTimezone() {
        return timezone;
    }

    public Boolean getVat() {
        return vat;
    }

    public String getVatTaxName() {
        return vatTaxName;
    }

    public String getAppBillingSystem() {
        return appBillingSystem;
    }

    public String getAbaAccountNumber() {
        return abaAccountNumber;
    }

    public String getDdaAccountNumber() {
        return ddaAccountNumber;
    }

    public Boolean getTrackStock() {
        return trackStock;
    }

    public Boolean getUpdateStock() {
        return updateStock;
    }

    public Boolean getAllowClockOutWithOpenOrders() {
        return allowClockOutWithOpenOrders;
    }

    public Boolean getLogInClockInPrompt() {
        return logInClockInPrompt;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getBusinessTypeCode() {
        return businessTypeCode;
    }

    public Integer getPinLength() {
        return pinLength;
    }

    public Boolean getCashBackEnabled() {
        return cashBackEnabled;
    }

    public String getCashBackOptions() {
        return cashBackOptions;
    }

    public Integer getMaxCashBack() {
        return maxCashBack;
    }

    public String getHierarchy() {
        return hierarchy;
    }

    public Boolean getHasConsented() {
        return hasConsented;
    }

    public String getMerchantBoardingStatus() {
        return merchantBoardingStatus;
    }

    public Boolean getAlwaysRequireSignature() {
        return alwaysRequireSignature;
    }

    public Boolean getPrintedFirstDataReceiptLogoEnabled() {
        return printedFirstDataReceiptLogoEnabled;
    }

    public String getPrivacyPolicyMode() {
        return privacyPolicyMode;
    }

    public String getMerchantPrivacyPolicyUrl() {
        return merchantPrivacyPolicyUrl;
    }

    public Boolean getDisablePrintTaxesPaymentOnReceipts() {
        return disablePrintTaxesPaymentOnReceipts;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String defaultCurrency;
        private Boolean tipsEnabled;
        private Integer maxTipPercentage;
        private String receiptProperties;
        private Integer summaryHour;
        private Long signatureThreshold;
        private Boolean hasDefaultEmployee;
        private Integer tipRateDefault;
        private Boolean onPaperTipSignatures;
        private Boolean noSignatureProgramEligible;
        private Boolean autoLogout;
        private String orderTitle;
        private Integer orderTitleMax;
        private Boolean resetOnReportingTime;
        private Boolean notesOnOrders;
        private Boolean deleteOrders;
        private Boolean removeTaxEnabled;
        private Boolean groupLineItems;
        private Boolean alternateInventoryNames;
        private Boolean autoPrint;
        private String hardwareProfile;
        private Boolean infoleaseSuppressBilling;
        private Boolean infoleaseSuppressPlanBilling;
        private String shippingAddress;
        private Boolean marketingEnabled;
        private String marketingPreferenceText;
        private Integer bankMarker;
        private String supportPhone;
        private String supportEmail;
        private Boolean manualCloseout;
        private Boolean manualCloseoutPerDevice;
        private String autoCloseoutTimezone;
        private Boolean showCloseoutOrders;
        private Boolean sendCloseoutEmail;
        private Boolean stayInCategory;
        private String locale;
        private String timezone;
        private Boolean vat;
        private String vatTaxName;
        private String appBillingSystem;
        private String abaAccountNumber;
        private String ddaAccountNumber;
        private Boolean trackStock;
        private Boolean updateStock;
        private Boolean allowClockOutWithOpenOrders;
        private Boolean logInClockInPrompt;
        private String accountType;
        private String businessTypeCode;
        private Integer pinLength;
        private Boolean cashBackEnabled;
        private String cashBackOptions;
        private Integer maxCashBack;
        private String hierarchy;
        private Boolean hasConsented;
        private String merchantBoardingStatus;
        private Boolean alwaysRequireSignature;
        private Boolean printedFirstDataReceiptLogoEnabled;
        private String privacyPolicyMode;
        private String merchantPrivacyPolicyUrl;
        private Boolean disablePrintTaxesPaymentOnReceipts;

        private Builder() {
        }

        public Builder defaultCurrency(String val) {
            defaultCurrency = val;
            return this;
        }

        public Builder tipsEnabled(Boolean val) {
            tipsEnabled = val;
            return this;
        }

        public Builder maxTipPercentage(Integer val) {
            maxTipPercentage = val;
            return this;
        }

        public Builder receiptProperties(String val) {
            receiptProperties = val;
            return this;
        }

        public Builder summaryHour(Integer val) {
            summaryHour = val;
            return this;
        }

        public Builder signatureThreshold(Long val) {
            signatureThreshold = val;
            return this;
        }

        public Builder hasDefaultEmployee(Boolean val) {
            hasDefaultEmployee = val;
            return this;
        }

        public Builder tipRateDefault(Integer val) {
            tipRateDefault = val;
            return this;
        }

        public Builder onPaperTipSignatures(Boolean val) {
            onPaperTipSignatures = val;
            return this;
        }

        public Builder noSignatureProgramEligible(Boolean val) {
            noSignatureProgramEligible = val;
            return this;
        }

        public Builder autoLogout(Boolean val) {
            autoLogout = val;
            return this;
        }

        public Builder orderTitle(String val) {
            orderTitle = val;
            return this;
        }

        public Builder orderTitleMax(Integer val) {
            orderTitleMax = val;
            return this;
        }

        public Builder resetOnReportingTime(Boolean val) {
            resetOnReportingTime = val;
            return this;
        }

        public Builder notesOnOrders(Boolean val) {
            notesOnOrders = val;
            return this;
        }

        public Builder deleteOrders(Boolean val) {
            deleteOrders = val;
            return this;
        }

        public Builder removeTaxEnabled(Boolean val) {
            removeTaxEnabled = val;
            return this;
        }

        public Builder groupLineItems(Boolean val) {
            groupLineItems = val;
            return this;
        }

        public Builder alternateInventoryNames(Boolean val) {
            alternateInventoryNames = val;
            return this;
        }

        public Builder autoPrint(Boolean val) {
            autoPrint = val;
            return this;
        }

        public Builder hardwareProfile(String val) {
            hardwareProfile = val;
            return this;
        }

        public Builder infoleaseSuppressBilling(Boolean val) {
            infoleaseSuppressBilling = val;
            return this;
        }

        public Builder infoleaseSuppressPlanBilling(Boolean val) {
            infoleaseSuppressPlanBilling = val;
            return this;
        }

        public Builder shippingAddress(String val) {
            shippingAddress = val;
            return this;
        }

        public Builder marketingEnabled(Boolean val) {
            marketingEnabled = val;
            return this;
        }

        public Builder marketingPreferenceText(String val) {
            marketingPreferenceText = val;
            return this;
        }

        public Builder bankMarker(Integer val) {
            bankMarker = val;
            return this;
        }

        public Builder supportPhone(String val) {
            supportPhone = val;
            return this;
        }

        public Builder supportEmail(String val) {
            supportEmail = val;
            return this;
        }

        public Builder manualCloseout(Boolean val) {
            manualCloseout = val;
            return this;
        }

        public Builder manualCloseoutPerDevice(Boolean val) {
            manualCloseoutPerDevice = val;
            return this;
        }

        public Builder autoCloseoutTimezone(String val) {
            autoCloseoutTimezone = val;
            return this;
        }

        public Builder showCloseoutOrders(Boolean val) {
            showCloseoutOrders = val;
            return this;
        }

        public Builder sendCloseoutEmail(Boolean val) {
            sendCloseoutEmail = val;
            return this;
        }

        public Builder stayInCategory(Boolean val) {
            stayInCategory = val;
            return this;
        }

        public Builder locale(String val) {
            locale = val;
            return this;
        }

        public Builder timezone(String val) {
            timezone = val;
            return this;
        }

        public Builder vat(Boolean val) {
            vat = val;
            return this;
        }

        public Builder vatTaxName(String val) {
            vatTaxName = val;
            return this;
        }

        public Builder appBillingSystem(String val) {
            appBillingSystem = val;
            return this;
        }

        public Builder abaAccountNumber(String val) {
            abaAccountNumber = val;
            return this;
        }

        public Builder ddaAccountNumber(String val) {
            ddaAccountNumber = val;
            return this;
        }

        public Builder trackStock(Boolean val) {
            trackStock = val;
            return this;
        }

        public Builder updateStock(Boolean val) {
            updateStock = val;
            return this;
        }

        public Builder allowClockOutWithOpenOrders(Boolean val) {
            allowClockOutWithOpenOrders = val;
            return this;
        }

        public Builder logInClockInPrompt(Boolean val) {
            logInClockInPrompt = val;
            return this;
        }

        public Builder accountType(String val) {
            accountType = val;
            return this;
        }

        public Builder businessTypeCode(String val) {
            businessTypeCode = val;
            return this;
        }

        public Builder pinLength(Integer val) {
            pinLength = val;
            return this;
        }

        public Builder cashBackEnabled(Boolean val) {
            cashBackEnabled = val;
            return this;
        }

        public Builder cashBackOptions(String val) {
            cashBackOptions = val;
            return this;
        }

        public Builder maxCashBack(Integer val) {
            maxCashBack = val;
            return this;
        }

        public Builder hierarchy(String val) {
            hierarchy = val;
            return this;
        }

        public Builder hasConsented(Boolean val) {
            hasConsented = val;
            return this;
        }

        public Builder merchantBoardingStatus(String val) {
            merchantBoardingStatus = val;
            return this;
        }

        public Builder alwaysRequireSignature(Boolean val) {
            alwaysRequireSignature = val;
            return this;
        }

        public Builder printedFirstDataReceiptLogoEnabled(Boolean val) {
            printedFirstDataReceiptLogoEnabled = val;
            return this;
        }

        public Builder privacyPolicyMode(String val) {
            privacyPolicyMode = val;
            return this;
        }

        public Builder merchantPrivacyPolicyUrl(String val) {
            merchantPrivacyPolicyUrl = val;
            return this;
        }

        public Builder disablePrintTaxesPaymentOnReceipts(Boolean val) {
            disablePrintTaxesPaymentOnReceipts = val;
            return this;
        }

        public MerchantProperties build() {
            return new MerchantProperties(this);
        }
    }
}
