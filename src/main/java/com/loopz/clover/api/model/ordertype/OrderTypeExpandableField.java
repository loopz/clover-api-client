package com.loopz.clover.api.model.ordertype;

import com.loopz.clover.api.model.ExpandableField;

public enum OrderTypeExpandableField implements ExpandableField {

    HOURS("hours"),
    ATTRIBUTES("attributes"),
    CATEGORIES("categories");

    private final String field;

    OrderTypeExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
