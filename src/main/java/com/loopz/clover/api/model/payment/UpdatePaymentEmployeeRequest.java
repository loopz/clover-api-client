package com.loopz.clover.api.model.payment;

import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.order.OrderReference;

public class UpdatePaymentEmployeeRequest {

    private final EmployeeReference employee;
    private final OrderReference order;

    public UpdatePaymentEmployeeRequest(OrderReference order, EmployeeReference employee) {
        this.order = order;
        this.employee = employee;
    }

    public OrderReference getOrder() {
        return order;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }
}
