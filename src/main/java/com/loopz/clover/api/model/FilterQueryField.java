package com.loopz.clover.api.model;

public interface FilterQueryField {

    String getField();

}
