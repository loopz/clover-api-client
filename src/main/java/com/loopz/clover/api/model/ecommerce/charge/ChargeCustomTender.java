package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ChargeCustomTender.Builder.class)
public class ChargeCustomTender {

    private final String labelKey;
    private final String label;
    private final String id;

    private ChargeCustomTender(Builder builder) {
        labelKey = builder.labelKey;
        label = builder.label;
        id = builder.id;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("label_key")
    public String getLabelKey() {
        return labelKey;
    }

    public String getLabel() {
        return label;
    }

    public String getId() {
        return id;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String labelKey;
        private String label;
        private String id;

        public Builder() {
        }

        public Builder labelKey(String val) {
            labelKey = val;
            return this;
        }

        public Builder label(String val) {
            label = val;
            return this;
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public ChargeCustomTender build() {
            return new ChargeCustomTender(this);
        }
    }
}
