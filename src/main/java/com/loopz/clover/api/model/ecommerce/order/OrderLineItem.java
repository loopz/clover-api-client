package com.loopz.clover.api.model.ecommerce.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(builder = OrderLineItem.Builder.class)
public class OrderLineItem {

    private final Long amount;
    private final String currency;
    private final String description;
    private final String object;
    private final Long quantity;
    private final List<OrderLineItemTaxRate> taxrates;

    private OrderLineItem(Builder builder) {
        amount = builder.amount;
        currency = builder.currency;
        description = builder.description;
        object = builder.object;
        quantity = builder.quantity;
        taxrates = builder.taxrates;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDescription() {
        return description;
    }

    public String getObject() {
        return object;
    }

    public Long getQuantity() {
        return quantity;
    }

    public List<OrderLineItemTaxRate> getTaxrates() {
        return taxrates;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Long amount;
        private String currency;
        private String description;
        private String object;
        private Long quantity;
        private List<OrderLineItemTaxRate> taxrates = new ArrayList<>();

        private Builder() {
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder object(String val) {
            object = val;
            return this;
        }

        public Builder quantity(Long val) {
            quantity = val;
            return this;
        }

        @JsonProperty("tax_rates")
        public Builder taxrates(List<OrderLineItemTaxRate> val) {
            taxrates = val;
            return this;
        }

        public OrderLineItem build() {
            return new OrderLineItem(this);
        }
    }
}
