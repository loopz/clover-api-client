package com.loopz.clover.api.model;

public interface ExpandableField {

    String getField();

}
