package com.loopz.clover.api.model.notification;

public class AppNotificationRequest {

    private final String event;
    private final Long timeToLive;
    private final String data;

    private AppNotificationRequest(Builder builder) {
        event = builder.event;
        timeToLive = builder.timeToLive;
        data = builder.data;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getEvent() {
        return event;
    }

    public Long getTimeToLive() {
        return timeToLive;
    }

    public String getData() {
        return data;
    }

    public static final class Builder {
        private String event;
        private Long timeToLive;
        private String data;

        private Builder() {
        }

        public Builder event(String val) {
            event = val;
            return this;
        }

        public Builder timeToLive(Long val) {
            timeToLive = val;
            return this;
        }

        public Builder data(String val) {
            data = val;
            return this;
        }

        public AppNotificationRequest build() {
            return new AppNotificationRequest(this);
        }
    }
}
