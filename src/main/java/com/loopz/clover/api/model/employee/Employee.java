package com.loopz.clover.api.model.employee;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = Employee.Builder.class)
public class Employee {

    private final String id;
    private final String role;
    private final boolean inviteSent;
    private final String customId;
    private final String pin;
    private final boolean isOwner;
    private final DateTime claimedTime;
    private final String name;
    private final String nickname;
    private final String unhashedPin;
    private final DateTime deletedItem;
    private final String email;

    private Employee(Builder builder) {
        id = builder.id;
        role = builder.role;
        inviteSent = builder.inviteSent;
        customId = builder.customId;
        pin = builder.pin;
        isOwner = builder.isOwner;
        claimedTime = builder.claimedTime;
        name = builder.name;
        nickname = builder.nickname;
        unhashedPin = builder.unhashedPin;
        deletedItem = builder.deletedItem;
        email = builder.email;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public boolean isInviteSent() {
        return inviteSent;
    }

    public String getCustomId() {
        return customId;
    }

    public String getPin() {
        return pin;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public DateTime getClaimedTime() {
        return claimedTime;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getUnhashedPin() {
        return unhashedPin;
    }

    public DateTime getDeletedItem() {
        return deletedItem;
    }

    public String getEmail() {
        return email;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String role;
        private boolean inviteSent;
        private String customId;
        private String pin;
        private boolean isOwner;
        private DateTime claimedTime;
        private String name;
        private String nickname;
        private String unhashedPin;
        private DateTime deletedItem;
        private String email;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder role(String val) {
            role = val;
            return this;
        }

        public Builder inviteSent(boolean val) {
            inviteSent = val;
            return this;
        }

        public Builder customId(String val) {
            customId = val;
            return this;
        }

        public Builder pin(String val) {
            pin = val;
            return this;
        }

        public Builder isOwner(boolean val) {
            isOwner = val;
            return this;
        }

        public Builder claimedTime(DateTime val) {
            claimedTime = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder nickname(String val) {
            nickname = val;
            return this;
        }

        public Builder unhashedPin(String val) {
            unhashedPin = val;
            return this;
        }

        public Builder deletedItem(DateTime val) {
            deletedItem = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
