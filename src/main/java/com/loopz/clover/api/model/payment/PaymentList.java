package com.loopz.clover.api.model.payment;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class PaymentList {

    private List<Payment> elements;

    @JsonCreator
    public PaymentList(@JsonProperty("elements") List<Payment> elements) {
        this.elements = elements;
    }

    public List<Payment> getElements() {
        return elements;
    }
}
