package com.loopz.clover.api.model.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class OrderList {

    private final List<Order> elements;

    @JsonCreator
    public OrderList(@JsonProperty("elements") List<Order> elements) {
        this.elements = elements;
    }

    public List<Order> getElements() {
        return elements;
    }
}
