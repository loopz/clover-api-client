package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AppSubscription.Builder.class)
public class AppSubscription {

    private final App app;
    private final long amount;
    private final String id;
    private final String label;
    private final boolean plan;
    private final String name;
    private final String description;
    private final boolean active;

    private AppSubscription(Builder builder) {
        app = builder.app;
        amount = builder.amount;
        id = builder.id;
        label = builder.label;
        plan = builder.plan;
        name = builder.name;
        description = builder.description;
        active = builder.active;
    }

    public static Builder builder() {
        return new Builder();
    }

    public App getApp() {
        return app;
    }

    public long getAmount() {
        return amount;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public boolean isPlan() {
        return plan;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isActive() {
        return active;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private App app;
        private long amount;
        private String id;
        private String label;
        private boolean plan;
        private String name;
        private String description;
        private boolean active;

        private Builder() {
        }

        public Builder app(App val) {
            app = val;
            return this;
        }

        public Builder amount(long val) {
            amount = val;
            return this;
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder label(String val) {
            label = val;
            return this;
        }

        public Builder plan(boolean val) {
            plan = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder active(boolean val) {
            active = val;
            return this;
        }

        public AppSubscription build() {
            return new AppSubscription(this);
        }
    }
}
