package com.loopz.clover.api.model.ecommerce.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = OrderSource.Builder.class)
public class OrderSource {

    private final String last4;
    private final String first6;
    private final String expMonth;
    private final String expYear;
    private final String brand;
    private final String cvcCheck;

    private OrderSource(Builder builder) {
        last4 = builder.last4;
        first6 = builder.first6;
        expMonth = builder.expMonth;
        expYear = builder.expYear;
        brand = builder.brand;
        cvcCheck = builder.cvcCheck;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getLast4() {
        return last4;
    }

    public String getFirst6() {
        return first6;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public String getBrand() {
        return brand;
    }

    public String getCvcCheck() {
        return cvcCheck;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String last4;
        private String first6;
        private String expMonth;
        private String expYear;
        private String brand;
        private String cvcCheck;

        private Builder() {
        }

        public Builder last4(String val) {
            last4 = val;
            return this;
        }

        public Builder first6(String val) {
            first6 = val;
            return this;
        }

        @JsonProperty("exp_month")
        public Builder expMonth(String val) {
            expMonth = val;
            return this;
        }

        @JsonProperty("exp_year")
        public Builder expYear(String val) {
            expYear = val;
            return this;
        }

        public Builder brand(String val) {
            brand = val;
            return this;
        }

        @JsonProperty("cvc_check")
        public Builder cvcCheck(String val) {
            cvcCheck = val;
            return this;
        }

        public OrderSource build() {
            return new OrderSource(this);
        }
    }
}
