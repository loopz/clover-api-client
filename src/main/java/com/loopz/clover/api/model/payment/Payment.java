package com.loopz.clover.api.model.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.device.DeviceReference;
import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.order.Order;
import com.loopz.clover.api.model.refund.RefundList;
import com.loopz.clover.api.model.tender.TenderReference;
import org.joda.time.DateTime;

/**

 {
 "modifiedTime": "long",
 "note": "",
 "voidReason": "",
 "lineItemPayments": "--Expandable Field--",
 "germanInfo": "--Expandable Field--",
 "cashTendered": "long",
 "cardTransaction": "--Expandable Field--",
 "transactionSettings": {
 "disableCashBack": false,
 "tipSuggestions": [
 {
 "percentage": "long",
 "isEnabled": false,
 "name": "",
 "id": ""
 }
 ],
 "disableDuplicateCheck": false,
 "cloverShouldHandleReceipts": false,
 "returnResultOnTransactionComplete": false,
 "cardEntryMethods": "int",
 "disableRestartTransactionOnFailure": false,
 "forceOfflinePayment": false,
 "signatureEntryLocation": "",
 "tipMode": "",
 "allowOfflinePayment": false,
 "disableReceiptSelection": false,
 "autoAcceptSignature": false,
 "forcePinEntryOnSwipe": false,
 "signatureThreshold": "long",
 "tippableAmount": "long",
 "autoAcceptPaymentConfirmations": false,
 "approveOfflinePaymentWithoutPrompt": false
 },
 "cashbackAmount": "long",
 "cashAdvanceExtra": {
 "cashAdvanceCustomerIdentification": {
 "addressZipCode": "",
 "addressCountry": "",
 "idType": "",
 "serialNumber": "",
 "maskedSerialNumber": "",
 "issuingState": "",
 "addressState": "",
 "customerName": "",
 "issuingCountry": "",
 "addressStreet1": "",
 "addressStreet2": "",
 "encryptedSerialNumber": "",
 "expirationDate": "",
 "addressCity": ""
 },
 "cashAdvanceSerialNum": ""
 },
 "employee": "--Expandable Field--",
 "refunds": "--Expandable Field--",
 "result": "",
 "externalReferenceId": "--Expandable Field--",
 "offline": false,
 "serviceCharge": {
 "amount": "long",
 "name": "",
 "id": ""
 },
 "taxRates": "--Expandable Field--",
 "createdTime": "long",
 "externalPaymentId": "",
 "id": "",
 "order": "--Expandable Field--",
 "tender": "--Expandable Field--",
 "amount": "long",
 "signatureDisclaimer": {
 "disclaimerValues": "map",
 "disclaimerText": ""
 },
 "tipAmount": "long",
 "dccInfo": "--Expandable Field--",
 "transactionInfo": "--Expandable Field--",
 "clientCreatedTime": "long",
 "additionalCharges": [
 {
 "amount": "long",
 "id": "",
 "type": ""
 }
 ],
 "appTracking": "--Expandable Field--",
 "taxAmount": "long",
 "device": {
 "id": ""
 }
 }

 */

@JsonDeserialize(builder = Payment.Builder.class)
public class Payment {

    private final DateTime modifiedTime;
    private final DateTime createdTime;
    private final DateTime clientCreatedTime;
    private final String id;
    private final Long amount;
    private final Long taxAmount;
    private final PaymentResultType resultType;
    private final RefundList refunds;
    private final EmployeeReference employee;
    private final TenderReference tender;
    private final Order order;
    private final CardTransactionInfo cardTransactionInfo;
    private final DeviceReference device;
    private final String note;
    private final String externalPaymentId;
    private final boolean offline;

    private Payment(Builder builder) {
        modifiedTime = builder.modifiedTime;
        createdTime = builder.createdTime;
        clientCreatedTime = builder.clientCreatedTime;
        id = builder.id;
        amount = builder.amount;
        taxAmount = builder.taxAmount;
        resultType = builder.resultType;
        refunds = builder.refunds;
        employee = builder.employee;
        tender = builder.tender;
        order = builder.order;
        cardTransactionInfo = builder.cardTransactionInfo;
        device = builder.device;
        note = builder.note;
        externalPaymentId = builder.externalPaymentId;
        offline = builder.offline;
    }

    public static Builder builder() {
        return new Builder();
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public String getId() {
        return id;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public PaymentResultType getResultType() {
        return resultType;
    }

    public RefundList getRefunds() {
        return refunds;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }

    public TenderReference getTender() {
        return tender;
    }

    public Order getOrder() {
        return order;
    }

    public CardTransactionInfo getCardTransactionInfo() {
        return cardTransactionInfo;
    }

    public DeviceReference getDevice() {
        return device;
    }

    public DateTime getClientCreatedTime() {
        return clientCreatedTime;
    }

    public boolean isOffline() {
        return offline;
    }

    public String getNote() {
        return note;
    }

    public String getExternalPaymentId() {
        return externalPaymentId;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {

        private DateTime modifiedTime;
        private DateTime createdTime;
        private DateTime clientCreatedTime;
        private String id;
        private Long amount;
        private Long taxAmount;
        private PaymentResultType resultType;
        private RefundList refunds;
        private EmployeeReference employee;
        private TenderReference tender;
        private Order order;
        private CardTransactionInfo cardTransactionInfo;
        private DeviceReference device;
        private String note;
        private String externalPaymentId;
        private boolean offline;

        private Builder() {
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder clientCreatedTime(DateTime val) {
            clientCreatedTime = val;
            return this;
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        @JsonProperty("result")
        public Builder resultType(PaymentResultType val) {
            resultType = val;
            return this;
        }

        public Builder refunds(RefundList val) {
            refunds = val;
            return this;
        }

        public Builder employee(EmployeeReference val) {
            employee = val;
            return this;
        }

        public Builder tender(TenderReference val) {
            tender = val;
            return this;
        }

        public Builder order(Order val) {
            order = val;
            return this;
        }

        @JsonProperty("cardTransaction")
        public Builder cardTransactionInfo(CardTransactionInfo val) {
            cardTransactionInfo = val;
            return this;
        }

        public Builder device(DeviceReference val) {
            device = val;
            return this;
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public Builder externalPaymentId(String val) {
            externalPaymentId = val;
            return this;
        }

        public Builder offline(boolean val) {
            offline = val;
            return this;
        }

        public Payment build() {
            return new Payment(this);
        }
    }
}
