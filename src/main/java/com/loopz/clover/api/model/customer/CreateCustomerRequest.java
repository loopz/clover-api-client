package com.loopz.clover.api.model.customer;

import org.joda.time.DateTime;

import java.util.List;

public class CreateCustomerRequest {

    private final String firstName;
    private final String lastName;
    private final List<EmailAddress> emailAddresses;
    private final List<PhoneNumber> phoneNumbers;
    private boolean marketingAllowed;
    private DateTime customerSince;

    private CreateCustomerRequest(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        emailAddresses = builder.emailAddresses;
        phoneNumbers = builder.phoneNumbers;
        marketingAllowed = builder.marketingAllowed;
        customerSince = builder.customerSince;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public boolean isMarketingAllowed() {
        return marketingAllowed;
    }

    public DateTime getCustomerSince() {
        return customerSince;
    }

    public static final class Builder {
        private String firstName;
        private String lastName;
        private List<EmailAddress> emailAddresses;
        private List<PhoneNumber> phoneNumbers;
        private boolean marketingAllowed;
        private DateTime customerSince;

        private Builder() {
        }

        public Builder firstName(String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder emailAddresses(List<EmailAddress> val) {
            emailAddresses = val;
            return this;
        }

        public Builder phoneNumbers(List<PhoneNumber> val) {
            phoneNumbers = val;
            return this;
        }

        public Builder marketingAllowed(boolean val) {
            marketingAllowed = val;
            return this;
        }

        public Builder customerSince(DateTime val) {
            customerSince = val;
            return this;
        }

        public CreateCustomerRequest build() {
            return new CreateCustomerRequest(this);
        }
    }
}
