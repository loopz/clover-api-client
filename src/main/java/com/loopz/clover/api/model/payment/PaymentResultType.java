package com.loopz.clover.api.model.payment;

public enum PaymentResultType {

    SUCCESS, FAIL, INITIATED, VOIDED, VOIDING, VOID_FAILED, AUTH, AUTH_COMPLETED;
}
