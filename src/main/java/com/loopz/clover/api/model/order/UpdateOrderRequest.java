package com.loopz.clover.api.model.order;

import com.loopz.clover.api.model.customer.CustomerReferenceList;
import com.loopz.clover.api.model.employee.EmployeeReference;

public class UpdateOrderRequest {

    private final OrderState state;
    private final EmployeeReference employee;
    private final CustomerReferenceList customers;
    private final String note;

    private UpdateOrderRequest(Builder builder) {
        state = builder.state;
        employee = builder.employee;
        customers = builder.customers;
        note = builder.note;
    }

    public static Builder builder() {
        return new Builder();
    }

    public OrderState getState() {
        return state;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }

    public CustomerReferenceList getCustomers() {
        return customers;
    }

    public String getNote() {
        return note;
    }

    public static final class Builder {
        private OrderState state;
        private EmployeeReference employee;
        private CustomerReferenceList customers;
        private String note;

        private Builder() {
        }

        public Builder state(OrderState val) {
            state = val;
            return this;
        }

        public Builder employee(EmployeeReference val) {
            employee = val;
            return this;
        }

        public Builder customers(CustomerReferenceList val) {
            customers = val;
            return this;
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public UpdateOrderRequest build() {
            return new UpdateOrderRequest(this);
        }
    }
}
