package com.loopz.clover.api.model.item;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = ItemStock.Builder.class)
public class ItemStock {

    private ItemReference item;
    private Long stockCount;
    private Double quantity;
    private DateTime modifiedTime;

    private ItemStock(Builder builder) {
        item = builder.item;
        stockCount = builder.stockCount;
        quantity = builder.quantity;
        modifiedTime = builder.modifiedTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public ItemReference getItem() {
        return item;
    }

    public Long getStockCount() {
        return stockCount;
    }

    public Double getQuantity() {
        return quantity;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private ItemReference item;
        private Long stockCount;
        private Double quantity;
        private DateTime modifiedTime;

        private Builder() {
        }

        public Builder item(ItemReference val) {
            item = val;
            return this;
        }

        public Builder stockCount(Long val) {
            stockCount = val;
            return this;
        }

        public Builder quantity(Double val) {
            quantity = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public ItemStock build() {
            return new ItemStock(this);
        }
    }
}
