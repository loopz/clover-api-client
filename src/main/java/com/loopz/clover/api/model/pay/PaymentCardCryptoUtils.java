package com.loopz.clover.api.model.pay;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

public class PaymentCardCryptoUtils {

    private static final SecureRandom RANDOM = new SecureRandom();

    public static PublicKey getPublicKey(final BigInteger modulus, final BigInteger exponent) {
        try {
            final KeyFactory factory = KeyFactory.getInstance("RSA");
            final PublicKey publicKey = factory.generatePublic(new RSAPublicKeySpec(modulus, exponent));
            return publicKey;
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }
    public static String encryptCardNumber(final String prefix, final String cardNumber, PublicKey publicKey) {
        byte[] input = String.format("%s%s", prefix, cardNumber).getBytes();
        try {
            Cipher cipher = Cipher.getInstance("RSA/None/OAEPWithSHA1AndMGF1Padding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey, RANDOM);
            byte[] cipherText = cipher.doFinal(input);
            return DatatypeConverter.printBase64Binary(cipherText);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

}
