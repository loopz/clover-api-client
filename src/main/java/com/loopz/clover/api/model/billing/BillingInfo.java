package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = BillingInfo.Builder.class)
public class BillingInfo {

    private AppSubscription appSubscription;
    private DateTime billingStartTime;
    private long daysLapsed;
    private long amount;
    private boolean isInTrial;
    private boolean billable;
    private boolean appBillable;
    private boolean planBillable;
    private boolean appExportable;
    private boolean planExportable;
    private BillingStatus status;
    private SubscriptionCountryList subscriptionCountries;

    private BillingInfo(Builder builder) {
        appSubscription = builder.appSubscription;
        billingStartTime = builder.billingStartTime;
        daysLapsed = builder.daysLapsed;
        amount = builder.amount;
        isInTrial = builder.isInTrial;
        billable = builder.billable;
        appBillable = builder.appBillable;
        planBillable = builder.planBillable;
        appExportable = builder.appExportable;
        planExportable = builder.planExportable;
        status = builder.status;
        subscriptionCountries = builder.subscriptionCountries;
    }

    public static Builder builder() {
        return new Builder();
    }

    public AppSubscription getAppSubscription() {
        return appSubscription;
    }

    public DateTime getBillingStartTime() {
        return billingStartTime;
    }

    public long getDaysLapsed() {
        return daysLapsed;
    }

    public boolean isInTrial() {
        return isInTrial;
    }

    public BillingStatus getStatus() {
        return status;
    }

    public long getAmount() {
        return amount;
    }

    public boolean isBillable() {
        return billable;
    }

    public boolean isAppBillable() {
        return appBillable;
    }

    public boolean isPlanBillable() {
        return planBillable;
    }

    public boolean isAppExportable() {
        return appExportable;
    }

    public boolean isPlanExportable() {
        return planExportable;
    }

    public SubscriptionCountryList getSubscriptionCountries() {
        return subscriptionCountries;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private AppSubscription appSubscription;
        private DateTime billingStartTime;
        private long daysLapsed;
        private long amount;
        private boolean isInTrial;
        private boolean billable;
        private boolean appBillable;
        private boolean planBillable;
        private boolean appExportable;
        private boolean planExportable;
        private BillingStatus status;
        private SubscriptionCountryList subscriptionCountries;

        private Builder() {
        }

        public Builder appSubscription(AppSubscription val) {
            appSubscription = val;
            return this;
        }

        public Builder billingStartTime(DateTime val) {
            billingStartTime = val;
            return this;
        }

        public Builder daysLapsed(long val) {
            daysLapsed = val;
            return this;
        }

        public Builder amount(long val) {
            amount = val;
            return this;
        }

        public Builder isInTrial(boolean val) {
            isInTrial = val;
            return this;
        }

        public Builder billable(boolean val) {
            billable = val;
            return this;
        }

        public Builder appBillable(boolean val) {
            appBillable = val;
            return this;
        }

        public Builder planBillable(boolean val) {
            planBillable = val;
            return this;
        }

        public Builder appExportable(boolean val) {
            appExportable = val;
            return this;
        }

        public Builder planExportable(boolean val) {
            planExportable = val;
            return this;
        }

        public Builder status(BillingStatus val) {
            status = val;
            return this;
        }

        public Builder subscriptionCountries(SubscriptionCountryList val) {
            subscriptionCountries = val;
            return this;
        }

        public BillingInfo build() {
            return new BillingInfo(this);
        }
    }
}
