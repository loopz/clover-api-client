package com.loopz.clover.api.model.customer;

import java.util.List;

public class UpdateCustomerRequest {

    private final String firstName;
    private final String lastName;
    private final List<EmailAddress> emailAddresses;
    private final List<PhoneNumber> phoneNumbers;
    private boolean marketingAllowed;

    private UpdateCustomerRequest(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        emailAddresses = builder.emailAddresses;
        phoneNumbers = builder.phoneNumbers;
        marketingAllowed = builder.marketingAllowed;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public boolean isMarketingAllowed() {
        return marketingAllowed;
    }

    public static final class Builder {
        private String firstName;
        private String lastName;
        private List<EmailAddress> emailAddresses;
        private List<PhoneNumber> phoneNumbers;
        private boolean marketingAllowed;

        private Builder() {
        }

        public Builder firstName(String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder emailAddresses(List<EmailAddress> val) {
            emailAddresses = val;
            return this;
        }

        public Builder phoneNumbers(List<PhoneNumber> val) {
            phoneNumbers = val;
            return this;
        }

        public Builder marketingAllowed(boolean val) {
            marketingAllowed = val;
            return this;
        }

        public UpdateCustomerRequest build() {
            return new UpdateCustomerRequest(this);
        }
    }
}
