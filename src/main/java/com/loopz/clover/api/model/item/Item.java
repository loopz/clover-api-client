package com.loopz.clover.api.model.item;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.category.CategoryList;
import com.loopz.clover.api.model.taxrate.TaxRateList;
import org.joda.time.DateTime;

/**
 * "itemGroup": {
 * "id": ""
 * },
 * "modifiedTime": "long",
 * "code": "",
 * "cost": "long",
 * "hidden": false,
 * "unitName": "",
 * "priceType": "",
 * "alternateName": "",
 * "isRevenue": false,
 * "modifierGroups": "--Expandable Field--",
 * "tags": "--Expandable Field--",
 * "taxRates": "--Expandable Field--",
 * "itemStock": "--Expandable Field--",
 * "price": "long",
 * "options": "--Expandable Field--",
 * "name": "",
 * "id": "",
 * "categories": "--Expandable Field--",
 * "sku": "",
 * "deletedTime": "long",
 * "defaultTaxRates": false,
 * "stockCount": "long",
 * "priceWithoutVat": "long"
 */

@JsonDeserialize(builder = Item.Builder.class)
public class Item {

    private final String id;
    private final String code;
    private final Long price;
    private final Long priceWithoutVat;
    private final Long cost;
    private final boolean hidden;
    private final String name;
    private final String unitName;
    private final String alternativeName;
    private final String sku;
    private final PriceType priceType;
    private final boolean isRevenue;
    private final boolean defaultTaxRates;
    private final Long stockCount;
    private final DateTime createdTime;
    private final DateTime modifiedTime;
    private final DateTime deletedTime;
    private final CategoryList categories;
    private final TaxRateList taxRates;

    private Item(Builder builder) {
        id = builder.id;
        code = builder.code;
        price = builder.price;
        priceWithoutVat = builder.priceWithoutVat;
        cost = builder.cost;
        hidden = builder.hidden;
        name = builder.name;
        unitName = builder.unitName;
        alternativeName = builder.alternativeName;
        sku = builder.sku;
        priceType = builder.priceType;
        isRevenue = builder.isRevenue;
        defaultTaxRates = builder.defaultTaxRates;
        stockCount = builder.stockCount;
        createdTime = builder.createdTime;
        modifiedTime = builder.modifiedTime;
        deletedTime = builder.deletedTime;
        categories = builder.categories;
        taxRates = builder.taxRates;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Long getPrice() {
        return price;
    }

    public Long getPriceWithoutVat() {
        return priceWithoutVat;
    }

    public Long getCost() {
        return cost;
    }

    public boolean isHidden() {
        return hidden;
    }

    public String getName() {
        return name;
    }

    public String getUnitName() {
        return unitName;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public String getSku() {
        return sku;
    }

    public PriceType getPriceType() {
        return priceType;
    }

    public boolean isRevenue() {
        return isRevenue;
    }

    public boolean isDefaultTaxRates() {
        return defaultTaxRates;
    }

    public Long getStockCount() {
        return stockCount;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public DateTime getDeletedTime() {
        return deletedTime;
    }

    public CategoryList getCategories() {
        return categories;
    }

    public TaxRateList getTaxRates() {
        return taxRates;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String code;
        private Long price;
        private Long priceWithoutVat;
        private Long cost;
        private boolean hidden;
        private String name;
        private String unitName;
        private String alternativeName;
        private String sku;
        private PriceType priceType;
        private boolean isRevenue;
        private boolean defaultTaxRates;
        private Long stockCount;
        private DateTime createdTime;
        private DateTime modifiedTime;
        private DateTime deletedTime;
        private CategoryList categories;
        private TaxRateList taxRates;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder code(String val) {
            code = val;
            return this;
        }

        public Builder price(Long val) {
            price = val;
            return this;
        }

        public Builder priceWithoutVat(Long val) {
            priceWithoutVat = val;
            return this;
        }

        public Builder cost(Long val) {
            cost = val;
            return this;
        }

        public Builder hidden(boolean val) {
            hidden = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder unitName(String val) {
            unitName = val;
            return this;
        }

        public Builder alternativeName(String val) {
            alternativeName = val;
            return this;
        }

        public Builder sku(String val) {
            sku = val;
            return this;
        }

        public Builder priceType(PriceType val) {
            priceType = val;
            return this;
        }

        public Builder isRevenue(boolean val) {
            isRevenue = val;
            return this;
        }

        public Builder defaultTaxRates(boolean val) {
            defaultTaxRates = val;
            return this;
        }

        public Builder stockCount(Long val) {
            stockCount = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder deletedTime(DateTime val) {
            deletedTime = val;
            return this;
        }

        public Builder categories(CategoryList val) {
            categories = val;
            return this;
        }

        public Builder taxRates(TaxRateList val) {
            taxRates = val;
            return this;
        }

        public Item build() {
            return new Item(this);
        }
    }
}
