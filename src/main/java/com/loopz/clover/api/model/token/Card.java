package com.loopz.clover.api.model.token;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Card.Builder.class)
public class Card {

    private final String number;
    @JsonProperty("encrypted_pan")
    private final String encryptedPan;
    @JsonProperty("exp_month")
    private final String expMonth;
    @JsonProperty("exp_year")
    private final String expYear;
    private final String cvv;
    private final String last4;
    private final String first6;
    private final String country;
    private final String brand;
    private final String name;
    @JsonProperty("address_line1")
    private final String addressLine1;
    @JsonProperty("address_line2")
    private final String addressLine2;
    @JsonProperty("address_city")
    private final String addressCity;
    @JsonProperty("address_state")
    private final String addressState;
    @JsonProperty("address_zip")
    private final String addressZip;
    @JsonProperty("address_country")
    private final String addressCountry;

    private Card(Builder builder) {
        number = builder.number;
        encryptedPan = builder.encryptedPan;
        expMonth = builder.expMonth;
        expYear = builder.expYear;
        cvv = builder.cvv;
        last4 = builder.last4;
        first6 = builder.first6;
        country = builder.country;
        brand = builder.brand;
        name = builder.name;
        addressLine1 = builder.addressLine1;
        addressLine2 = builder.addressLine2;
        addressCity = builder.addressCity;
        addressState = builder.addressState;
        addressZip = builder.addressZip;
        addressCountry = builder.addressCountry;
    }

    public static final Builder builder() {
        return new Builder();
    }

    public String getNumber() {
        return number;
    }

    public String getEncryptedPan() {
        return encryptedPan;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public String getCvv() {
        return cvv;
    }

    public String getLast4() {
        return last4;
    }

    public String getFirst6() {
        return first6;
    }

    public String getCountry() {
        return country;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String number;
        private String encryptedPan;
        private String expMonth;
        private String expYear;
        private String cvv;
        private String last4;
        private String first6;
        private String country;
        private String brand;
        private String name;
        private String addressLine1;
        private String addressLine2;
        private String addressCity;
        private String addressState;
        private String addressZip;
        private String addressCountry;

        public Builder() {
        }

        public Builder number(String val) {
            number = val;
            return this;
        }

        public Builder encryptedPan(String val) {
            encryptedPan = val;
            return this;
        }

        public Builder expMonth(String val) {
            expMonth = val;
            return this;
        }

        public Builder expYear(String val) {
            expYear = val;
            return this;
        }

        public Builder cvv(String val) {
            cvv = val;
            return this;
        }

        public Builder last4(String val) {
            last4 = val;
            return this;
        }

        public Builder first6(String val) {
            first6 = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder brand(String val) {
            brand = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder addressLine1(String val) {
            addressLine1 = val;
            return this;
        }

        public Builder addressLine2(String val) {
            addressLine2 = val;
            return this;
        }

        public Builder addressCity(String val) {
            addressCity = val;
            return this;
        }

        public Builder addressState(String val) {
            addressState = val;
            return this;
        }

        public Builder addressZip(String val) {
            addressZip = val;
            return this;
        }

        public Builder addressCountry(String val) {
            addressCountry = val;
            return this;
        }

        public Card build() {
            return new Card(this);
        }
    }
}
