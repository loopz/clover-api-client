package com.loopz.clover.api.model.tender;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 "instructions": "",
 "visible": false,
 "editable": false,
 "id": "",
 "label": "",
 "labelKey": "",
 "opensCashDrawer": false,
 "supportsTipping": false,
 "enabled": false
 */

@JsonDeserialize(builder = Tender.Builder.class)
public class Tender {

    private final String id;
    private final String label;
    private final String labelKey;
    private final boolean opensCashDrawer;
    private final boolean supportsTipping;
    private final boolean enabled;
    private final boolean editable;
    private final boolean visible;
    private final String instructions;
    private final String href;

    private Tender(Builder builder) {
        id = builder.id;
        label = builder.label;
        labelKey = builder.labelKey;
        opensCashDrawer = builder.opensCashDrawer;
        supportsTipping = builder.supportsTipping;
        enabled = builder.enabled;
        editable = builder.editable;
        visible = builder.visible;
        instructions = builder.instructions;
        href = builder.href;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public boolean isOpensCashDrawer() {
        return opensCashDrawer;
    }

    public boolean isSupportsTipping() {
        return supportsTipping;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getInstructions() {
        return instructions;
    }

    public String getHref() {
        return href;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String label;
        private String labelKey;
        private boolean opensCashDrawer;
        private boolean supportsTipping;
        private boolean enabled;
        private boolean editable;
        private boolean visible;
        private String instructions;
        private String href;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder label(String val) {
            label = val;
            return this;
        }

        public Builder labelKey(String val) {
            labelKey = val;
            return this;
        }

        public Builder opensCashDrawer(boolean val) {
            opensCashDrawer = val;
            return this;
        }

        public Builder supportsTipping(boolean val) {
            supportsTipping = val;
            return this;
        }

        public Builder enabled(boolean val) {
            enabled = val;
            return this;
        }

        public Builder editable(boolean val) {
            editable = val;
            return this;
        }

        public Builder visible(boolean val) {
            visible = val;
            return this;
        }

        public Builder instructions(String val) {
            instructions = val;
            return this;
        }

        public Builder href(String val) {
            href = val;
            return this;
        }

        public Tender build() {
            return new Tender(this);
        }
    }
}
