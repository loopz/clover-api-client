package com.loopz.clover.api.model.ecommerce.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * "id" : "0AT3A6PAZXTX2",
 *   "object" : "order",
 *   "amount" : 5000,
 *   "amount_paid" : 5000,
 *   "currency" : "USD",
 *   "charge" : "1XQW62PBY3B26",
 *   "created" : 1625662663000,
 *   "email" : "michael@loopz.io",
 *   "ref_num" : "118800500010",
 *   "auth_code" : "OK6570",
 *   "items" : [ {
 *     "parent" : "S97S7N3ZPXTAT",
 *     "amount" : 5000,
 *     "description" : "Loopz Test Merchant Gift Card",
 *     "tax_rates" : [ {
 *       "name" : "NO_TAX_APPLIED"
 *     } ]
 *   } ],
 *   "source" : {
 *     "brand" : "DISCOVER",
 *     "cvc_check" : "pass",
 *     "exp_month" : "12",
 *     "exp_year" : "2022",
 *     "first6" : "601136",
 *     "last4" : "6668"
 *   },
 *   "status" : "paid",
 *   "status_transitions" : {
 *     "paid" : 1625662683134
 *   },
 *   "ecomind" : "ecom"
 */


@JsonDeserialize(builder = Order.Builder.class)
public class Order {

    private final String id;
    private final String email;
    private final Long amount;
    private final Long amountPaid;
    private final Long taxAmount;
    private final Long taxAmountPaid;
    private final Long tipAmount;
    private final Long surchargeAmount;
    private final Long amountReturned;
    private final String currency;
    private final String authCode;
    private final String refNum;
    private final String warningMessage;
    private final DateTime created;
    private final String defaultSource;
    private final String description;
    private final Map<String, String> metadata;
    private final String chargeId;
    private final OrderSource source;
    private final String status;
    private final List<OrderLineItem> items;

    private Order(Builder builder) {
        id = builder.id;
        email = builder.email;
        amount = builder.amount;
        amountPaid = builder.amountPaid;
        taxAmount = builder.taxAmount;
        taxAmountPaid = builder.taxAmountPaid;
        tipAmount = builder.tipAmount;
        surchargeAmount = builder.surchargeAmount;
        amountReturned = builder.amountReturned;
        currency = builder.currency;
        authCode = builder.authCode;
        refNum = builder.refNum;
        warningMessage = builder.warningMessage;
        created = builder.created;
        defaultSource = builder.defaultSource;
        description = builder.description;
        metadata = builder.metadata;
        chargeId = builder.chargeId;
        source = builder.source;
        status = builder.status;
        items = builder.items;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getAmountPaid() {
        return amountPaid;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public Long getTaxAmountPaid() {
        return taxAmountPaid;
    }

    public Long getTipAmount() {
        return tipAmount;
    }

    public Long getSurchargeAmount() {
        return surchargeAmount;
    }

    public Long getAmountReturned() {
        return amountReturned;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAuthCode() {
        return authCode;
    }

    public String getRefNum() {
        return refNum;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public DateTime getCreated() {
        return created;
    }

    public String getDefaultSource() {
        return defaultSource;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public String getChargeId() {
        return chargeId;
    }

    public OrderSource getSource() {
        return source;
    }

    public String getStatus() {
        return status;
    }

    public List<OrderLineItem> getItems() {
        return items;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String email;
        private Long amount;
        private Long amountPaid;
        private Long taxAmount;
        private Long taxAmountPaid;
        private Long tipAmount;
        private Long surchargeAmount;
        private Long amountReturned;
        private String currency;
        private String authCode;
        private String refNum;
        private String warningMessage;
        private DateTime created;
        private String defaultSource;
        private String description;
        private Map<String, String> metadata;
        private String chargeId;
        private OrderSource source;
        private String status;
        private List<OrderLineItem> items = new ArrayList<>();

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        @JsonProperty("amount_paid")
        public Builder amountPaid(Long val) {
            amountPaid = val;
            return this;
        }

        @JsonProperty("tax_amount")
        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        @JsonProperty("tax_amount_paid")
        public Builder taxAmountPaid(Long val) {
            taxAmountPaid = val;
            return this;
        }

        @JsonProperty("tip_amount")
        public Builder tipAmount(Long val) {
            tipAmount = val;
            return this;
        }

        @JsonProperty("surcharge_amount")
        public Builder surchargeAmount(Long val) {
            surchargeAmount = val;
            return this;
        }

        @JsonProperty("amount_returned")
        public Builder amountReturned(Long val) {
            amountReturned = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        @JsonProperty("auth_code")
        public Builder authCode(String val) {
            authCode = val;
            return this;
        }

        @JsonProperty("ref_num")
        public Builder refNum(String val) {
            refNum = val;
            return this;
        }

        @JsonProperty("warning_message")
        public Builder warningMessage(String val) {
            warningMessage = val;
            return this;
        }

        public Builder created(DateTime val) {
            created = val;
            return this;
        }

        @JsonProperty("default_source")
        public Builder defaultSource(String val) {
            defaultSource = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder metadata(Map<String, String> val) {
            metadata = val;
            return this;
        }

        @JsonProperty("charge")
        public Builder chargeId(String val) {
            chargeId = val;
            return this;
        }

        public Builder source(OrderSource val) {
            source = val;
            return this;
        }

        public Builder status(String val) {
            status = val;
            return this;
        }

        public Builder items(List<OrderLineItem> val) {
            items = val;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }
}
