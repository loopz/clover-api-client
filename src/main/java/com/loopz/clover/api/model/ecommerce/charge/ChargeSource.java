package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ChargeSource.Builder.class)
public class ChargeSource {

    private final String id;
    private final String object;
    private final String addressLine1;
    private final String addressLine1Check;
    private final String addressLine2;
    private final String addressCity;
    private final String addressState;
    private final String addressZip;
    private final String addressZipCheck;
    private final String addressCountry;
    private final String country;
    private final String brand;
    private final String cvcCheck;
    private final String expMonth;
    private final String expYear;
    private final String fingerprint;
    private final String first6;
    private final String last4;
    private final String name;
    private final String funding;

    private ChargeSource(Builder builder) {
        id = builder.id;
        object = builder.object;
        addressLine1 = builder.addressLine1;
        addressLine1Check = builder.addressLine1Check;
        addressLine2 = builder.addressLine2;
        addressCity = builder.addressCity;
        addressState = builder.addressState;
        addressZip = builder.addressZip;
        addressZipCheck = builder.addressZipCheck;
        addressCountry = builder.addressCountry;
        country = builder.country;
        brand = builder.brand;
        cvcCheck = builder.cvcCheck;
        expMonth = builder.expMonth;
        expYear = builder.expYear;
        fingerprint = builder.fingerprint;
        first6 = builder.first6;
        last4 = builder.last4;
        name = builder.name;
        funding = builder.funding;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getObject() {
        return object;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine1Check() {
        return addressLine1Check;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public String getAddressZipCheck() {
        return addressZipCheck;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public String getCountry() {
        return country;
    }

    public String getBrand() {
        return brand;
    }

    public String getCvcCheck() {
        return cvcCheck;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public String getFirst6() {
        return first6;
    }

    public String getLast4() {
        return last4;
    }

    public String getName() {
        return name;
    }

    public String getFunding() {
        return funding;
    }

    @Override
    public String toString() {
        return "ChargeSource{" +
                "id='" + id + '\'' +
                ", object='" + object + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine1Check='" + addressLine1Check + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", addressCity='" + addressCity + '\'' +
                ", addressState='" + addressState + '\'' +
                ", addressZip='" + addressZip + '\'' +
                ", addressZipCheck='" + addressZipCheck + '\'' +
                ", addressCountry='" + addressCountry + '\'' +
                ", country='" + country + '\'' +
                ", brand='" + brand + '\'' +
                ", cvcCheck='" + cvcCheck + '\'' +
                ", expMonth='" + expMonth + '\'' +
                ", expYear='" + expYear + '\'' +
                ", fingerprint='" + fingerprint + '\'' +
                ", first6='" + first6 + '\'' +
                ", last4='" + last4 + '\'' +
                ", name='" + name + '\'' +
                ", funding='" + funding + '\'' +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String object;
        private String addressLine1;
        private String addressLine1Check;
        private String addressLine2;
        private String addressCity;
        private String addressState;
        private String addressZip;
        private String addressZipCheck;
        private String addressCountry;
        private String country;
        private String brand;
        private String cvcCheck;
        private String expMonth;
        private String expYear;
        private String fingerprint;
        private String first6;
        private String last4;
        private String name;
        private String funding;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder object(String val) {
            object = val;
            return this;
        }

        @JsonProperty("address_line1")
        public Builder addressLine1(String val) {
            addressLine1 = val;
            return this;
        }

        @JsonProperty("address_line1_check")
        public Builder addressLine1Check(String val) {
            addressLine1Check = val;
            return this;
        }

        @JsonProperty("address_line2")
        public Builder addressLine2(String val) {
            addressLine2 = val;
            return this;
        }

        @JsonProperty("address_city")
        public Builder addressCity(String val) {
            addressCity = val;
            return this;
        }

        @JsonProperty("address_state")
        public Builder addressState(String val) {
            addressState = val;
            return this;
        }

        @JsonProperty("address_zip")
        public Builder addressZip(String val) {
            addressZip = val;
            return this;
        }

        @JsonProperty("address_zip_check")
        public Builder addressZipCheck(String val) {
            addressZipCheck = val;
            return this;
        }

        @JsonProperty("address_country")
        public Builder addressCountry(String val) {
            addressCountry = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder brand(String val) {
            brand = val;
            return this;
        }

        @JsonProperty("exp_check")
        public Builder cvcCheck(String val) {
            cvcCheck = val;
            return this;
        }

        @JsonProperty("exp_month")
        public Builder expMonth(String val) {
            expMonth = val;
            return this;
        }

        @JsonProperty("exp_year")
        public Builder expYear(String val) {
            expYear = val;
            return this;
        }

        public Builder fingerprint(String val) {
            fingerprint = val;
            return this;
        }

        public Builder first6(String val) {
            first6 = val;
            return this;
        }

        public Builder last4(String val) {
            last4 = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder funding(String val) {
            funding = val;
            return this;
        }

        public ChargeSource build() {
            return new ChargeSource(this);
        }
    }
}
