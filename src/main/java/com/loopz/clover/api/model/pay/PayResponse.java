package com.loopz.clover.api.model.pay;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 {
 "authCode" : String
 "failureMessage" : String
 "token" : String
 "result" : (APPROVED|DECLINED)
 "avsResult" : (SUCCESS|ZIP_CODE_MATCH|ZIP_CODE_MATCH_ADDRESS_NOT_CHECKED|ADDRESS_MATCH|ADDRESS_MATCH_ZIP_NOT_CHECKED|NEITHER_MATCH|SERVICE_FAILURE|SERVICE_UNAVAILABLE|NOT_CHECKED|ZIP_CODE_NOT_MATCHED_ADDRESS_NOT_CHECKED|ADDRESS_NOT_MATCHED_ZIP_CODE_NOT_CHECKED)
 "paymentId" : String
 "cvvResult" : (SUCCESS|FAILURE|NOT_PROCESSED|NOT_PRESENT)
 }
 */
@JsonDeserialize(builder = PayResponse.Builder.class)
public class PayResponse {

    private final String paymentId;
    private final String authCode;
    private final String failureMessage;
    private final String token;
    private final PayResultType result;
    private final AVSResultType avsResult;
    private final CVVResultType cvvResult;

    private PayResponse(Builder builder) {
        paymentId = builder.paymentId;
        authCode = builder.authCode;
        failureMessage = builder.failureMessage;
        token = builder.token;
        result = builder.result;
        avsResult = builder.avsResult;
        cvvResult = builder.cvvResult;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public String getToken() {
        return token;
    }

    public PayResultType getResult() {
        return result;
    }

    public AVSResultType getAvsResult() {
        return avsResult;
    }

    public CVVResultType getCvvResult() {
        return cvvResult;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String paymentId;
        private String authCode;
        private String failureMessage;
        private String token;
        private PayResultType result;
        private AVSResultType avsResult;
        private CVVResultType cvvResult;

        private Builder() {
        }

        public Builder paymentId(String val) {
            paymentId = val;
            return this;
        }

        public Builder authCode(String val) {
            authCode = val;
            return this;
        }

        public Builder failureMessage(String val) {
            failureMessage = val;
            return this;
        }

        public Builder token(String val) {
            token = val;
            return this;
        }

        public Builder result(PayResultType val) {
            result = val;
            return this;
        }

        public Builder avsResult(AVSResultType val) {
            avsResult = val;
            return this;
        }

        public Builder cvvResult(CVVResultType val) {
            cvvResult = val;
            return this;
        }

        public PayResponse build() {
            return new PayResponse(this);
        }
    }
}
