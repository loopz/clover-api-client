package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.clover.api.model.order.OrderReference;

import java.util.List;

public class CustomerReferenceList {

    private final List<CustomerReference> elements;

    @JsonCreator
    public CustomerReferenceList(@JsonProperty("elements") List<CustomerReference> elements) {
        this.elements = elements;
    }

    public List<CustomerReference> getElements() {
        return elements;
    }
}
