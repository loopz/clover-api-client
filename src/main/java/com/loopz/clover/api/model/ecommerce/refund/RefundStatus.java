package com.loopz.clover.api.model.ecommerce.refund;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum RefundStatus {

    PENDING("pending"),
    SUCCEEDED("succeeded"),
    FAILED("failed"),
    CANCELED("canceled");

    private final String value;

    @JsonCreator
    RefundStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
