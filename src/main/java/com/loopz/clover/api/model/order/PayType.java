package com.loopz.clover.api.model.order;

public enum PayType {

    SPLIT_GUEST ,
    SPLIT_ITEM,
    SPLIT_CUSTOM,
    FULL;

}
