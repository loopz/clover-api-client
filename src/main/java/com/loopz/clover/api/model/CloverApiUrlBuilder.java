package com.loopz.clover.api.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CloverApiUrlBuilder {

    private final String url;
    private List<ExpandableField> expandableFields = new ArrayList<>();
    private List<FilterQuery> filterQueries = new ArrayList<>();
    private int limit = 1000;
    private int offset = 0;


    public CloverApiUrlBuilder(String url) {
        this.url = url;
    }

    public CloverApiUrlBuilder addField(ExpandableField expandableField) {
        expandableFields.add(expandableField);
        return this;
    }

    public CloverApiUrlBuilder addFields(ExpandableField... expandableFields) {
        this.expandableFields.addAll(Arrays.asList(expandableFields));
        return this;
    }

    public CloverApiUrlBuilder addFilterQuery(FilterQuery filterQuery) {
        filterQueries.add(filterQuery);
        return this;
    }

    public CloverApiUrlBuilder addFilterQueries(List<? extends FilterQuery> filterQueries) {
        this.filterQueries.addAll(filterQueries);
        return this;
    }

    public CloverApiUrlBuilder limit(int limit) {
        if(limit >= 1 && limit <= 1000) {
            this.limit = limit;
        }
        return this;
    }

    public CloverApiUrlBuilder offset(int offset) {
        if(offset >= 0) {
            this.offset = offset;
        }
        return this;
    }

    public String build() {
        StringBuilder builder = new StringBuilder(url);

        int queryFieldsAdded = 0;

        builder.append("?limit=").append(limit)
        .append("&offset=").append(offset);

        if (expandableFields.isEmpty() && filterQueries.isEmpty()) {
            return builder.toString();
        }

        builder.append("&");

        Iterator<FilterQuery> filterQueryIterator = filterQueries.iterator();
        while (filterQueryIterator.hasNext()) {
            FilterQuery filterQuery = filterQueryIterator.next();

            String filterQueryValue = filterQuery.getField().getField() +
                    filterQuery.getOperator().getOperator() +
                    filterQuery.getValue();

            builder.append("filter=")
                    .append(encodeQueryValue(filterQueryValue));

            if (filterQueryIterator.hasNext()) {
                builder.append("&");
            }
            queryFieldsAdded++;
        }

        if (expandableFields.isEmpty()) {
            return builder.toString();

        }

        builder.append(queryFieldsAdded > 0 ? "&" : "")
                .append("expand=");

        Iterator<ExpandableField> expandableFieldIterator = expandableFields.iterator();
        while (expandableFieldIterator.hasNext()) {
            ExpandableField expandableField = expandableFieldIterator.next();
            builder.append(expandableField.getField());
            if (expandableFieldIterator.hasNext()) {
                builder.append(",");
            }
        }

        return builder.toString();
    }

    private String encodeQueryValue(String value) {
        return value;
//        try {
//            return URLEncoder.encode(String.valueOf(value), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            throw new RuntimeException(e);
//        }
    }

}
