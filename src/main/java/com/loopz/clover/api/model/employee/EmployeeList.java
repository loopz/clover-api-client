package com.loopz.clover.api.model.employee;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class EmployeeList {

    private final List<Employee> elements;

    @JsonCreator
    public EmployeeList(@JsonProperty("elements") List<Employee> elements) {
        this.elements = elements;
    }

    public List<Employee> getElements() {
        return elements;
    }
}
