package com.loopz.clover.api.model.order;

public class CreateOrderRequest {

    private final Boolean taxRemoved;
    private final String title;
    private final String note;

    private CreateOrderRequest(Builder builder) {
        taxRemoved = builder.taxRemoved;
        title = builder.title;
        note = builder.note;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Boolean getTaxRemoved() {
        return taxRemoved;
    }

    public String getTitle() {
        return title;
    }

    public String getNote() {
        return note;
    }

    public static final class Builder {
        private Boolean taxRemoved;
        private String title;
        private String note;

        private Builder() {
        }

        public Builder taxRemoved(Boolean val) {
            taxRemoved = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public CreateOrderRequest build() {
            return new CreateOrderRequest(this);
        }
    }
}
