package com.loopz.clover.api.model.merchant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Logo {

    private final String type;
    private final String url;

    @JsonCreator
    public Logo(@JsonProperty("type") String type, @JsonProperty("type") String url) {
        this.type = type;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }
}
