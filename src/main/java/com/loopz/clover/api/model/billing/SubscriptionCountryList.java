package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class SubscriptionCountryList {

    private final List<SubscriptionCountry> elements;

    @JsonCreator
    public SubscriptionCountryList(List<SubscriptionCountry> elements) {
        this.elements = elements;
    }

    public List<SubscriptionCountry> getElements() {
        return elements;
    }
}
