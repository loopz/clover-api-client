package com.loopz.clover.api.model.billing;

public enum BillingStatus {

    ACTIVE,
    LAPSED,
    INACTIVE,
    SUPPRESSED;

}
