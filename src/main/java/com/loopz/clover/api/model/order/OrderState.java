package com.loopz.clover.api.model.order;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OrderState {

    @JsonProperty("open")
    OPEN,
    @JsonProperty("paid")
    PAID,
    @JsonProperty("locked")
    LOCKED;

}
