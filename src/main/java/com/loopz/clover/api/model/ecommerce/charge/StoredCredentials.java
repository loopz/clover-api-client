package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = StoredCredentials.Builder.class)
public class StoredCredentials {

    private StoredCredentials(Builder builder) {
        sequence = builder.sequence;
        isScheduled = builder.isScheduled;
        cardbrandOriginalAmount = builder.cardbrandOriginalAmount;
        initiator = builder.initiator;
        installmentInfo = builder.installmentInfo;
    }

    public enum Sequence {
        FIRST,
        SUBSEQUENT;
    }

    public enum Initiator {
        MERCHANT,
        CARDHOLDER;
    }

    private final Sequence sequence;
    private final boolean isScheduled;
    private final String cardbrandOriginalAmount;
    private final Initiator initiator;
    private final InstallmentInfo installmentInfo;

    public static Builder builder() {
        return new Builder();
    }

    public Sequence getSequence() {
        return sequence;
    }

    @JsonProperty("is_scheduled")
    public boolean isScheduled() {
        return isScheduled;
    }

    @JsonProperty("cardbrand_original_amount")
    public String getCardbrandOriginalAmount() {
        return cardbrandOriginalAmount;
    }

    public Initiator getInitiator() {
        return initiator;
    }

    @JsonProperty("installment_info")
    public InstallmentInfo getInstallmentInfo() {
        return installmentInfo;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Sequence sequence;
        private boolean isScheduled;
        private String cardbrandOriginalAmount;
        private Initiator initiator;
        private InstallmentInfo installmentInfo;

        public Builder() {
        }

        public Builder sequence(Sequence val) {
            sequence = val;
            return this;
        }

        public Builder isScheduled(boolean val) {
            isScheduled = val;
            return this;
        }

        public Builder cardbrandOriginalAmount(String val) {
            cardbrandOriginalAmount = val;
            return this;
        }

        public Builder initiator(Initiator val) {
            initiator = val;
            return this;
        }

        public Builder installmentInfo(InstallmentInfo val) {
            installmentInfo = val;
            return this;
        }

        public StoredCredentials build() {
            return new StoredCredentials(this);
        }
    }
}
