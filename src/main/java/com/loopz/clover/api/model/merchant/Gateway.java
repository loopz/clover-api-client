package com.loopz.clover.api.model.merchant;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * {
 "acquiringBackEnd": "",
 "authorizationFrontEnd": "",
 "backendMid": "",
 "accountName": "",
 "production": false,
 "groupId": "",
 "paymentProcessorName": "",
 "mid": "",
 "fns": "",
 "storeId": "",
 "mcc": "",
 "debitKeyCode": "",
 "supportsMultiPayToken": false,
 "tid": "",
 "altMid": "",
 "supportsNakedCredit": false,
 "closingTime": "",
 "paymentGatewayApi": "",
 "supportsTipAdjust": false,
 "frontendMid": "",
 "tokenType": "",
 "supportsTipping": false,
 "newBatchCloseEnabled": false
 }
 */
@JsonDeserialize(builder = Gateway.Builder.class)
public class Gateway {

    private final String acquiringBackEnd;
    private final String authorizationFrontEnd;
    private final String backendMid;
    private final String accountName;
    private final boolean production;
    private final String groupId;
    private final String paymentProcessorName;
    private final String mid;
    private final String fns;
    private final String storeId;
    private final String debitKeyCode;
    private final boolean supportsMultiPayToken;
    private final String tid;
    private final String altMid;
    private final boolean supportsNakedCredit;
    private final String closingTime;
    private final String paymentGatewayApi;
    private final boolean supportsTipAdjust;
    private final String frontendMid;
    private final String tokenType;
    private final boolean supportsTipping;
    private final boolean newBatchCloseEnabled;

    private Gateway(Builder builder) {
        acquiringBackEnd = builder.acquiringBackEnd;
        authorizationFrontEnd = builder.authorizationFrontEnd;
        backendMid = builder.backendMid;
        accountName = builder.accountName;
        production = builder.production;
        groupId = builder.groupId;
        paymentProcessorName = builder.paymentProcessorName;
        mid = builder.mid;
        fns = builder.fns;
        storeId = builder.storeId;
        debitKeyCode = builder.debitKeyCode;
        supportsMultiPayToken = builder.supportsMultiPayToken;
        tid = builder.tid;
        altMid = builder.altMid;
        supportsNakedCredit = builder.supportsNakedCredit;
        closingTime = builder.closingTime;
        paymentGatewayApi = builder.paymentGatewayApi;
        supportsTipAdjust = builder.supportsTipAdjust;
        frontendMid = builder.frontendMid;
        tokenType = builder.tokenType;
        supportsTipping = builder.supportsTipping;
        newBatchCloseEnabled = builder.newBatchCloseEnabled;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getAcquiringBackEnd() {
        return acquiringBackEnd;
    }

    public String getAuthorizationFrontEnd() {
        return authorizationFrontEnd;
    }

    public String getBackendMid() {
        return backendMid;
    }

    public String getAccountName() {
        return accountName;
    }

    public boolean isProduction() {
        return production;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getPaymentProcessorName() {
        return paymentProcessorName;
    }

    public String getMid() {
        return mid;
    }

    public String getFns() {
        return fns;
    }

    public String getStoreId() {
        return storeId;
    }

    public String getDebitKeyCode() {
        return debitKeyCode;
    }

    public boolean isSupportsMultiPayToken() {
        return supportsMultiPayToken;
    }

    public String getTid() {
        return tid;
    }

    public String getAltMid() {
        return altMid;
    }

    public boolean isSupportsNakedCredit() {
        return supportsNakedCredit;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public String getPaymentGatewayApi() {
        return paymentGatewayApi;
    }

    public boolean isSupportsTipAdjust() {
        return supportsTipAdjust;
    }

    public String getFrontendMid() {
        return frontendMid;
    }

    public String getTokenType() {
        return tokenType;
    }

    public boolean isSupportsTipping() {
        return supportsTipping;
    }

    public boolean isNewBatchCloseEnabled() {
        return newBatchCloseEnabled;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String acquiringBackEnd;
        private String authorizationFrontEnd;
        private String backendMid;
        private String accountName;
        private boolean production;
        private String groupId;
        private String paymentProcessorName;
        private String mid;
        private String fns;
        private String storeId;
        private String debitKeyCode;
        private boolean supportsMultiPayToken;
        private String tid;
        private String altMid;
        private boolean supportsNakedCredit;
        private String closingTime;
        private String paymentGatewayApi;
        private boolean supportsTipAdjust;
        private String frontendMid;
        private String tokenType;
        private boolean supportsTipping;
        private boolean newBatchCloseEnabled;

        private Builder() {
        }

        public Builder acquiringBackEnd(String val) {
            acquiringBackEnd = val;
            return this;
        }

        public Builder authorizationFrontEnd(String val) {
            authorizationFrontEnd = val;
            return this;
        }

        public Builder backendMid(String val) {
            backendMid = val;
            return this;
        }

        public Builder accountName(String val) {
            accountName = val;
            return this;
        }

        public Builder production(boolean val) {
            production = val;
            return this;
        }

        public Builder groupId(String val) {
            groupId = val;
            return this;
        }

        public Builder paymentProcessorName(String val) {
            paymentProcessorName = val;
            return this;
        }

        public Builder mid(String val) {
            mid = val;
            return this;
        }

        public Builder fns(String val) {
            fns = val;
            return this;
        }

        public Builder storeId(String val) {
            storeId = val;
            return this;
        }

        public Builder debitKeyCode(String val) {
            debitKeyCode = val;
            return this;
        }

        public Builder supportsMultiPayToken(boolean val) {
            supportsMultiPayToken = val;
            return this;
        }

        public Builder tid(String val) {
            tid = val;
            return this;
        }

        public Builder altMid(String val) {
            altMid = val;
            return this;
        }

        public Builder supportsNakedCredit(boolean val) {
            supportsNakedCredit = val;
            return this;
        }

        public Builder closingTime(String val) {
            closingTime = val;
            return this;
        }

        public Builder paymentGatewayApi(String val) {
            paymentGatewayApi = val;
            return this;
        }

        public Builder supportsTipAdjust(boolean val) {
            supportsTipAdjust = val;
            return this;
        }

        public Builder frontendMid(String val) {
            frontendMid = val;
            return this;
        }

        public Builder tokenType(String val) {
            tokenType = val;
            return this;
        }

        public Builder supportsTipping(boolean val) {
            supportsTipping = val;
            return this;
        }

        public Builder newBatchCloseEnabled(boolean val) {
            newBatchCloseEnabled = val;
            return this;
        }

        public Gateway build() {
            return new Gateway(this);
        }
    }
}
