package com.loopz.clover.api.model.customer;

import com.loopz.clover.api.model.FilterQuery;
import com.loopz.clover.api.model.QueryOperators;

public class CustomerFilterQuery extends FilterQuery<CustomerFilterQueryField> {

    public CustomerFilterQuery(CustomerFilterQueryField field, QueryOperators operator, Object value) {
        super(field, operator, value);
    }
}
