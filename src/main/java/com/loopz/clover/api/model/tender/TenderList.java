package com.loopz.clover.api.model.tender;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.List;

@JsonDeserialize(builder = TenderList.Builder.class)
public class TenderList {

    private final List<Tender> elements;

    private TenderList(Builder builder) {
        elements = builder.elements;
    }

    public static Builder builder() {
        return new Builder();
    }

    public List<Tender> getElements() {
        return elements;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private List<Tender> elements;

        private Builder() {
        }

        public Builder elements(List<Tender> val) {
            elements = val;
            return this;
        }

        public TenderList build() {
            return new TenderList(this);
        }
    }
}
