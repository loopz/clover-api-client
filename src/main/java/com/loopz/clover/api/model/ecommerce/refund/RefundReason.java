package com.loopz.clover.api.model.ecommerce.refund;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum RefundReason {

    DUPLICATE("duplicate"),
    FRAUDULENT("fraudulent"),
    REQUESTED_BY_CUSTOMER("requested_by_customer");

    private final String value;

    @JsonCreator
    RefundReason(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}