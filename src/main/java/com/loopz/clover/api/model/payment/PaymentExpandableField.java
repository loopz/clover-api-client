package com.loopz.clover.api.model.payment;

import com.loopz.clover.api.model.ExpandableField;

public enum PaymentExpandableField  implements ExpandableField {

    CARD_TRANSACTION("cardTransaction"),
    REFUNDS("refunds"),
    ORDER("order"),
    TRANSACTION_INFO("transactionInfo"),
    DCC_INFO("dccInfo"),
    TENDER("tender"),
    EXTERNAL_REFERENCE_ID("externalReferenceId"),
    TAX_RATES("taxRates"),
    LINE_ITEM_PAYMENTS("lineItemPayments"),
    APP_TRACKING("appTracking"),
    EMPLOYEE("employee");

    private final String field;

    PaymentExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
