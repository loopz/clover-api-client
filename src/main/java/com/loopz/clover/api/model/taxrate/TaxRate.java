package com.loopz.clover.api.model.taxrate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.item.ItemList;
import org.joda.time.DateTime;

/**
 "modifiedTime": "long",
 "isDefault": false,
 "rate": "long",
 "name": "",
 "id": "",
 "taxAmount": "long",
 "deletedTime": "long",
 "items": "--Expandable Field--",
 "taxType": ""
 */
@JsonDeserialize(builder = TaxRate.Builder.class)
public class TaxRate {

    private final String id;
    private final String name;
    private final Long rate;
    private final Long taxAmount;
    private final TaxType taxType;
    private final boolean isDefault;
    private final DateTime modifiedTime;
    private final DateTime deletedTime;
    private final ItemList items;

    private TaxRate(Builder builder) {
        id = builder.id;
        name = builder.name;
        rate = builder.rate;
        taxAmount = builder.taxAmount;
        taxType = builder.taxType;
        isDefault = builder.isDefault;
        modifiedTime = builder.modifiedTime;
        deletedTime = builder.deletedTime;
        items = builder.items;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getRate() {
        return rate;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public TaxType getTaxType() {
        return taxType;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public DateTime getDeletedTime() {
        return deletedTime;
    }

    public ItemList getItems() {
        return items;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private Long rate;
        private Long taxAmount;
        private TaxType taxType;
        private boolean isDefault;
        private DateTime modifiedTime;
        private DateTime deletedTime;
        private ItemList items;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder rate(Long val) {
            rate = val;
            return this;
        }

        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        public Builder taxType(TaxType val) {
            taxType = val;
            return this;
        }

        public Builder isDefault(boolean val) {
            isDefault = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder deletedTime(DateTime val) {
            deletedTime = val;
            return this;
        }

        public Builder items(ItemList val) {
            items = val;
            return this;
        }

        public TaxRate build() {
            return new TaxRate(this);
        }
    }
}
