package com.loopz.clover.api.model.token;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = TokenizedCard.Builder.class)
public class TokenizedCard {

    private final String id;
    private final String object;
    private final Card card;
    private final DateTime created;
    private final String type;

    private TokenizedCard(Builder builder) {
        id = builder.id;
        object = builder.object;
        card = builder.card;
        created = builder.created;
        type = builder.type;
    }

    public String getId() {
        return id;
    }

    public String getObject() {
        return object;
    }

    public Card getCard() {
        return card;
    }

    public DateTime getCreated() {
        return created;
    }

    public String getType() {
        return type;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String object;
        private Card card;
        private DateTime created;
        private String type;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder object(String val) {
            object = val;
            return this;
        }

        public Builder card(Card val) {
            card = val;
            return this;
        }

        public Builder created(DateTime val) {
            created = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public TokenizedCard build() {
            return new TokenizedCard(this);
        }
    }
}
