package com.loopz.clover.api.model.taxrate;

public enum TaxType {

    VAT_TAXABLE,
    VAT_NON_TAXABLE,
    VAT_EXEMPT,
    INTERNAL_TAX;

}
