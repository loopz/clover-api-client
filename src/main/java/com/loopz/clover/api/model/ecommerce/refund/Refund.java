package com.loopz.clover.api.model.ecommerce.refund;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.Map;

public class Refund {

    private final String id;
    private final Long amount;
    private final String currency;
    private final String externalReferenceId;
    private final RefundStatus status;
    private final RefundReason reason;
    private final String failureReason;
    private final String description;
    private final DateTime created;
    private final Map<String, String> metadata;

    private Refund(Builder builder) {
        id = builder.id;
        amount = builder.amount;
        currency = builder.currency;
        externalReferenceId = builder.externalReferenceId;
        status = builder.status;
        reason = builder.reason;
        failureReason = builder.failureReason;
        description = builder.description;
        created = builder.created;
        metadata = builder.metadata;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public Long getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public com.loopz.clover.api.model.ecommerce.refund.RefundStatus getStatus() {
        return status;
    }

    public com.loopz.clover.api.model.ecommerce.refund.RefundReason getReason() {
        return reason;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public String getDescription() {
        return description;
    }

    public DateTime getCreated() {
        return created;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public static final class Builder {
        private String id;
        private Long amount;
        private String currency;
        private String externalReferenceId;
        private RefundStatus status;
        private RefundReason reason;
        private String failureReason;
        private String description;
        private DateTime created;
        private Map<String, String> metadata;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        @JsonProperty("external_reference_id")
        public Builder externalReferenceId(String val) {
            externalReferenceId = val;
            return this;
        }

        public Builder status(RefundStatus val) {
            status = val;
            return this;
        }

        public Builder reason(RefundReason val) {
            reason = val;
            return this;
        }

        @JsonProperty("failure_reason")
        public Builder failureReason(String val) {
            failureReason = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public Builder created(DateTime val) {
            created = val;
            return this;
        }

        public Builder metadata(Map<String, String> val) {
            metadata = val;
            return this;
        }

        public Refund build() {
            return new Refund(this);
        }
    }
}
