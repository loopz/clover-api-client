package com.loopz.clover.api.model.taxrate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class TaxRateList {

    private final List<TaxRate> elements;

    @JsonCreator
    public TaxRateList(@JsonProperty("elements") List<TaxRate> elements) {
        this.elements = elements;
    }

    public List<TaxRate> getElements() {
        return elements;
    }
}
