package com.loopz.clover.api.model.device;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.merchant.Merchant;

/**
 * String	id
 * Unique identifier
 *
 * String	name
 * Name of the device (if entered)
 *
 * String	model
 * Object	merchant
 * String	merchant.id
 * Unique identifier
 *
 * String	orderPrefix
 * A prefix that will be applied to order numbers. This is useful if the merchant and/or customer needs to track which device an order came from.
 *
 * String	terminalId
 * The merchant device's terminal Id. FD-IPG sets the merchant_device terminal Id for LATAM devices
 *
 * Integer	terminalPrefix
 * String	serial
 * The device's serial number.
 *
 * String	imei
 * The IMEI of the device
 *
 * String	imsi
 * The IMSI of the SIM in the device (if present)
 *
 * String	simIccid
 * The ICCID of the SIM in the device (if present)
 *
 * String	deviceCertificate
 * String	pedCertificate
 * String	deviceTypeName
 * String	productName
 * Boolean	pinDisabled
 * Whether this device has PIN prompt disabled.
 *
 * Boolean	offlinePayments
 * Boolean	offlinePaymentsAll
 * Integer	offlinePaymentsLimit
 * Integer	offlinePaymentsPromptThreshold
 * Integer	offlinePaymentsTotalPaymentsLimit
 * Integer	offlinePaymentsLimitDefault
 * Integer	offlinePaymentsPromptThresholdDefault
 * Integer	offlinePaymentsTotalPaymentsLimitDefault
 * Integer	offlinePaymentsMaxLimit
 * Integer	offlinePaymentsMaxTotalPaymentsLimit
 * Boolean	showOfflinePayments
 * Integer	maxOfflineDays
 * Boolean	allowStoreAndForward
 * [Object]	secureReports
 * String	secureReports[].id
 * Unique identifier
 *
 * String	bundleIndicator
 */

@JsonDeserialize(builder = Device.Builder.class)
public class Device {

    private final String id;
    private final String name;
    private final String model;
    private final Merchant merchant;
    private final String orderPrefix;
    private final String terminalId;
    private final int terminalPrefix;
    private final String serial;
    private final String imei;
    private final String imsi;
    private final String simIccid;
    private final String deviceCertificate;
    private final String pedCertificate;
    private final String deviceTypeName;
    private final String productName;
    private final boolean pinDisabled;
    private final boolean offlinePayments;
    private final boolean offlinePaymentsAll;
    private final int offlinePaymentsLimit;
    private final int offlinePaymentsPromptThreshold;
    private final int offlinePaymentsTotalPaymentsLimit;
    private final int offlinePaymentsLimitDefault;
    private final int offlinePaymentsPromptThresholdDefault;
    private final int offlinePaymentsTotalPaymentsLimitDefault;
    private final int offlinePaymentsMaxLimit;
    private final int offlinePaymentsMaxTotalPaymentsLimit;
    private final boolean showOfflinePayments;
    private final int maxOfflineDays;
    private final boolean allowStoreAndForward;
    private final String bundleIndicator;

    private Device(Builder builder) {
        id = builder.id;
        name = builder.name;
        model = builder.model;
        merchant = builder.merchant;
        orderPrefix = builder.orderPrefix;
        terminalId = builder.terminalId;
        terminalPrefix = builder.terminalPrefix;
        serial = builder.serial;
        imei = builder.imei;
        imsi = builder.imsi;
        simIccid = builder.simIccid;
        deviceCertificate = builder.deviceCertificate;
        pedCertificate = builder.pedCertificate;
        deviceTypeName = builder.deviceTypeName;
        productName = builder.productName;
        pinDisabled = builder.pinDisabled;
        offlinePayments = builder.offlinePayments;
        offlinePaymentsAll = builder.offlinePaymentsAll;
        offlinePaymentsLimit = builder.offlinePaymentsLimit;
        offlinePaymentsPromptThreshold = builder.offlinePaymentsPromptThreshold;
        offlinePaymentsTotalPaymentsLimit = builder.offlinePaymentsTotalPaymentsLimit;
        offlinePaymentsLimitDefault = builder.offlinePaymentsLimitDefault;
        offlinePaymentsPromptThresholdDefault = builder.offlinePaymentsPromptThresholdDefault;
        offlinePaymentsTotalPaymentsLimitDefault = builder.offlinePaymentsTotalPaymentsLimitDefault;
        offlinePaymentsMaxLimit = builder.offlinePaymentsMaxLimit;
        offlinePaymentsMaxTotalPaymentsLimit = builder.offlinePaymentsMaxTotalPaymentsLimit;
        showOfflinePayments = builder.showOfflinePayments;
        maxOfflineDays = builder.maxOfflineDays;
        allowStoreAndForward = builder.allowStoreAndForward;
        bundleIndicator = builder.bundleIndicator;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public String getOrderPrefix() {
        return orderPrefix;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public int getTerminalPrefix() {
        return terminalPrefix;
    }

    public String getSerial() {
        return serial;
    }

    public String getImei() {
        return imei;
    }

    public String getImsi() {
        return imsi;
    }

    public String getSimIccid() {
        return simIccid;
    }

    public String getDeviceCertificate() {
        return deviceCertificate;
    }

    public String getPedCertificate() {
        return pedCertificate;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public String getProductName() {
        return productName;
    }

    public boolean isPinDisabled() {
        return pinDisabled;
    }

    public boolean isOfflinePayments() {
        return offlinePayments;
    }

    public boolean isOfflinePaymentsAll() {
        return offlinePaymentsAll;
    }

    public int getOfflinePaymentsLimit() {
        return offlinePaymentsLimit;
    }

    public int getOfflinePaymentsPromptThreshold() {
        return offlinePaymentsPromptThreshold;
    }

    public int getOfflinePaymentsTotalPaymentsLimit() {
        return offlinePaymentsTotalPaymentsLimit;
    }

    public int getOfflinePaymentsLimitDefault() {
        return offlinePaymentsLimitDefault;
    }

    public int getOfflinePaymentsPromptThresholdDefault() {
        return offlinePaymentsPromptThresholdDefault;
    }

    public int getOfflinePaymentsTotalPaymentsLimitDefault() {
        return offlinePaymentsTotalPaymentsLimitDefault;
    }

    public int getOfflinePaymentsMaxLimit() {
        return offlinePaymentsMaxLimit;
    }

    public int getOfflinePaymentsMaxTotalPaymentsLimit() {
        return offlinePaymentsMaxTotalPaymentsLimit;
    }

    public boolean isShowOfflinePayments() {
        return showOfflinePayments;
    }

    public int getMaxOfflineDays() {
        return maxOfflineDays;
    }

    public boolean isAllowStoreAndForward() {
        return allowStoreAndForward;
    }

    public String getBundleIndicator() {
        return bundleIndicator;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String model;
        private Merchant merchant;
        private String orderPrefix;
        private String terminalId;
        private int terminalPrefix;
        private String serial;
        private String imei;
        private String imsi;
        private String simIccid;
        private String deviceCertificate;
        private String pedCertificate;
        private String deviceTypeName;
        private String productName;
        private boolean pinDisabled;
        private boolean offlinePayments;
        private boolean offlinePaymentsAll;
        private int offlinePaymentsLimit;
        private int offlinePaymentsPromptThreshold;
        private int offlinePaymentsTotalPaymentsLimit;
        private int offlinePaymentsLimitDefault;
        private int offlinePaymentsPromptThresholdDefault;
        private int offlinePaymentsTotalPaymentsLimitDefault;
        private int offlinePaymentsMaxLimit;
        private int offlinePaymentsMaxTotalPaymentsLimit;
        private boolean showOfflinePayments;
        private int maxOfflineDays;
        private boolean allowStoreAndForward;
        private String bundleIndicator;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder model(String val) {
            model = val;
            return this;
        }

        public Builder merchant(Merchant val) {
            merchant = val;
            return this;
        }

        public Builder orderPrefix(String val) {
            orderPrefix = val;
            return this;
        }

        public Builder terminalId(String val) {
            terminalId = val;
            return this;
        }

        public Builder terminalPrefix(int val) {
            terminalPrefix = val;
            return this;
        }

        public Builder serial(String val) {
            serial = val;
            return this;
        }

        public Builder imei(String val) {
            imei = val;
            return this;
        }

        public Builder imsi(String val) {
            imsi = val;
            return this;
        }

        public Builder simIccid(String val) {
            simIccid = val;
            return this;
        }

        public Builder deviceCertificate(String val) {
            deviceCertificate = val;
            return this;
        }

        public Builder pedCertificate(String val) {
            pedCertificate = val;
            return this;
        }

        public Builder deviceTypeName(String val) {
            deviceTypeName = val;
            return this;
        }

        public Builder productName(String val) {
            productName = val;
            return this;
        }

        public Builder pinDisabled(boolean val) {
            pinDisabled = val;
            return this;
        }

        public Builder offlinePayments(boolean val) {
            offlinePayments = val;
            return this;
        }

        public Builder offlinePaymentsAll(boolean val) {
            offlinePaymentsAll = val;
            return this;
        }

        public Builder offlinePaymentsLimit(int val) {
            offlinePaymentsLimit = val;
            return this;
        }

        public Builder offlinePaymentsPromptThreshold(int val) {
            offlinePaymentsPromptThreshold = val;
            return this;
        }

        public Builder offlinePaymentsTotalPaymentsLimit(int val) {
            offlinePaymentsTotalPaymentsLimit = val;
            return this;
        }

        public Builder offlinePaymentsLimitDefault(int val) {
            offlinePaymentsLimitDefault = val;
            return this;
        }

        public Builder offlinePaymentsPromptThresholdDefault(int val) {
            offlinePaymentsPromptThresholdDefault = val;
            return this;
        }

        public Builder offlinePaymentsTotalPaymentsLimitDefault(int val) {
            offlinePaymentsTotalPaymentsLimitDefault = val;
            return this;
        }

        public Builder offlinePaymentsMaxLimit(int val) {
            offlinePaymentsMaxLimit = val;
            return this;
        }

        public Builder offlinePaymentsMaxTotalPaymentsLimit(int val) {
            offlinePaymentsMaxTotalPaymentsLimit = val;
            return this;
        }

        public Builder showOfflinePayments(boolean val) {
            showOfflinePayments = val;
            return this;
        }

        public Builder maxOfflineDays(int val) {
            maxOfflineDays = val;
            return this;
        }

        public Builder allowStoreAndForward(boolean val) {
            allowStoreAndForward = val;
            return this;
        }

        public Builder bundleIndicator(String val) {
            bundleIndicator = val;
            return this;
        }

        public Device build() {
            return new Device(this);
        }
    }
}
