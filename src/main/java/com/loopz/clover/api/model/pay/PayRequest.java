package com.loopz.clover.api.model.pay;

/**

 {
 "orderId": "HCFFREA222N02",
 "taxAmount": 9,
 "zip": "94041",
 "expMonth": 1,
 "cvv": "111",
 "amount": 100,
 "currency": "usd",
 "last4": "1111",
 "expYear": 2015,
 "first6": "411111",
 "cardEncrypted": "X8UeKej+AEG1h0wD9Xw=="
 }

 */
public class PayRequest {

    private final String orderId;
    private final Long amount;
    private final Long taxAmount;
    private final String currency;
    private final Long expMonth;
    private final Long expYear;
    private final String cvv;
    private final String zip;
    private final String last4;
    private final String first6;
    private final String cardEncrypted;

    private PayRequest(Builder builder) {
        orderId = builder.orderId;
        amount = builder.amount;
        taxAmount = builder.taxAmount;
        currency = builder.currency;
        expMonth = builder.expMonth;
        expYear = builder.expYear;
        cvv = builder.cvv;
        zip = builder.zip;
        last4 = builder.last4;
        first6 = builder.first6;
        cardEncrypted = builder.cardEncrypted;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getOrderId() {
        return orderId;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public Long getExpMonth() {
        return expMonth;
    }

    public Long getExpYear() {
        return expYear;
    }

    public String getCvv() {
        return cvv;
    }

    public String getZip() {
        return zip;
    }

    public String getLast4() {
        return last4;
    }

    public String getFirst6() {
        return first6;
    }

    public String getCardEncrypted() {
        return cardEncrypted;
    }

    public static final class Builder {
        private String orderId;
        private Long amount;
        private Long taxAmount;
        private String currency;
        private Long expMonth;
        private Long expYear;
        private String cvv;
        private String zip;
        private String last4;
        private String first6;
        private String cardEncrypted;

        private Builder() {
        }

        public Builder orderId(String val) {
            orderId = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public Builder expMonth(Long val) {
            expMonth = val;
            return this;
        }

        public Builder expYear(Long val) {
            expYear = val;
            return this;
        }

        public Builder cvv(String val) {
            cvv = val;
            return this;
        }

        public Builder zip(String val) {
            zip = val;
            return this;
        }

        public Builder last4(String val) {
            last4 = val;
            return this;
        }

        public Builder first6(String val) {
            first6 = val;
            return this;
        }

        public Builder cardEncrypted(String val) {
            cardEncrypted = val;
            return this;
        }

        public PayRequest build() {
            return new PayRequest(this);
        }
    }
}
