package com.loopz.clover.api.model.order;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.customer.CustomerList;
import com.loopz.clover.api.model.device.DeviceReference;
import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.lineitem.LineItemList;
import com.loopz.clover.api.model.payment.PaymentList;
import com.loopz.clover.api.model.refund.RefundList;
import java.util.Currency;

import com.loopz.clover.api.model.taxrate.TaxRateList;
import org.joda.time.DateTime;

@JsonDeserialize(builder = Order.Builder.class)
public class Order {

    private final String id;
    private final OrderState state;
    private final Currency currency;
    private final Long total;
    private final boolean taxRemoved;
    private final boolean isVat;
    private final boolean manualTransaction;
    private final boolean groupLineItems;
    private final boolean testMode;
    private final PayType payType;
    private final DateTime createdTime;
    private final DateTime clientCreatedTime;
    private final DateTime modifiedTime;
    private final String note;
    private final EmployeeReference employee;
    private final LineItemList lineItems;
    private final CustomerList customers;
    private final PaymentList payments;
    private final RefundList refunds;
    private final DeviceReference device;
    private final TaxRateList taxRates;

    private Order(Builder builder) {
        id = builder.id;
        state = builder.state;
        currency = builder.currency;
        total = builder.total;
        taxRemoved = builder.taxRemoved;
        isVat = builder.isVat;
        manualTransaction = builder.manualTransaction;
        groupLineItems = builder.groupLineItems;
        testMode = builder.testMode;
        payType = builder.payType;
        createdTime = builder.createdTime;
        clientCreatedTime = builder.clientCreatedTime;
        modifiedTime = builder.modifiedTime;
        note = builder.note;
        employee = builder.employee;
        lineItems = builder.lineItems;
        customers = builder.customers;
        payments = builder.payments;
        refunds = builder.refunds;
        device = builder.device;
        taxRates = builder.taxRates;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public OrderState getState() {
        return state;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Long getTotal() {
        return total;
    }

    public boolean isTaxRemoved() {
        return taxRemoved;
    }

    public boolean isVat() {
        return isVat;
    }

    public boolean isManualTransaction() {
        return manualTransaction;
    }

    public boolean isGroupLineItems() {
        return groupLineItems;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public PayType getPayType() {
        return payType;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public DateTime getClientCreatedTime() {
        return clientCreatedTime;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public String getNote() {
        return note;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }

    public LineItemList getLineItems() {
        return lineItems;
    }

    public CustomerList getCustomers() {
        return customers;
    }

    public PaymentList getPayments() {
        return payments;
    }

    public RefundList getRefunds() {
        return refunds;
    }

    public DeviceReference getDevice() {
        return device;
    }

    public TaxRateList getTaxRates() {
        return taxRates;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private OrderState state;
        private Currency currency;
        private Long total;
        private boolean taxRemoved;
        private boolean isVat;
        private boolean manualTransaction;
        private boolean groupLineItems;
        private boolean testMode;
        private PayType payType;
        private DateTime createdTime;
        private DateTime clientCreatedTime;
        private DateTime modifiedTime;
        private String note;
        private EmployeeReference employee;
        private LineItemList lineItems;
        private CustomerList customers;
        private PaymentList payments;
        private RefundList refunds;
        private DeviceReference device;
        private TaxRateList taxRates;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder state(OrderState val) {
            state = val;
            return this;
        }

        public Builder currency(Currency val) {
            currency = val;
            return this;
        }

        public Builder total(Long val) {
            total = val;
            return this;
        }

        public Builder taxRemoved(boolean val) {
            taxRemoved = val;
            return this;
        }

        public Builder isVat(boolean val) {
            isVat = val;
            return this;
        }

        public Builder manualTransaction(boolean val) {
            manualTransaction = val;
            return this;
        }

        public Builder groupLineItems(boolean val) {
            groupLineItems = val;
            return this;
        }

        public Builder testMode(boolean val) {
            testMode = val;
            return this;
        }

        public Builder payType(PayType val) {
            payType = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder clientCreatedTime(DateTime val) {
            clientCreatedTime = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public Builder employee(EmployeeReference val) {
            employee = val;
            return this;
        }

        public Builder lineItems(LineItemList val) {
            lineItems = val;
            return this;
        }

        public Builder customers(CustomerList val) {
            customers = val;
            return this;
        }

        public Builder payments(PaymentList val) {
            payments = val;
            return this;
        }

        public Builder refunds(RefundList val) {
            refunds = val;
            return this;
        }

        public Builder device(DeviceReference val) {
            device = val;
            return this;
        }

        public Builder taxRates(TaxRateList val) {
            taxRates = val;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }
}
