package com.loopz.clover.api.model.ecommerce.charge;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = InstallmentInfo.Builder.class)
public class InstallmentInfo {

    private InstallmentInfo(Builder builder) {
        billPayIndicator = builder.billPayIndicator;
        invoiceNumber = builder.invoiceNumber;
        description = builder.description;
    }

    public enum BillPayIndicator {
        RECURRING,
        INSTALLMENT;
    }

    private final BillPayIndicator billPayIndicator;
    private final String invoiceNumber;
    private final String description;

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("bill_pay_indicator")
    public BillPayIndicator getBillPayIndicator() {
        return billPayIndicator;
    }

    @JsonProperty("invoice_number")
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getDescription() {
        return description;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private BillPayIndicator billPayIndicator;
        private String invoiceNumber;
        private String description;

        public Builder() {
        }

        public Builder billPayIndicator(BillPayIndicator val) {
            billPayIndicator = val;
            return this;
        }

        public Builder invoiceNumber(String val) {
            invoiceNumber = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        public InstallmentInfo build() {
            return new InstallmentInfo(this);
        }
    }
}
