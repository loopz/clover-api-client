package com.loopz.clover.api.model.refund;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.employee.EmployeeReference;
import org.joda.time.DateTime;

@JsonDeserialize(builder = Refund.Builder.class)
public class Refund {

    private final String id;
    private final DateTime modifiedTime;
    private final DateTime createdTime;
    private final Long amount;
    private final Long taxAmount;
    private final Long tipAmount;
    private final boolean voided;
    private final String voidedReason;
    private EmployeeReference employee;

    private Refund(Builder builder) {
        id = builder.id;
        modifiedTime = builder.modifiedTime;
        createdTime = builder.createdTime;
        amount = builder.amount;
        taxAmount = builder.taxAmount;
        tipAmount = builder.tipAmount;
        voided = builder.voided;
        voidedReason = builder.voidedReason;
        employee = builder.employee;
    }


    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getTaxAmount() {
        return taxAmount;
    }

    public boolean isVoided() {
        return voided;
    }

    public Long getTipAmount() {
        return tipAmount;
    }

    public String getVoidedReason() {
        return voidedReason;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private DateTime modifiedTime;
        private DateTime createdTime;
        private Long amount;
        private Long taxAmount;
        private Long tipAmount;
        private boolean voided;
        private String voidedReason;
        private EmployeeReference employee;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder taxAmount(Long val) {
            taxAmount = val;
            return this;
        }

        public Builder tipAmount(Long val) {
            tipAmount = val;
            return this;
        }

        public Builder voided(boolean val) {
            voided = val;
            return this;
        }

        public Builder voidedReason(String val) {
            voidedReason = val;
            return this;
        }

        public Builder employee(EmployeeReference val) {
            employee = val;
            return this;
        }

        public Refund build() {
            return new Refund(this);
        }
    }
}
