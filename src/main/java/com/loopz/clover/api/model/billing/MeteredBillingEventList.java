package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class MeteredBillingEventList {

    private final List<MeteredBillingEvent> elements;

    @JsonCreator
    public MeteredBillingEventList(List<MeteredBillingEvent> elements) {
        this.elements = elements;
    }

    public List<MeteredBillingEvent> getElements() {
        return elements;
    }
}
