package com.loopz.clover.api.model.pay;

public enum PayResultType {

    APPROVED, DECLINED;
}
