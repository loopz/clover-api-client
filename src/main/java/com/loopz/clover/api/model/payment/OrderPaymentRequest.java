package com.loopz.clover.api.model.payment;

import com.loopz.clover.api.model.device.DeviceReference;
import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.tender.TenderReference;

public class OrderPaymentRequest {

    private final Long amount;
    private final TenderReference tender;
    private final EmployeeReference employee;
    private final DeviceReference device;
    private final String externalPaymentId;

    private OrderPaymentRequest(Builder builder) {
        amount = builder.amount;
        tender = builder.tender;
        employee = builder.employee;
        device = builder.device;
        externalPaymentId = builder.externalPaymentId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getAmount() {
        return amount;
    }

    public TenderReference getTender() {
        return tender;
    }

    public EmployeeReference getEmployee() {
        return employee;
    }

    public DeviceReference getDevice() {
        return device;
    }

    public String getExternalPaymentId() {
        return externalPaymentId;
    }

    public static final class Builder {

        private Long amount;
        private TenderReference tender;
        private EmployeeReference employee;
        private DeviceReference device;
        private String externalPaymentId;

        public Builder() {
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder tender(TenderReference val) {
            tender = val;
            return this;
        }

        public Builder employee(EmployeeReference val) {
            employee = val;
            return this;
        }

        public Builder device(DeviceReference val) {
            device = val;
            return this;
        }

        public Builder externalPaymentId(String val) {
            externalPaymentId = val;
            return this;
        }

        public OrderPaymentRequest build() {
            return new OrderPaymentRequest(this);
        }
    }
}
