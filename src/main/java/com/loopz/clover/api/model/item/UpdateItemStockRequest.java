package com.loopz.clover.api.model.item;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = UpdateItemStockRequest.Builder.class)
public class UpdateItemStockRequest {

    private ItemReference item;
    private Long stockCount;
    private Double quantity;

    private UpdateItemStockRequest(Builder builder) {
        item = builder.item;
        stockCount = builder.stockCount;
        quantity = builder.quantity;
    }

    public static Builder builder() {
        return new Builder();
    }

    public ItemReference getItem() {
        return item;
    }

    public Long getStockCount() {
        return stockCount;
    }

    public Double getQuantity() {
        return quantity;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private ItemReference item;
        private Long stockCount;
        private Double quantity;

        private Builder() {
        }

        public Builder item(ItemReference val) {
            item = val;
            return this;
        }

        public Builder stockCount(Long val) {
            stockCount = val;
            return this;
        }

        public Builder quantity(Double val) {
            quantity = val;
            return this;
        }

        public UpdateItemStockRequest build() {
            return new UpdateItemStockRequest(this);
        }
    }
}
