package com.loopz.clover.api.model.lineitem;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.item.ItemReference;
import org.joda.time.DateTime;

@JsonDeserialize(builder = LineItem.Builder.class)
public class LineItem {

    private final String id;
    private final ItemReference item;
    private final String name;
    private final Long price;
    private final String itemCode;
    private final String binName;
    private final String userData;
    private final boolean printed;
    private final DateTime createdTime;
    private final DateTime orderClientCreatedTime;
    private final boolean exchanged;
    private final boolean refunded;
    private final boolean isRevenue;
    private final Long unitQty;

    private LineItem(Builder builder) {
        id = builder.id;
        item = builder.item;
        name = builder.name;
        price = builder.price;
        itemCode = builder.itemCode;
        binName = builder.binName;
        userData = builder.userData;
        printed = builder.printed;
        createdTime = builder.createdTime;
        orderClientCreatedTime = builder.orderClientCreatedTime;
        exchanged = builder.exchanged;
        refunded = builder.refunded;
        isRevenue = builder.isRevenue;
        unitQty = builder.unitQty;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public ItemReference getItem() {
        return item;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getItemCode() {
        return itemCode;
    }

    public String getBinName() {
        return binName;
    }

    public String getUserData() {
        return userData;
    }

    public boolean isPrinted() {
        return printed;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public DateTime getOrderClientCreatedTime() {
        return orderClientCreatedTime;
    }

    public boolean isExchanged() {
        return exchanged;
    }

    public boolean isRefunded() {
        return refunded;
    }

    public boolean isRevenue() {
        return isRevenue;
    }

    public Long getUnitQty() {
        return unitQty;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private ItemReference item;
        private String name;
        private Long price;
        private String itemCode;
        private String binName;
        private String userData;
        private boolean printed;
        private DateTime createdTime;
        private DateTime orderClientCreatedTime;
        private boolean exchanged;
        private boolean refunded;
        private boolean isRevenue;
        private Long unitQty;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder item(ItemReference val) {
            item = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder price(Long val) {
            price = val;
            return this;
        }

        public Builder itemCode(String val) {
            itemCode = val;
            return this;
        }

        public Builder binName(String val) {
            binName = val;
            return this;
        }

        public Builder userData(String val) {
            userData = val;
            return this;
        }

        public Builder printed(boolean val) {
            printed = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder orderClientCreatedTime(DateTime val) {
            orderClientCreatedTime = val;
            return this;
        }

        public Builder exchanged(boolean val) {
            exchanged = val;
            return this;
        }

        public Builder refunded(boolean val) {
            refunded = val;
            return this;
        }

        public Builder isRevenue(boolean val) {
            isRevenue = val;
            return this;
        }

        public Builder unitQty(Long val) {
            unitQty = val;
            return this;
        }

        public LineItem build() {
            return new LineItem(this);
        }
    }
}
