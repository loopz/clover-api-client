package com.loopz.clover.api.model.refund;

import com.loopz.clover.api.model.ExpandableField;

public enum RefundExpandableField implements ExpandableField {

    PAYMENT("payment"),
    APP_TRACKING("appTracking"),
    TRANSACTION_INFO("transactionInfo"),
    OVERRIDE_MERCHANT_TENDER("overrideMerchantTender"),
    SERVICE_CHARGE("serviceCharge"),
    LINE_ITEMS("lineItems"),
    EMPLOYEE("employee");

    private final String field;

    RefundExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
