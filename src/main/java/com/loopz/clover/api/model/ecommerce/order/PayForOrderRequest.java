package com.loopz.clover.api.model.ecommerce.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.clover.api.model.ecommerce.charge.ChargeCustomTender;
import com.loopz.clover.api.model.ecommerce.charge.ChargeInputType;
import com.loopz.clover.api.model.ecommerce.charge.StoredCredentials;

public class PayForOrderRequest {

    private final String customerId;
    private final ChargeInputType inputType;
    private final String externalReferenceId;
    private final String source;
    private final StoredCredentials storedCredentials;
    private final Long tipAmount;
    private final ChargeCustomTender tender;;
    private final String email;

    private PayForOrderRequest(Builder builder) {
        customerId = builder.customerId;
        inputType = builder.inputType;
        externalReferenceId = builder.externalReferenceId;
        source = builder.source;
        storedCredentials = builder.storedCredentials;
        tipAmount = builder.tipAmount;
        tender = builder.tender;
        email = builder.email;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("ecomind")
    public ChargeInputType getInputType() {
        return inputType;
    }

    public String getEmail() {
        return email;
    }

    @JsonProperty("customer")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("external_reference_id")
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public String getSource() {
        return source;
    }

    @JsonProperty("stored_credentials")
    public StoredCredentials getStoredCredentials() {
        return storedCredentials;
    }

    @JsonProperty("tip_amount")
    public Long getTipAmount() {
        return tipAmount;
    }

    public ChargeCustomTender getTender() {
        return tender;
    }

    public static final class Builder {
        private String customerId;
        private ChargeInputType inputType;
        private String externalReferenceId;
        private String source;
        private StoredCredentials storedCredentials;
        private Long tipAmount;
        private ChargeCustomTender tender;
        private String email;

        public Builder() {
        }

        public Builder customerId(String val) {
            customerId = val;
            return this;
        }

        public Builder inputType(ChargeInputType val) {
            inputType = val;
            return this;
        }

        public Builder externalReferenceId(String val) {
            externalReferenceId = val;
            return this;
        }

        public Builder source(String val) {
            source = val;
            return this;
        }

        public Builder storedCredentials(StoredCredentials val) {
            storedCredentials = val;
            return this;
        }

        public Builder tipAmount(Long val) {
            tipAmount = val;
            return this;
        }

        public Builder tender(ChargeCustomTender val) {
            tender = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public PayForOrderRequest build() {
            return new PayForOrderRequest(this);
        }
    }
}
