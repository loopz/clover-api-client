package com.loopz.clover.api.model.pay;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 {
 "pem" : String
 "prefix" : String
 "modulus" : String
 "exponent" : String
 "id" : String
 }
 */
@JsonDeserialize(builder = MerchantPayKey.Builder.class)
public class MerchantPayKey {

    private final String id;
    private final String pem;
    private final String prefix;
    private final String modulus;
    private final String exponent;

    private MerchantPayKey(Builder builder) {
        id = builder.id;
        pem = builder.pem;
        prefix = builder.prefix;
        modulus = builder.modulus;
        exponent = builder.exponent;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getPem() {
        return pem;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getModulus() {
        return modulus;
    }

    public String getExponent() {
        return exponent;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String pem;
        private String prefix;
        private String modulus;
        private String exponent;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder pem(String val) {
            pem = val;
            return this;
        }

        public Builder prefix(String val) {
            prefix = val;
            return this;
        }

        public Builder modulus(String val) {
            modulus = val;
            return this;
        }

        public Builder exponent(String val) {
            exponent = val;
            return this;
        }

        public MerchantPayKey build() {
            return new MerchantPayKey(this);
        }
    }
}
