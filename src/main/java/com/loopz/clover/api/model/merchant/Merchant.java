package com.loopz.clover.api.model.merchant;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.employee.Employee;
import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(builder = Merchant.Builder.class)
public class Merchant {

    private final String id;
    private final String name;
    private final Boolean isBillable;
    private final Employee owner;
    private final Address address;
    private final String website;
    private final List<Logo> logos;

    private Merchant(Builder builder) {
        id = builder.id;
        name = builder.name;
        isBillable = builder.isBillable;
        owner = builder.owner;
        address = builder.address;
        website = builder.website;
        logos = builder.logos;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Employee getOwner() {
        return owner;
    }

    public Address getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }

    public List<Logo> getLogos() {
        return logos;
    }

    public Boolean isBillable() {
        return isBillable;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private Boolean isBillable;
        private Employee owner;
        private Address address;
        private String website;
        private List<Logo> logos = new ArrayList<>();

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder isBillable(Boolean val) {
            isBillable = val;
            return this;
        }

        public Builder owner(Employee val) {
            owner = val;
            return this;
        }

        public Builder address(Address val) {
            address = val;
            return this;
        }

        public Builder website(String val) {
            website = val;
            return this;
        }

        public Builder logos(JsonNode logos) {
            JsonNode elements = logos.withArray("elements");
            for (JsonNode logoNode : elements) {
                Logo logo = new Logo(logoNode.get("logoType").textValue(), logoNode.get("url").textValue());
                this.logos.add(logo);
            }
            return this;
        }

        public Merchant build() {
            return new Merchant(this);
        }
    }
}
