package com.loopz.clover.api.model.merchant;

import com.loopz.clover.api.model.ExpandableField;

public enum MerchantExpandableField implements ExpandableField {

    OWNER("owner"),
    ADDRESS("address"),
    LOGOS("logos"),
    GATEWAY("gateway"),
    BANK_PROCESSING("bankProcessing"),
    EXTERNAL_MERCHANT("externalMerchant"),
    MERCHANT_PLAN("merchantPlan");

    private final String field;

    MerchantExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
