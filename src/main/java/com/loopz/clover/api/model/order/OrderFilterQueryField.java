package com.loopz.clover.api.model.order;

import com.loopz.clover.api.model.FilterQueryField;

public enum OrderFilterQueryField implements FilterQueryField {

    ORDER_TYPE("orderType"),
    TOUCHED("touched"),
    LAST_4("cardTransaction.last4"),
    EMPLOYEE_ID("employee.id"),
    EMPLOYEE_NAME("employee.name"),
    TITLE("title"),
    DEVICE_ID("device.id"),
    EXTERNAL_REFERENCE_ID("externalReferenceId"),
    TOTAL("total"),
    PAY_TYPE("payType"),
    CUSTOMER_ID("customer.id"),
    TEST_MODE("testMode"),
    ID("id"),
    STATE("state"),
    CREATED_TIME("createdTime"),
    MODIFIED_TIME("modifiedTime"),
    DELETED_TIME("deletedTime");

    private final String field;

    OrderFilterQueryField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
