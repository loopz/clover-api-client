package com.loopz.clover.api.model.merchant;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 "zip": "",
 "country": "",
 "phoneNumber": "",
 "address3": "",
 "address2": "",
 "city": "",
 "address1": "",
 "state": ""
 */

@JsonDeserialize(builder = Address.Builder.class)
public class Address {

    private final String address1;
    private final String address2;
    private final String address3;
    private final String city;
    private final String state;
    private final String zip;
    private final String country;
    private final String phoneNumber;

    private Address(Builder builder) {
        address1 = builder.address1;
        address2 = builder.address2;
        address3 = builder.address3;
        city = builder.city;
        state = builder.state;
        zip = builder.zip;
        country = builder.country;
        phoneNumber = builder.phoneNumber;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String address1;
        private String address2;
        private String address3;
        private String city;
        private String state;
        private String zip;
        private String country;
        private String phoneNumber;

        private Builder() {
        }

        public Builder address1(String val) {
            address1 = val;
            return this;
        }

        public Builder address2(String val) {
            address2 = val;
            return this;
        }

        public Builder address3(String val) {
            address3 = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder state(String val) {
            state = val;
            return this;
        }

        public Builder zip(String val) {
            zip = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder phoneNumber(String val) {
            phoneNumber = val;
            return this;
        }

        public Address build() {
            return new Address(this);
        }
    }
}
