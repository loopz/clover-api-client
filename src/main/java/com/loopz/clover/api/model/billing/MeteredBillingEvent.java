package com.loopz.clover.api.model.billing;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

/**
 {
 "modifiedTime":"long",
 "charge":{
    "id":""
 },
 "appMetered":{
    "app":{
        "id":""
    },
    "amount":"long",
    "action":"",
    "active":false,
    "meteredCountries":[
        {
            "country":"",
            "amount":"long",
            "appMetered":{
        "id":""
    },
 "action":"",
 "active":false,
 "id":""
 }
 ],
 "id":"",
 "label":""
 },
 "count":"long",
 "createdTime":"long",
 "id":""
 }
 */
@JsonDeserialize(builder = MeteredBillingEvent.Builder.class)
public class MeteredBillingEvent {

    private final String id;
    private final Long count;
    private final DateTime createdTime;
    private final DateTime modifiedTime;

    private MeteredBillingEvent(Builder builder) {
        id = builder.id;
        count = builder.count;
        createdTime = builder.createdTime;
        modifiedTime = builder.modifiedTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public Long getCount() {
        return count;
    }

    public DateTime getCreatedTime() {
        return createdTime;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private Long count;
        private DateTime createdTime;
        private DateTime modifiedTime;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder count(Long val) {
            count = val;
            return this;
        }

        public Builder createdTime(DateTime val) {
            createdTime = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public MeteredBillingEvent build() {
            return new MeteredBillingEvent(this);
        }
    }
}
