package com.loopz.clover.api.model.lineitem;

import com.loopz.clover.api.model.item.ItemReference;

public class AddLineItemRequest {

    private final String note;
    private final Long price;
    private final Long unitQty;
    private final ItemReference item;

    private AddLineItemRequest(Builder builder) {
        note = builder.note;
        price = builder.price;
        unitQty = builder.unitQty;
        item = builder.item;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getNote() {
        return note;
    }

    public Long getPrice() {
        return price;
    }

    public Long getUnitQty() {
        return unitQty;
    }

    public ItemReference getItem() {
        return item;
    }

    public static final class Builder {
        private String note;
        private Long price;
        private Long unitQty;
        private ItemReference item;

        private Builder() {
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public Builder price(Long val) {
            price = val;
            return this;
        }

        public Builder unitQty(Long val) {
            unitQty = val;
            return this;
        }

        public Builder item(ItemReference val) {
            item = val;
            return this;
        }

        public AddLineItemRequest build() {
            return new AddLineItemRequest(this);
        }
    }
}
