package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class CustomerList {

    private final List<Customer> elements;

    @JsonCreator
    public CustomerList(@JsonProperty("elements") List<Customer> elements) {
        this.elements = elements;
    }

    public List<Customer> getElements() {
        return elements;
    }
}
