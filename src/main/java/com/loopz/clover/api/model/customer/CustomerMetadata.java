package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.joda.time.DateTime;

@JsonDeserialize(builder = CustomerMetadata.Builder.class)
public class CustomerMetadata {

    private final String businessName;
    private final String note;
    private final String dobYear;
    private final String dobMonth;
    private final String dobDay;
    private final DateTime modifiedTime;

    private CustomerMetadata(Builder builder) {
        businessName = builder.businessName;
        note = builder.note;
        dobYear = builder.dobYear;
        dobMonth = builder.dobMonth;
        dobDay = builder.dobDay;
        modifiedTime = builder.modifiedTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getNote() {
        return note;
    }

    public String getDobYear() {
        return dobYear;
    }

    public String getDobMonth() {
        return dobMonth;
    }

    public String getDobDay() {
        return dobDay;
    }

    public DateTime getModifiedTime() {
        return modifiedTime;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String businessName;
        private String note;
        private String dobYear;
        private String dobMonth;
        private String dobDay;
        private DateTime modifiedTime;

        private Builder() {
        }

        public Builder businessName(String val) {
            businessName = val;
            return this;
        }

        public Builder note(String val) {
            note = val;
            return this;
        }

        public Builder dobYear(String val) {
            dobYear = val;
            return this;
        }

        public Builder dobMonth(String val) {
            dobMonth = val;
            return this;
        }

        public Builder dobDay(String val) {
            dobDay = val;
            return this;
        }

        public Builder modifiedTime(DateTime val) {
            modifiedTime = val;
            return this;
        }

        public CustomerMetadata build() {
            return new CustomerMetadata(this);
        }
    }
}
