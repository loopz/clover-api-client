package com.loopz.clover.api.model.device;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.clover.api.model.customer.Customer;

import java.util.List;

public class DeviceList {

    private final List<Device> elements;

    @JsonCreator
    public DeviceList(@JsonProperty("elements") List<Device> elements) {
        this.elements = elements;
    }

    public List<Device> getElements() {
        return elements;
    }
}
