package com.loopz.clover.api.model.ecommerce.refund;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.ecommerce.order.PayForOrderRequest;

@JsonDeserialize(builder = CreateRefundRequest.Builder.class)
public class CreateRefundRequest {

    private final String chargeId;
    private final Long amount;
    private final String externalReferenceId;
    private final RefundReason reason;

    private CreateRefundRequest(Builder builder) {
        chargeId = builder.chargeId;
        amount = builder.amount;
        externalReferenceId = builder.externalReferenceId;
        reason = builder.reason;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("charge")
    public String getChargeId() {
        return chargeId;
    }

    public Long getAmount() {
        return amount;
    }

    @JsonProperty("external_reference_id")
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public RefundReason getReason() {
        return reason;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String chargeId;
        private Long amount;
        private String externalReferenceId;
        private RefundReason reason;

        public Builder() {
        }

        public Builder chargeId(String val) {
            chargeId = val;
            return this;
        }

        public Builder amount(Long val) {
            amount = val;
            return this;
        }

        public Builder externalReferenceId(String val) {
            externalReferenceId = val;
            return this;
        }

        public Builder reason(RefundReason val) {
            reason = val;
            return this;
        }

        public CreateRefundRequest build() {
            return new CreateRefundRequest(this);
        }
    }
}
