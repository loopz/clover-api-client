package com.loopz.clover.api.model.customer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.clover.api.model.order.OrderReference;
import com.loopz.clover.api.model.order.OrderReferenceList;
import org.joda.time.DateTime;

import java.util.Collections;
import java.util.List;

/**
 "customerSince": "long",
 "firstName": "",
 "lastName": "",
 "addresses": "--Expandable Field--",
 "metadata": "--Expandable Field--",
 "emailAddresses": "--Expandable Field--",
 "cards": "--Expandable Field--",
 "marketingAllowed": false,
 "merchant": {
 "id": ""
 },
 "orders": [
 {
 "id": ""
 }
 ],
 "id": "",
 "phoneNumbers": "--Expandable Field--"
 */
@JsonDeserialize(builder = Customer.Builder.class)
public class Customer {

    private final String id;
    private final String firstName;
    private final String lastName;
    private final DateTime customerSince;
    private final EmailAddressList emailAddresses;
    private final PhoneNumberList phoneNumbers;
    private final boolean marketingAllowed;
    private final OrderReferenceList orders;
    private final CustomerMetadata metadata;

    private Customer(Builder builder) {
        id = builder.id;
        firstName = builder.firstName;
        lastName = builder.lastName;
        customerSince = builder.customerSince;
        emailAddresses = builder.emailAddresses;
        phoneNumbers = builder.phoneNumbers;
        marketingAllowed = builder.marketingAllowed;
        orders = builder.orders;
        metadata = builder.metadata;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public DateTime getCustomerSince() {
        return customerSince;
    }

    public EmailAddressList getEmailAddresses() {
        return emailAddresses;
    }

    public boolean isMarketingAllowed() {
        return marketingAllowed;
    }

    public PhoneNumberList getPhoneNumbers() {
        return phoneNumbers;
    }

    public OrderReferenceList getOrders() {
        return orders;
    }

    public CustomerMetadata getMetadata() {
        return metadata;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String firstName;
        private String lastName;
        private DateTime customerSince;
        private EmailAddressList emailAddresses = new EmailAddressList(Collections.<EmailAddress>emptyList());
        private PhoneNumberList phoneNumbers = new PhoneNumberList(Collections.<PhoneNumber>emptyList());
        private boolean marketingAllowed;
        private OrderReferenceList orders = new OrderReferenceList(Collections.<OrderReference>emptyList());
        private CustomerMetadata metadata;

        private Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder firstName(String val) {
            firstName = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder customerSince(DateTime val) {
            customerSince = val;
            return this;
        }

        public Builder emailAddresses(EmailAddressList val) {
            emailAddresses = val;
            return this;
        }

        public Builder phoneNumbers(PhoneNumberList val) {
            phoneNumbers = val;
            return this;
        }

        public Builder marketingAllowed(boolean val) {
            marketingAllowed = val;
            return this;
        }

        public Builder orders(OrderReferenceList val) {
            orders = val;
            return this;
        }

        public Builder metadata(CustomerMetadata val) {
            metadata = val;
            return this;
        }

        public Customer build() {
            return new Customer(this);
        }
    }
}
