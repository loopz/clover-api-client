package com.loopz.clover.api.model.payment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * "cardTransaction":{
         "paymentRef":{
             "id":"Y74QSZ849VH8P"
         },
         "cardType":"VISA",
         "entryType":"KEYED",
         "first6":"424242",
         "last4":"4242",
         "type":"PREAUTH",
         "authCode":"OK4790",
         "referenceId":"900500000133",
         "state":"PENDING",
         "extra":{
             "common":"{\"LocalDateTime\":\"20190105201339\",\"POSEntryMode\":\"012\",\"MerchID\":\"RCTST0000008099\",\"OrderNum\":\"Y74QSZ849VH8P\",\"TermEntryCapablt\":\"10\",\"CardCaptCap\":\"0\",\"STAN\":\"000133\",\"POSCondCode\":\"59\",\"TermLocInd\":\"1\",\"TermID\":\"00000001\",\"TrnmsnDateTime\":\"20190105201339\"}",
             "func":"CREDIT",
             "authorizingNetworkName":"VISA",
             "athNtwkId":"02",
             "exp":"202012",
             "card":"{\"TransID\":\"019005915187626G370\",\"ACI\":\"W\"}",
             "verification":"{\"avs\":\"Z\"}"
         },
         "cardholderName":""
 }
 */

@JsonDeserialize(builder = CardTransactionInfo.Builder.class)
public class CardTransactionInfo {

    private final String cardType;
    private final String entryType;
    private final String first6;
    private final String last4;
    private final String type;
    private final String authCode;
    private final String referenceId;
    private final String state;
    private final String cardholderName;
    private final JsonNode extra;

    private CardTransactionInfo(Builder builder) {
        cardType = builder.cardType;
        entryType = builder.entryType;
        first6 = builder.first6;
        last4 = builder.last4;
        type = builder.type;
        authCode = builder.authCode;
        referenceId = builder.referenceId;
        state = builder.state;
        cardholderName = builder.cardholderName;
        extra = builder.extra;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getCardType() {
        return cardType;
    }

    public String getEntryType() {
        return entryType;
    }

    public String getFirst6() {
        return first6;
    }

    public String getLast4() {
        return last4;
    }

    public String getType() {
        return type;
    }

    public String getAuthCode() {
        return authCode;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public String getState() {
        return state;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public JsonNode getExtra() {
        return extra;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {

        private String cardType;
        private String entryType;
        private String first6;
        private String last4;
        private String type;
        private String authCode;
        private String referenceId;
        private String state;
        private String cardholderName;
        private JsonNode extra;

        private Builder() {
        }

        public Builder cardType(String val) {
            cardType = val;
            return this;
        }

        public Builder entryType(String val) {
            entryType = val;
            return this;
        }

        public Builder first6(String val) {
            first6 = val;
            return this;
        }

        public Builder last4(String val) {
            last4 = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder authCode(String val) {
            authCode = val;
            return this;
        }

        public Builder referenceId(String val) {
            referenceId = val;
            return this;
        }

        public Builder state(String val) {
            state = val;
            return this;
        }

        public Builder cardholderName(String val) {
            cardholderName = val;
            return this;
        }

        public Builder extra(JsonNode val) {
            extra = val;
            return this;
        }

        public CardTransactionInfo build() {
            return new CardTransactionInfo(this);
        }
    }
}
