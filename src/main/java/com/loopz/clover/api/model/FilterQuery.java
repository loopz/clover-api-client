package com.loopz.clover.api.model;

public abstract class FilterQuery<FIELD extends FilterQueryField> {

    private final FIELD field;
    private final QueryOperators operator;
    private final Object value;

    public FilterQuery(FIELD field, QueryOperators operator, Object value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public FIELD getField() {
        return field;
    }

    public QueryOperators getOperator() {
        return operator;
    }

    public Object getValue() {
        return value;
    }
}