package com.loopz.clover.api.model.order;

import com.loopz.clover.api.model.ExpandableField;

public enum OrderExpandableField implements ExpandableField {

    LINE_ITEMS("lineItems"),
    CUSTOMERS("customers"),
    PAYMENTS("payments"),
    CREDITS("credits"),
    REFUNDS("refunds"),
    SERVICE_CHARGE("serviceCharge"),
    DISCOUNTS("discounts");

    private final String field;

    OrderExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
