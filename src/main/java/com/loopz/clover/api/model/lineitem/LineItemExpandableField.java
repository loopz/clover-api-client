package com.loopz.clover.api.model.lineitem;

import com.loopz.clover.api.model.ExpandableField;

public enum LineItemExpandableField implements ExpandableField {

    EMPLOYEE("employee"),
    ORDER_TYPE("orderType"),
    DISCOUNTS("discounts"),
    MODIFICATIONS("modifications"),
    TAX_RATES("taxRates"),
    PAYMENTS("payments");

    private final String field;

    LineItemExpandableField(String field) {
        this.field = field;
    }

    @Override
    public String getField() {
        return field;
    }
}
