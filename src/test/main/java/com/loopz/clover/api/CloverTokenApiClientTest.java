package com.loopz.clover.api;

import com.loopz.clover.api.client.ApiKeyAuthenticationStrategy;
import com.loopz.clover.api.client.ApiTokenAuthenticationStrategy;
import com.loopz.clover.api.client.CloverApiClient;
import com.loopz.clover.api.client.CloverTokenApiClient;
import com.loopz.clover.api.model.apikey.ApiKey;
import com.loopz.clover.api.model.pay.MerchantPayKey;
import com.loopz.clover.api.model.pay.PaymentCardCryptoUtils;
import com.loopz.clover.api.model.token.Card;
import com.loopz.clover.api.model.token.CreateTokenRequest;
import com.loopz.clover.api.model.token.TokenizedCard;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.security.PublicKey;
import java.security.Security;

import static com.loopz.clover.api.CloverTestConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CloverTokenApiClientTest {

    private CloverApiClient cloverApiClient;
    private CloverTokenApiClient cloverTokenApiClient;

    @BeforeClass
    public static void setupClass() {
        CloverApi.setApiToken(SANDBOX_API_TOKEN);
        CloverApi.setAppId(SANDBOX_APP_ID);
        CloverApi.setMeteredId(SANDBOX_METERED_ID);
    }

    @Before
    public void setup() throws CloverApiException {
        cloverApiClient = new CloverApiClient(CloverApiUrls.SANDBOX, new ApiTokenAuthenticationStrategy(SANDBOX_ACCESS_TOKEN));

        ApiKey apiKey = cloverApiClient.getApiKey(SANDBOX_MERCHANT_ID);

        cloverTokenApiClient = new CloverTokenApiClient(CloverTokenApiUrls.SANDBOX, new ApiKeyAuthenticationStrategy(apiKey.getApiKey()));
    }

    @Test
    public void testCreateToken() throws CloverApiException {
        String cardNumber = "6011361000006668";
        String first6 = cardNumber.substring(0, 6);
        String last4 = cardNumber.substring(cardNumber.length() -4, cardNumber.length());

        Card card = Card.builder()
                .number(cardNumber)
                .cvv("123")
                .expMonth("12")
                .expYear("2022")
                .last4(last4)
                .first6(first6)
                .brand("DISCOVER")
                .build();

        TokenizedCard tokenizedCard = cloverTokenApiClient.createToken(new CreateTokenRequest(card));

        assertThat(tokenizedCard).isNotNull();
        assertThat(tokenizedCard.getId()).isNotNull();
        assertThat(tokenizedCard.getCard()).isNotNull();
        assertThat(tokenizedCard.getCard().getFirst6()).isEqualTo(first6);
        assertThat(tokenizedCard.getCard().getLast4()).isEqualTo(last4);


    }
}
