package com.loopz.clover.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.loopz.clover.api.client.ApiTokenAuthenticationStrategy;
import com.loopz.clover.api.client.CloverApiClient;
import com.loopz.clover.api.model.QueryOperators;
import com.loopz.clover.api.model.billing.BillingInfo;
import com.loopz.clover.api.model.billing.MeteredBillingEvent;
import com.loopz.clover.api.model.billing.MeteredBillingEventList;
import com.loopz.clover.api.model.customer.*;
import com.loopz.clover.api.model.device.DeviceList;
import com.loopz.clover.api.model.device.DeviceReference;
import com.loopz.clover.api.model.employee.Employee;
import com.loopz.clover.api.model.employee.EmployeeList;
import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.item.*;
import com.loopz.clover.api.model.lineitem.AddLineItemRequest;
import com.loopz.clover.api.model.lineitem.LineItem;
import com.loopz.clover.api.model.merchant.Gateway;
import com.loopz.clover.api.model.merchant.Merchant;
import com.loopz.clover.api.model.merchant.MerchantExpandableField;
import com.loopz.clover.api.model.merchant.MerchantProperties;
import com.loopz.clover.api.model.order.*;
import com.loopz.clover.api.model.ordertype.OrderTypeExpandableField;
import com.loopz.clover.api.model.ordertype.OrderTypeList;
import com.loopz.clover.api.model.pay.MerchantPayKey;
import com.loopz.clover.api.model.pay.PayRequest;
import com.loopz.clover.api.model.pay.PayResponse;
import com.loopz.clover.api.model.pay.PaymentCardCryptoUtils;
import com.loopz.clover.api.model.payment.*;
import com.loopz.clover.api.model.refund.Refund;
import com.loopz.clover.api.model.refund.RefundExpandableField;
import com.loopz.clover.api.model.refund.RefundList;
import com.loopz.clover.api.model.tender.*;
import com.loopz.clover.api.util.JsonUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigInteger;
import java.security.PublicKey;
import java.security.Security;
import java.util.*;
import java.util.concurrent.*;

import static com.loopz.clover.api.CloverTestConstants.*;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.fail;

public class CloverApiClientTest {

    // curl -s https://apisandbox.dev.clover.com/v3/merchants/0EA8A3YVPGYJP/items?access_token=31475edb-c76f-422e-3fd2-9e3edcc1e989

    private CloverApiClient cloverApiClient;

    @BeforeClass
    public static void setupClass() {
        CloverApi.setApiToken(SANDBOX_API_TOKEN);
        CloverApi.setAppId(SANDBOX_APP_ID);
        CloverApi.setMeteredId(SANDBOX_METERED_ID);
    }

    @Before
    public void setup() {
        cloverApiClient = new CloverApiClient(CloverApiUrls.SANDBOX, new ApiTokenAuthenticationStrategy(SANDBOX_ACCESS_TOKEN));
    }

    @Test
    public void testGetBillingInfo() throws CloverApiException {
        BillingInfo billingInfo = cloverApiClient.getBillingInfo(SANDBOX_MERCHANT_ID);

        assertThat(billingInfo).isNotNull();
    }

    @Test
    public void testGetMerchant() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");
//        String merchantId = "B1XB6MQVKZRSC";
//        String accessToken = "993bd90c-5a90-16af-e881-40239ed99811";
        String merchantId = "6PF8CYA08HG1A";
        String accessToken = "c63fb5e1-6288-7c1a-141a-d58afd634052";
        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(accessToken));

        Merchant merchant = cloverApiClient.getMerchant(merchantId, MerchantExpandableField.OWNER, MerchantExpandableField.ADDRESS, MerchantExpandableField.LOGOS, MerchantExpandableField.GATEWAY);

        System.out.println(JsonUtils.toJSON(merchant));
    }

    @Test
    public void testGetMerchantGateway() throws CloverApiException {
        Gateway gateway = cloverApiClient.getMerchantGateway(SANDBOX_MERCHANT_ID);

        assertThat(gateway).isNotNull();
    }

    @Test
    public void testGetMerchantProperties() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");
        String cloverMerchantId = "GNW1K9Q1J1A71";
        String cloverAccessToken = "ca0b2a98-8b3a-24d7-9ad8-cbfe77a2b47b";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        MerchantProperties properties = cloverApiClient.getMerchantProperties(cloverMerchantId);

        assertThat(properties).isNotNull();
        System.out.println(JsonUtils.toJSON(properties));
    }


    @Test
    public void testCreateMeteredBillingEvent() throws CloverApiException {
        MeteredBillingEvent meteredBillingEvent = cloverApiClient.createMeteredBillingEvent(SANDBOX_MERCHANT_ID, 5);
        assertThat(meteredBillingEvent).isNotNull();
    }

    @Test
    public void testDeleteMeteredBillingEvent() throws CloverApiException {
        MeteredBillingEvent meteredBillingEvent = cloverApiClient.createMeteredBillingEvent(SANDBOX_MERCHANT_ID, 10);
        assertThat(meteredBillingEvent).isNotNull();

        meteredBillingEvent = cloverApiClient.getMeteredBillingEvent(SANDBOX_MERCHANT_ID, meteredBillingEvent.getId());
        assertThat(meteredBillingEvent).isNotNull();

        cloverApiClient.deleteMeteredBillingEvent(SANDBOX_MERCHANT_ID, meteredBillingEvent.getId());

        try {
            meteredBillingEvent = cloverApiClient.getMeteredBillingEvent(SANDBOX_MERCHANT_ID, meteredBillingEvent.getId());
            fail("Expected an exception to be thrown.");
        } catch (CloverApiException e) {

        }
    }

    @Test
    @Ignore
    public void testGetMeteredBillingEventProd() throws CloverApiException {
        // Retrieve MangiPasta billing event
        CloverApi.setAppId("9G7WJV36Z1TET");
        CloverApiClient prodCloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy("507c28ea-5ac1-ba40-5929-b646ba241acd"));
        MeteredBillingEvent meteredBillingEvents = prodCloverApiClient.getMeteredBillingEvent("G9F80MXTTWGVJ", "HCYM5R090ZN24", "C1NTXKK9FGQQ0");

        assertThat(meteredBillingEvents).isNotNull();
    }


    //[{"id":"C1NTXKK9FGQQ0","appMetered":{},"count":4,"createdTime":1564246815000,"modifiedTime":1564246814000}]
    @Test
    @Ignore
    public void testGetMeteredBillingProd() throws CloverApiException {
        // Retrieve MangiPasta payment
        CloverApi.setAppId("9G7WJV36Z1TET");
        CloverApiClient prodCloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy("507c28ea-5ac1-ba40-5929-b646ba241acd"));
        MeteredBillingEventList meteredBillingEvents = prodCloverApiClient.getMeteredBillingEvents("G9F80MXTTWGVJ", "HCYM5R090ZN24");

        assertThat(meteredBillingEvents).isNotNull();
    }

    @Test
    public void testGetMerchantPayKey() throws CloverApiException {
        MerchantPayKey merchantPayKey = cloverApiClient.getMerchantPayKey(SANDBOX_MERCHANT_ID);
        assertThat(merchantPayKey).isNotNull();

        MerchantPayKey merchantPayKey2 = cloverApiClient.getMerchantPayKey(SANDBOX_MERCHANT_ID);

        assertThat(merchantPayKey.getExponent()).isEqualTo(merchantPayKey2.getExponent());
        assertThat(merchantPayKey.getModulus()).isEqualTo(merchantPayKey2.getModulus());
        assertThat(merchantPayKey.getPrefix()).isEqualTo(merchantPayKey2.getPrefix());
        assertThat(merchantPayKey.getId()).isEqualTo(merchantPayKey2.getId());
        assertThat(merchantPayKey.getPem()).isEqualTo(merchantPayKey2.getPem());

    }

    @Test
    public void testCreateOrder_NonExistingMerchant_ExpectError() throws CloverApiException {
        try {
            Order order = cloverApiClient.createOrder("invalidmerchantid");
            fail();
        } catch (CloverApiException e) {
            assertThat(e.getStatusCode()).isEqualTo(401);
            assertThat(e.getMessage()).isNotNull();
        }
    }

    @Test
    public void testDeleteOrder() throws CloverApiException {
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        cloverApiClient.deleteOrder(SANDBOX_MERCHANT_ID, order.getId());

        try {
            cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, order.getId());
            fail();
        } catch (CloverApiException e) {
            assertThat(e.getStatusCode()).isEqualTo(404);
        }
    }

    @Test
    public void testPay() throws CloverApiException {
        Security.addProvider(new BouncyCastleProvider());


        MerchantPayKey merchantPayKey = cloverApiClient.getMerchantPayKey(SANDBOX_MERCHANT_ID);
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        String cardNumber = "4242424242424242";

        PublicKey publicKey = PaymentCardCryptoUtils.getPublicKey(new BigInteger(merchantPayKey.getModulus()), new BigInteger(merchantPayKey.getExponent()));
        String cardEncrypted = PaymentCardCryptoUtils.encryptCardNumber(merchantPayKey.getPrefix(), cardNumber, publicKey);

        PayRequest payRequest = PayRequest.builder()
                .amount(100L)
                .currency("USD")
                .cvv("123")
                .expMonth(12L)
                .expYear(2020L)
                .zip("10463")
                .last4("4242")
                .first6("424242")
                .orderId(order.getId())
                .cardEncrypted(cardEncrypted)
                .build();

        PayResponse payResponse = cloverApiClient.pay(SANDBOX_MERCHANT_ID, payRequest);

        assertThat(payResponse).isNotNull();

        Payment payment = cloverApiClient.getPayment(SANDBOX_MERCHANT_ID, payResponse.getPaymentId(), PaymentExpandableField.values());

        assertThat(payment.getId()).isEqualTo(payResponse.getPaymentId());
        assertThat(payment.getResultType()).isEqualTo(PaymentResultType.SUCCESS);
        assertThat(payment.getOrder()).isNotNull();
        assertThat(payment.getOrder().getId()).isEqualTo(order.getId());
        assertThat(payment.getCardTransactionInfo()).isNotNull();
        assertThat(payment.getCardTransactionInfo().getLast4()).isEqualTo("4242");

        // Try to retrieve the payment using the last 4 query param
        Long createdFrom = payment.getCreatedTime().minusSeconds(5).getMillis();
        Long createdTo = payment.getCreatedTime().plusSeconds(5).getMillis();

        List<PaymentFilterQuery> filterQueries = Arrays.asList(
                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.GREATER_THAN_OR_EQUAL, createdFrom),
                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.LESS_THAN_OR_EQUAL, createdTo),
                new PaymentFilterQuery(PaymentFilterQueryField.LAST_4, QueryOperators.EQUALS, "4242"),
                new PaymentFilterQuery(PaymentFilterQueryField.AMOUNT, QueryOperators.EQUALS, 100)
        );

        PaymentList payments = cloverApiClient.getPayments(SANDBOX_MERCHANT_ID, filterQueries, PaymentExpandableField.values());

        assertThat(payments.getElements().size() == 1);
    }

    @Test
    public void testGetPayments() throws CloverApiException {
        PaymentList payments = cloverApiClient.getPayments(SANDBOX_MERCHANT_ID, PaymentExpandableField.values());

        assertThat(payments.getElements()).isNotEmpty();
    }

    @Test
    public void testGetPayment() throws CloverApiException {
        Payment payment = cloverApiClient.getPayment(SANDBOX_MERCHANT_ID, "T8S9TM28NBNAC", PaymentExpandableField.REFUNDS);

        assertThat(payment).isNotNull();
    }

    @Test
    @Ignore
    public void testGetPaymentFromProd() throws CloverApiException {
        // Retrieve MangiPasta payment
        CloverApiClient prodCloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy("507c28ea-5ac1-ba40-5929-b646ba241acd"));
        Payment payment = prodCloverApiClient.getPayment("G9F80MXTTWGVJ", "DN1WEPZ68A9MA", PaymentExpandableField.REFUNDS, PaymentExpandableField.CARD_TRANSACTION, PaymentExpandableField.DCC_INFO, PaymentExpandableField.EXTERNAL_REFERENCE_ID, PaymentExpandableField.LINE_ITEM_PAYMENTS, PaymentExpandableField.ORDER, PaymentExpandableField.TAX_RATES, PaymentExpandableField.TENDER, PaymentExpandableField.TRANSACTION_INFO);

        assertThat(payment).isNotNull();
    }

    @Test
    public void testGetRefunds() throws CloverApiException {
        RefundList refunds = cloverApiClient.getRefunds(SANDBOX_MERCHANT_ID, RefundExpandableField.values());

        assertThat(refunds.getElements()).isNotEmpty();
    }

    @Test
    public void testGetRefund() throws CloverApiException {
        Refund refund = cloverApiClient.getRefund(SANDBOX_MERCHANT_ID, "VESZRF7B6PQ1J", RefundExpandableField.values());

        assertThat(refund).isNotNull();
    }

    @Test
    public void testCreateOrderAndAddPayment() throws CloverApiException, InterruptedException {
        TenderList tenders = cloverApiClient.getTenders(SANDBOX_MERCHANT_ID);

        Tender cashTender = null;
        for (Tender tender : tenders.getElements()) {
            if (tender.getLabelKey().equals("com.clover.tender.cash")) {
                cashTender = tender;
                break;
            }
        }

        EmployeeList employees = cloverApiClient.getEmployees(SANDBOX_MERCHANT_ID);
        Employee employee = employees.getElements().get(0);

        assertThat(cashTender).isNotNull();

        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .employee(new EmployeeReference(employee.getId()))
                .state(OrderState.OPEN)
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);
        assertThat(order.getState()).isEqualTo(OrderState.OPEN);
        assertThat(order.getEmployee().getId()).isEqualTo(employee.getId());

        OrderPaymentRequest orderPaymentRequest = OrderPaymentRequest.builder()
                .amount(100L)
                .employee(new EmployeeReference(employee.getId()))
                .tender(new TenderReference(cashTender.getId()))
                .device(new DeviceReference("C050UQ82030150"))
                .build();

        Payment orderPayment = cloverApiClient.createOrderPayment(SANDBOX_MERCHANT_ID, order.getId(), orderPaymentRequest);

        assertThat(orderPayment).isNotNull();
        assertThat(orderPayment.getAmount()).isEqualTo(100L);
        assertThat(orderPayment.getTender().getId()).isEqualTo(cashTender.getId());
        assertThat(orderPayment.getEmployee().getId()).isEqualTo(employee.getId());
        assertThat(orderPayment.getEmployee().getId()).isEqualTo(employee.getId());

        // Verify that the order is not in the PAID state
        Thread.sleep(2000);
        order = cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, order.getId());
        assertThat(order.getState()).isEqualTo(OrderState.PAID);
    }

    @Test
    public void testCreateOrderAndAddTwoPayments() throws CloverApiException, InterruptedException {
        TenderList tenders = cloverApiClient.getTenders(SANDBOX_MERCHANT_ID);

        Tender cashTender = null;
        for (Tender tender : tenders.getElements()) {
            if (tender.getLabelKey().equals("com.clover.tender.cash")) {
                cashTender = tender;
                break;
            }
        }

        EmployeeList employees = cloverApiClient.getEmployees(SANDBOX_MERCHANT_ID);
        Employee employee = employees.getElements().get(0);

        assertThat(cashTender).isNotNull();

        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .employee(new EmployeeReference(employee.getId()))
                .state(OrderState.OPEN)
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);
        assertThat(order.getState()).isEqualTo(OrderState.OPEN);
        assertThat(order.getEmployee().getId()).isEqualTo(employee.getId());

        OrderPaymentRequest orderPaymentRequest = OrderPaymentRequest.builder()
                .amount(50L)
                .employee(new EmployeeReference(employee.getId()))
                .tender(new TenderReference(cashTender.getId()))
                .device(new DeviceReference("C050UQ82030150"))
                .build();

        Payment orderPaymentOne = cloverApiClient.createOrderPayment(SANDBOX_MERCHANT_ID, order.getId(), orderPaymentRequest);

        assertThat(orderPaymentOne).isNotNull();
        assertThat(orderPaymentOne.getAmount()).isEqualTo(50L);
        assertThat(orderPaymentOne.getTender().getId()).isEqualTo(cashTender.getId());
        assertThat(orderPaymentOne.getEmployee().getId()).isEqualTo(employee.getId());
        assertThat(orderPaymentOne.getEmployee().getId()).isEqualTo(employee.getId());

        Payment orderPaymentTwo = cloverApiClient.createOrderPayment(SANDBOX_MERCHANT_ID, order.getId(), orderPaymentRequest);

        assertThat(orderPaymentTwo).isNotNull();
        assertThat(orderPaymentTwo.getAmount()).isEqualTo(50L);
        assertThat(orderPaymentTwo.getTender().getId()).isEqualTo(cashTender.getId());
        assertThat(orderPaymentTwo.getEmployee().getId()).isEqualTo(employee.getId());
        assertThat(orderPaymentTwo.getEmployee().getId()).isEqualTo(employee.getId());

        // Verify that the order is not in the PAID state
        Thread.sleep(4000);
        order = cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, order.getId());
        assertThat(order.getState()).isEqualTo(OrderState.PAID);
    }

    @Test
    public void testCreateOrderAndAddStoreCreditPayment() throws CloverApiException, InterruptedException {
        TenderList tenders = cloverApiClient.getTenders(SANDBOX_MERCHANT_ID);

        Tender storeCreditTender = null;
        for (Tender tender : tenders.getElements()) {
            if (tender.getLabel().equals("Loopz Store Credit")) {
                storeCreditTender = tender;
                break;
            }
        }

        EmployeeList employees = cloverApiClient.getEmployees(SANDBOX_MERCHANT_ID);
        Employee employee = employees.getElements().get(0);

        assertThat(storeCreditTender).isNotNull();

        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .employee(new EmployeeReference(employee.getId()))
                .state(OrderState.OPEN)
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);
        assertThat(order.getState()).isEqualTo(OrderState.OPEN);
        assertThat(order.getEmployee().getId()).isEqualTo(employee.getId());

        OrderPaymentRequest orderPaymentRequest = OrderPaymentRequest.builder()
                .amount(100L)
                .employee(new EmployeeReference(employee.getId()))
                .tender(new TenderReference(storeCreditTender.getId()))
//                .device(new DeviceReference("C050UQ82030150"))
                .build();

        Payment orderPayment = cloverApiClient.createOrderPayment(SANDBOX_MERCHANT_ID, order.getId(), orderPaymentRequest);

        assertThat(orderPayment).isNotNull();
        assertThat(orderPayment.getAmount()).isEqualTo(100L);
        assertThat(orderPayment.getTender().getId()).isEqualTo(storeCreditTender.getId());
        assertThat(orderPayment.getEmployee().getId()).isEqualTo(employee.getId());
        assertThat(orderPayment.getEmployee().getId()).isEqualTo(employee.getId());

        // Verify that the order is not in the PAID state
        Thread.sleep(5000);
        order = cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, order.getId());
        assertThat(order.getState()).isEqualTo(OrderState.LOCKED);
    }

    @Test
    public void testGetOrders() throws CloverApiException {
        OrderList orders = cloverApiClient.getOrders(SANDBOX_MERCHANT_ID, OrderExpandableField.values());

        assertThat(orders.getElements().size()).isGreaterThan(0);
        Order order = orders.getElements().get(0);

        assertThat(order.getState()).isNotNull();
        assertThat(order.getId()).isNotNull();
        assertThat(order.getCreatedTime()).isNotNull();
        assertThat(order.getCurrency()).isNotNull();
        assertThat(order.getPayType()).isNotNull();
        assertThat(order.getTotal()).isNotNull();
    }

    @Test
    public void testGetOrders_Prod() throws CloverApiException {
        String cloverMerchantId = "Z9B57KSQGDPB1";
        String cloverAccessToken = "fbadddb0-9ef2-2e92-742d-da9271d50c5b";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        OrderList orders = cloverApiClient.getOrders(cloverMerchantId, OrderExpandableField.values());

        System.out.println("Found " + orders.getElements().size() + " Orders, checking for orders with customers.");

        for(Order order : orders.getElements()) {
            if(order.getCustomers() != null && !order.getCustomers().getElements().isEmpty()) {
                System.out.println("--------------------------");
                System.out.println("Found order with customer.");
                System.out.println(JsonUtils.toJSON(order));
            }
        }

    }

    @Test
    @Ignore
    public void testGetCustomers() throws CloverApiException {
        CustomerList customers = cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Collections.<CustomerFilterQuery>emptyList(), CustomerExpandableField.values());
        assertThat(customers.getElements().size()).isGreaterThan(0);

        Customer customer = customers.getElements().get(0);

        CustomerFilterQuery filterQuery = new CustomerFilterQuery(CustomerFilterQueryField.ID, QueryOperators.EQUALS, customer.getId());
        customers = cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Arrays.asList(filterQuery), CustomerExpandableField.EMAIL_ADDRESSES);
        assertThat(customers.getElements().size()).isEqualTo(1);


        String customerEmailAddress = customer.getEmailAddresses().getElements().get(0).getEmailAddress();
        filterQuery = new CustomerFilterQuery(CustomerFilterQueryField.EMAIL_ADDRESS, QueryOperators.EQUALS, customerEmailAddress);
        customers = cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Arrays.asList(filterQuery), CustomerExpandableField.EMAIL_ADDRESSES);
        assertThat(customers.getElements().size()).isGreaterThan(0);
        assertThat(customers.getElements().get(0).getEmailAddresses().getElements().get(0).getEmailAddress()).isEqualTo(customerEmailAddress);
    }

    @Test
    public void testGetCustomers_RateLimited() throws CloverApiException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        final CountDownLatch latch = new CountDownLatch(100);
        for(int i = 0; i < 100; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Collections.<CustomerFilterQuery>emptyList(), CustomerExpandableField.values());
                    } catch (CloverApiException e) {
                        e.printStackTrace();
                    } finally {
                        latch.countDown();
                    }
                }
            });
        }

        latch.await();
    }

    @Test
    @Ignore
    public void testGetCustomers_Prod() throws CloverApiException {
        String cloverMerchantId = "Z9B57KSQGDPB1";
        String cloverAccessToken = "fbadddb0-9ef2-2e92-742d-da9271d50c5b";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        CustomerList customers = cloverApiClient.getCustomers(cloverMerchantId, 100, 0, Collections.<CustomerFilterQuery>emptyList(), CustomerExpandableField.values());
        assertThat(customers.getElements().size()).isGreaterThan(0);
    }

    @Test
    public void testUpdateCustomer() throws CloverApiException {
        CustomerList customers = cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Collections.<CustomerFilterQuery>emptyList(), CustomerExpandableField.values());
        assertThat(customers.getElements().size()).isGreaterThan(0);

        Customer customer = customers.getElements().get(0);

        EmailAddress emailAddress = EmailAddress.builder()
                .emailAddress("michael+" + UUID.randomUUID() + "@loopz.io")
                .primaryEmail(true)
                .build();

        if(!customer.getEmailAddresses().getElements().isEmpty()) {
            EmailAddress existingEmailAddress = customer.getEmailAddresses().getElements().get(0);

            emailAddress = EmailAddress.builder()
                    .emailAddress(emailAddress.getEmailAddress())
                    .primaryEmail(emailAddress.isPrimaryEmail())
                    .id(existingEmailAddress.getId())
                    .build();

        }

        UpdateCustomerRequest updateCustomerRequest = UpdateCustomerRequest.builder()
                .firstName("Michael " + UUID.randomUUID())
                .lastName("Elias " + UUID.randomUUID())
                .marketingAllowed(true)
                .emailAddresses(Arrays.asList(emailAddress))
                .build();

        Customer updatedCustomer = cloverApiClient.updateCustomer(SANDBOX_MERCHANT_ID, customer.getId(), updateCustomerRequest, CustomerExpandableField.values());

        assertThat(updatedCustomer.getFirstName()).isEqualTo(updateCustomerRequest.getFirstName());
    }

    @Test
    public void testCreateUpdateDeleteItem() throws CloverApiException {
        CreateItemRequest createItemRequest = CreateItemRequest.builder()
                .name("Test Item")
                .price(2500L)
                .priceType(PriceType.FIXED)
                .hidden(false)
                .isRevenue(false)
                .code("code")
                .build();

        Item item = cloverApiClient.createItem(SANDBOX_MERCHANT_ID, createItemRequest);

        assertThat(item).isNotNull();
        assertThat(item.getName()).isEqualTo(createItemRequest.getName());
        assertThat(item.getPrice()).isEqualTo(createItemRequest.getPrice());
        assertThat(item.getPriceType()).isEqualTo(createItemRequest.getPriceType());
        assertThat(item.isHidden()).isEqualTo(createItemRequest.isHidden());
        assertThat(item.getCode()).isEqualTo(createItemRequest.getCode());
        assertThat(item.isRevenue()).isEqualTo(createItemRequest.isRevenue());

        UpdateItemRequest updateItemRequest = UpdateItemRequest.builder()
                .name("Alternative Name")
                .hidden(true)
                .priceType(PriceType.VARIABLE)
                .price(1000L)
                .build();

        Item updatedItem = cloverApiClient.updateItem(SANDBOX_MERCHANT_ID, item.getId(), updateItemRequest);
        assertThat(updatedItem).isNotNull();
        assertThat(updatedItem.getName()).isEqualTo(updateItemRequest.getName());
        assertThat(updatedItem.getPrice()).isEqualTo(updateItemRequest.getPrice());
        assertThat(updatedItem.getPriceType()).isEqualTo(updateItemRequest.getPriceType());
        assertThat(updatedItem.isHidden()).isEqualTo(updateItemRequest.isHidden());
        assertThat(updatedItem.getCode()).isEqualTo(updateItemRequest.getCode());
        assertThat(updatedItem.isRevenue()).isEqualTo(updateItemRequest.isRevenue());

        cloverApiClient.deleteItem(SANDBOX_MERCHANT_ID, item.getId());
    }

    @Test
    public void testDeleteLoopzItems() throws CloverApiException, ExecutionException, InterruptedException {
        ItemList items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);
        String itemName = "Program";

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        List<Future> futures = new ArrayList<>();

        while (!items.getElements().isEmpty() && items.getElements().get(0).getName().contains(itemName)) {
            for (final Item item : items.getElements()) {
                if (item.getName().contains(itemName)) {
                    futures.add(executorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                cloverApiClient.deleteItem(SANDBOX_MERCHANT_ID, item.getId());
                            } catch (CloverApiException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                }
            }
            for (Future future : futures) {
                future.get();
            }
            futures.clear();
            items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);
        }
    }

    @Test
    public void testAddLineItemToOrder() throws CloverApiException {
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .state(OrderState.OPEN)
                .note("A custom note")
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);
        assertThat(order.getNote()).isNotNull();
        assertThat(order.getNote()).isEqualTo(updateOrderRequest.getNote());

        ItemList items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);

        Item cardItem = null;
        for (Item item : items.getElements()) {
            if (item.getName().contains("Loopz Test Merchant")) {
                cardItem = item;
                break;
            }
        }
        if (cardItem == null) {
            fail("Expected certain item to be present.");
        }

        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
                .item(new ItemReference(cardItem.getId()))
                .price(5000L)
                .unitQty(1L)
                .build();

        LineItem lineItem = cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);

        assertThat(lineItem.getId()).isNotNull();
    }

    @Test(expected = CloverApiException.class)
    public void testAddLineItemToOrder_UseNonExistingItem_ExpectException() throws CloverApiException {
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .state(OrderState.OPEN)
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);


        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
                .item(new ItemReference("invalid-item-id"))
                .price(5000L)
                .unitQty(1L)
                .build();

        cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);
        fail();
    }

    @Test
    public void testGetOrder() throws CloverApiException {
        Order order = cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, "83RND9HJ88KGW", OrderExpandableField.values());

        System.out.println(JsonUtils.toJSON(order));
    }

    @Test
    public void testGetItemStock() throws CloverApiException {
        ItemStock itemStock = cloverApiClient.getItemStock(SANDBOX_MERCHANT_ID, "GKJSCBD75747T");

        System.out.println(JsonUtils.toJSON(itemStock));
    }

    @Test
    public void testUpdateItemStock() throws CloverApiException {
        ItemStock itemStock = cloverApiClient.getItemStock(SANDBOX_MERCHANT_ID, "GKJSCBD75747T");

        UpdateItemStockRequest updateItemStockRequest = UpdateItemStockRequest.builder()
                .item(itemStock.getItem())
                .stockCount(4L)
                .quantity(4.0)
                .build();

        ItemStock updatedItemStock = cloverApiClient.updateItemStock(SANDBOX_MERCHANT_ID, "GKJSCBD75747T", updateItemStockRequest);
        System.out.println(JsonUtils.toJSON(updatedItemStock));
    }

    @Test
    public void testAddCustomerToOrder() throws CloverApiException {

        CustomerList customers = cloverApiClient.getCustomers(SANDBOX_MERCHANT_ID, Collections.<CustomerFilterQuery>emptyList());

        Customer customer1 = null;
        Customer customer2 = null;

        for(Customer customer : customers.getElements()) {
            if(customer.getFirstName() != null && customer.getFirstName().equalsIgnoreCase("Michael 1")) {
                customer1 = customer;
                continue;
            }
            if(customer.getFirstName() != null && customer.getFirstName().equalsIgnoreCase("Michael 2")) {
                customer2 = customer;
                continue;
            }
        }

        if (customer2 == null || customer1 == null) {
            throw new RuntimeException("Customers Not Found");
        }

        Order order = cloverApiClient.getOrder(SANDBOX_MERCHANT_ID, "4TSS35WMVPWBW");

        // Verify that the order has customer 1
        assertThat(order.getCustomers().getElements().get(0).getId()).isEqualTo(customer1.getId());

        UpdateOrderRequest updateOrderRequest = UpdateOrderRequest.builder()
                .customers(new CustomerReferenceList(Arrays.asList(new CustomerReference(customer2.getId()))))
                .build();

        order = cloverApiClient.updateOrder(SANDBOX_MERCHANT_ID, order.getId(), updateOrderRequest);

        assertThat(order.getCustomers().getElements().get(0).getId()).isEqualTo(customer2.getId());




//
//        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
//                .item(new ItemReference("invalid-item-id"))
//                .price(5000L)
//                .unitQty(1L)
//                .build();
//
//        cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);
    }

    @Test
    public void testCreateTender() throws CloverApiException {
        CreateTenderRequest createTenderRequest = CreateTenderRequest.builder()
                .editable(false)
                .enabled(true)
                .visible(false)
                .label("Store Credit")
//                .labelKey("loopz-store-credit-" + UUID.randomUUID())
                .opensCashDrawer(false)
                .supportsTipping(true)
                .instructions("Issue store credit through a gift card")
                .build();

        Tender tender = cloverApiClient.createTender(SANDBOX_MERCHANT_ID, createTenderRequest);

        assertThat(tender).isNotNull();
        System.out.println(JsonUtils.toJSON(tender));
    }


    @Test
    public void testCRUDTender() throws CloverApiException {
        CreateTenderRequest createTenderRequest = CreateTenderRequest.builder()
                .editable(false)
                .enabled(false)
                .visible(false)
                .label("Store Credit " + UUID.randomUUID())
                .opensCashDrawer(false)
                .supportsTipping(false)
                .instructions("Issue store credit through a gift card")
                .build();

        Tender tender = cloverApiClient.createTender(SANDBOX_MERCHANT_ID, createTenderRequest);

        assertThat(tender).isNotNull();
        assertThat(tender.isVisible()).isFalse();
        assertThat(tender.isEnabled()).isFalse();
        assertThat(tender.isEditable()).isFalse();

        UpdateTenderRequest updateTenderRequest = UpdateTenderRequest.builder()
                .editable(true)
                .enabled(true)
                .visible(true)
                .label("Store Credit " + UUID.randomUUID())
                .opensCashDrawer(false)
                .supportsTipping(false)
                .instructions("Issue store credit through a gift card - UPDATED")
                .build();

        Tender updatedTender = cloverApiClient.updateTender(SANDBOX_MERCHANT_ID, tender.getId(), updateTenderRequest);

        assertThat(updatedTender.getId()).isEqualTo(tender.getId());
        assertThat(tender.isVisible()).isTrue();
        assertThat(tender.isEnabled()).isTrue();
        assertThat(tender.isEditable()).isTrue();

        cloverApiClient.deleteTender(SANDBOX_MERCHANT_ID, updatedTender.getId());

        try {
            cloverApiClient.getTender(SANDBOX_MERCHANT_ID, tender.getId());
            fail("Expected not to find the tender because it's deleted.");
        } catch (CloverApiException e) {
            assertThat(e.getStatusCode()).isEqualTo(404);
        }
    }

    @Test
    public void testGetTenders() throws CloverApiException {
        TenderList tenders = cloverApiClient.getTenders(SANDBOX_MERCHANT_ID);

        assertThat(tenders).isNotNull();
    }

    @Test
    public void testGetTenders_Prod() throws CloverApiException {
        String cloverMerchantId = "KJF25QKG04311";
        String cloverAccessToken = "8690522e-7ce2-16e7-1131-b3c05785f43b";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        TenderList tenders = cloverApiClient.getTenders(cloverMerchantId);

        assertThat(tenders).isNotNull();
    }

    @Test
    public void testGetMerchant_Prod() throws CloverApiException {

//     https://dashboard.loopz.io/login?merchant_id=89ZYK2A6356D1&access_token=6b9213cc-018b-d0ba-7e21-9571110e42f1
        String cloverMerchantId = "35D5V21Q8XC01";
        String cloverAccessToken = "8544ad0e-5228-08db-51cb-e79d57916547";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        Merchant merchant = cloverApiClient.getMerchant(cloverMerchantId, MerchantExpandableField.values());

        System.out.print(JsonUtils.toJSON(merchant));
    }

    @Test
    public void testGetPaymentByExternalReferenceIdBenjamins() throws CloverApiException {
        String cloverMerchantId = "DVJ50TSWFQX91";
        String cloverAccessToken = "32ba340e-3000-3c92-a1d2-6d2126ecb1e7";
        String hash = "16f86bd37bfd8c";
        Long transactionTime = 1574376732386L;
        Long amount = 3648L;

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        List<PaymentFilterQuery> filterQueries = Arrays.asList(
//                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.GREATER_THAN_OR_EQUAL, transactionTime - 5000),
//                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.LESS_THAN_OR_EQUAL, transactionTime + 5000),
                new PaymentFilterQuery(PaymentFilterQueryField.EXTERNAL_PAYMENT_ID, QueryOperators.EQUALS, hash)
        );

        PaymentList payments = cloverApiClient.getPayments(cloverMerchantId, filterQueries, PaymentExpandableField.values());

        System.out.println(payments);
    }

    @Test
    public void testGetPaymentsFor() throws CloverApiException {
        String cloverMerchantId = "B9K03YQX2YXD1";
        String cloverAccessToken = "65d076d2-977f-7fe1-a568-a474744586cf";
        String itemId = "SP89HSYV086D2";

        Long createdFrom = DateTime.parse("2022-09-25T00:00:00.000").getMillis();
        Long createdTo = DateTime.parse("2022-09-27T23:00:00.000").getMillis();

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

//        List<PaymentFilterQuery> filterQueries = Arrays.asList(
//                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.GREATER_THAN_OR_EQUAL, createdFrom),
//                new PaymentFilterQuery(PaymentFilterQueryField.CREATED_TIME, QueryOperators.LESS_THAN_OR_EQUAL, createdTo)
//        );
//        PaymentList paymentList = cloverApiClient.getPayments(cloverMerchantId, filterQueries, PaymentExpandableField.values());



        List<OrderFilterQuery> filterQueries = Arrays.asList(
                new OrderFilterQuery(OrderFilterQueryField.CREATED_TIME, QueryOperators.GREATER_THAN_OR_EQUAL, createdFrom),
                new OrderFilterQuery(OrderFilterQueryField.CREATED_TIME, QueryOperators.LESS_THAN_OR_EQUAL, createdTo)
        );
        OrderList orders = cloverApiClient.getOrders(cloverMerchantId, filterQueries, OrderExpandableField.values());
//        OrderList orders = cloverApiClient.getOrders(cloverMerchantId);

        List<Order> ordersContainingSingleItem = new ArrayList<>();

        for(Order order : orders.getElements()) {
            if (order.getLineItems() != null) {
                for(LineItem lineItem : order.getLineItems().getElements()) {
                    if(lineItem != null && lineItem.getItem() != null && lineItem.getItem().getId().equals(itemId)) {
                        ordersContainingSingleItem.add(order);
                        break;
                    }
                }
            }
        }

        System.out.println("Total Orders Found: " + orders.getElements().size());
        System.out.println("Total Gift Card Orders Found: " + ordersContainingSingleItem.size());

        for (Order order : ordersContainingSingleItem) {
            Payment payment = order.getPayments().getElements().get(0);

            payment = cloverApiClient.getPayment(cloverMerchantId, payment.getId(), PaymentExpandableField.values());

            String paymentMethod = "";
            if(payment.getCardTransactionInfo() != null) {
                paymentMethod = payment.getCardTransactionInfo().getCardType() + " " + payment.getCardTransactionInfo().getLast4();
            } else {
                Tender tender = cloverApiClient.getTender(cloverMerchantId, payment.getTender().getId());
                paymentMethod = tender.getLabel();
            }

            System.out.println("Payment [ID=" + payment.getId() + ", amount=" + payment.getAmount() + ", paymentMethod=" + paymentMethod + " ]");
            System.out.println("Order [ID=" + order.getId() + ", lineitems=" + order.getLineItems().getElements().size() + ", amount=" + order.getTotal() + ", date=" + order.getCreatedTime().toString() + "]");
            System.out.println("-------------------------------");
        }

//        System.out.println(JsonUtils.toJSON(orders));
    }

    @Test
    public void testGetCloverMeteredBillingEventsForMerchant() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");
        String cloverMerchantId = "5H4R902HW5C41";
        String cloverAccessToken = "d4db2539-fab2-ecd8-5c33-72e0cd13c7e2";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        MeteredBillingEventList meteredBillingEvents = cloverApiClient.getMeteredBillingEvents(cloverMerchantId, "HCYM5R090ZN24");

        assertThat(meteredBillingEvents).isNotNull();
    }


    @Test
    public void testListDevices() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");

        String cloverMerchantId = "27WR85766T0M1";
        String cloverAccessToken = "e7e6523a-a341-ea89-9430-f37fb0aaf75e";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        DeviceList deviceList = cloverApiClient.listDevices(cloverMerchantId);

        System.out.println(JsonUtils.toJSON(deviceList));
    }

    @Test
    public void testUpdatePaymentEmployee() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");

        String cloverMerchantId = "BW6WRQMAC9PD1";
        String cloverAccessToken = "ddf789dd-e636-9f58-5a7a-d63032c93567";

        String cloverTenderId = "4ZJA4X09NM5CW";
        String cloverOrderId = "MW1MC89TW4YAC";
        String cloverPaymentId = "XYPS0FV3APHKT";
        String newEmployeeId = "524JKPK9MW79T";
        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        Order order = cloverApiClient.getOrder(cloverMerchantId, cloverOrderId, OrderExpandableField.PAYMENTS, OrderExpandableField.CREDITS);

        UpdatePaymentEmployeeRequest updatePaymentEmployeeRequest = new UpdatePaymentEmployeeRequest(new OrderReference(cloverOrderId), new EmployeeReference(newEmployeeId));

        Payment payment = cloverApiClient.updatePaymentEmployee(cloverMerchantId, cloverPaymentId, updatePaymentEmployeeRequest);

        payment = cloverApiClient.getPayment(cloverMerchantId, cloverPaymentId, PaymentExpandableField.values());

        System.out.println(JsonUtils.toJSON(payment));
    }

    @Test
    public void testGetOrder_Prod() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");

        String cloverMerchantId = "4JKNCSDBDDB3E";
        String cloverAccessToken = "ee8f8a20-dc7f-82d8-f569-dfc3841f96bf";
        String cloverOrderId = "3PPA4DXQYHVHR";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        Order order = cloverApiClient.getOrder(cloverMerchantId, cloverOrderId, OrderExpandableField.values());

        System.out.println(JsonUtils.toJSON(order));
    }

    @Test
    public void testGetOrderTypes_Prod() throws CloverApiException {
        CloverApi.setAppId("9G7WJV36Z1TET");

        String cloverMerchantId = "4JKNCSDBDDB3E";
        String cloverAccessToken = "ee8f8a20-dc7f-82d8-f569-dfc3841f96bf";
        String cloverOrderId = "3PPA4DXQYHVHR";

        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        OrderTypeList orderTypes = cloverApiClient.getOrderTypes(cloverMerchantId, OrderTypeExpandableField.values());

        System.out.println(JsonUtils.toJSON(orderTypes));
    }

    @Test
    public void testCancelMeteredBillingIds() {
//        String meteredIds = "G925NHVWDMKPG\n" +
//                "TF6DKYDMXEE88\n" +
//                "66R8X4Z7BB7NM\n" +
//                "76RYK6PQDE85R\n" +
//                "453QN9NP3BV0Y\n" +
//                "S9PM6VGTWA1VE\n" +
//                "4WZJE5ZXT0MSP\n" +
//                "HV9NCZD6F5V8R\n" +
//                "5V2TR7QCXNA3J\n" +
//                "1713ME714QH3C\n" +
//                "QAPVSN63M5H8R\n" +
//                "S40BKHZ9Q7HBY\n" +
//                "SA7XWK7YKA8ZJ\n" +
//                "W0A7VKWQ65K08\n" +
//                "PZK280BY6XTA2\n" +
//                "KJHQR0KRPS5M8\n" +
//                "Y4P5T82EWCRXA\n" +
//                "5W7Y82KX4WAJP\n" +
//                "F9D252ZY85S50\n" +
//                "63HB0ZKDTJGTP\n" +
//                "F8CMNXY9HAGV8\n" +
//                "NSEA12NR39EER\n" +
//                "PNB2SJ2F2599W\n" +
//                "BJKTB820110ZA\n" +
//                "54GKYQSD77RWJ\n" +
//                "R82Q65TH2682C\n" +
//                "QP8JKCJHK4VVW\n" +
//                "X0NDSZ405A9W4\n" +
//                "H57FNZ7JTEJBP\n" +
//                "2Y9HV3XP53P0J\n" +
//                "QJ497NKGMN7CE\n" +
//                "60TPTME3GKVPC\n" +
//                "4YNC442YKGCYC\n" +
//                "GV0NQKE37K74M\n" +
//                "122H7FTR7SNHT\n" +
//                "YP1W60SA3NS0E\n" +
//                "XP317W77BGGY0\n" +
//                "AA60QA6YXNEDY\n" +
//                "S3J90024W5GQ4\n" +
//                "4E51SKS7DQ39W\n" +
//                "R9ZEPZD27ZNZW\n" +
//                "K9E3QQKGSVGVG\n" +
//                "KP5TJ71P361TG\n" +
//                "H8C8WR8V0QCMT\n" +
//                "Z3385YW4E1TPE\n" +
//                "1SAYP953DPPFA\n" +
//                "ZZQ1FF24EDA98\n" +
//                "C0EGGKGRN8ZF2\n" +
//                "YDNYW5EXG4Z24\n" +
//                "NH1QZKF6WFR88\n" +
//                "25TAV7Y579Z4A\n" +
//                "25PGM5XQVDX1P\n" +
//                "P0SB63P3F79A8\n" +
//                "MHRZN1PMT4YT4\n" +
//                "F6ZKR6T3QQYH2\n" +
//                "V4268XNFDBN18\n" +
//                "PTNEX921P7SF0\n" +
//                "YR4DBE8Y9QMN4\n" +
//                "C8AYPSNEX227C\n" +
//                "D4ZKB8Y1CVD8C\n" +
//                "DDGPDVR5AMTHY\n" +
//                "EG4CT59C90EFE\n" +
//                "CNTT07D9Q2HQC\n" +
//                "5G8HZ1J34KED8\n" +
//                "MPQH5YGG2AWBT\n" +
//                "S6F1J98WTRE00\n" +
//                "14GP6VBKKT8PT\n" +
//                "DMEFP1KYDK0V6\n" +
//                "YKCPVYAZR1VDT\n" +
//                "T0FTA992SQZ5W\n" +
//                "0MBF0TQFK594T\n" +
//                "BRSFVDYAEX0HW\n" +
//                "Q9TG8R4MNDWPR\n" +
//                "6YZN9YMP32XPE\n" +
//                "8Z6S6R1J23HDE\n" +
//                "JXE0879Y6NS2C\n" +
//                "JHY4JQY224VAT\n" +
//                "8RAGB2JK8BA18\n" +
//                "FNMXGEZVXDJKW\n" +
//                "HWZRACP9REHYE\n" +
//                "BWPJ30ME1DPFW\n" +
//                "VBDJ31AG7G7PE\n" +
//                "X7JEKAAGKJXYR\n" +
//                "ZHGN4PXXCZC7Y\n" +
//                "M8ZHMA9FYKC58\n" +
//                "9FV4J7MF7AMAJ\n" +
//                "KD9VSAGFQ4QSY\n" +
//                "RPHXCKM3G7K1A\n" +
//                "ZF08CNRAZZMMA\n" +
//                "VFQGA4VB0WDX0\n" +
//                "8701S347TK5YR\n" +
//                "PPREYRHYQBG8G\n" +
//                "FTSH9DAYZ5M98\n" +
//                "VJE1H5C24AE4M\n" +
//                "NS16BTH4DYYC0\n" +
//                "9730MY8MXB23Y\n" +
//                "CDRWYF7Q246QY\n" +
//                "RD55N5DAJ3TDY\n" +
//                "HG7CW0HB8Z48R\n" +
//                "CP74KBEVQQVMW\n" +
//                "3K6QM0DG5CFCR\n" +
//                "5S221JVAQX056\n" +
//                "D7BVXK10P1YDM\n" +
//                "A1C8GXDCGEFE8\n" +
//                "R1E9DPKFJYXDJ\n" +
//                "9W2N6DP9S87XP\n" +
//                "HYZGYN4C5JV6E\n" +
//                "11S7Y5X31NV2M\n" +
//                "59BQ7TTCEV588\n" +
//                "6RMPA33JS8JNR\n" +
//                "CHE36PWBYKZKW\n" +
//                "47XRPN64ZG2R4\n" +
//                "Z4STPY6GCAH2C\n" +
//                "DPPHPBSK8J3EE\n" +
//                "85CDVF8V7GGW0\n" +
//                "79DFKGBFYHHAG\n" +
//                "DENNREHQ62B8G\n" +
//                "VYRMXKVTVTR48\n" +
//                "4KS9PH3534ATE\n" +
//                "CXQH1V2KG7F4E\n" +
//                "N85V5DMQ4EYR2\n" +
//                "8H57ZK4B4TDQR\n" +
//                "4X9YMN66F4KRJ\n" +
//                "7VJSGBMFY9HS8\n" +
//                "NM0NNK32ZQJBY\n" +
//                "NS6JN66CSAH58\n" +
//                "A0AD8S9WDBYGG\n" +
//                "BMM7Z2ZT32BN6\n" +
//                "XJ1Z5WYRV08X8\n" +
//                "MHTG11J7A2ZBT\n" +
//                "5WTC17X81MH92\n" +
//                "891PFT00QH2GY\n" +
//                "MG53B01C4QGJM\n" +
//                "8QJ744FNTH0E4\n" +
//                "7GWKG5MQD5N00\n" +
//                "WT2G7CEYADQ0P\n" +
//                "KQRTKNHS40F0E\n" +
//                "F11CDBJ2C9T4Y\n" +
//                "ZQE1YH94RR4GE\n" +
//                "2KJESBAA81R0R\n" +
//                "XPQ6HQTP4W84P\n" +
//                "HRA41KY9XG17C\n" +
//                "TRVS4RA4PCMCJ\n" +
//                "K6QPYPWCD0WW8\n" +
//                "VRN96N0K3CCZA\n" +
//                "954ZD06FP53AT\n" +
//                "3GE7PHRMW5SD4\n" +
//                "KNZZNG9QAVP36\n" +
//                "JW4M4QX27E5MM\n" +
//                "11DX52DRJV5SM\n" +
//                "TCZNF11NHGQHP\n" +
//                "WMVWP26YC0TMJ\n" +
//                "W76KSAWMVN19M\n" +
//                "4KTTC5Q7W21KY\n" +
//                "2HYKZ3K6HNDRC\n" +
//                "C9MTR9PYBCCJA\n" +
//                "901AHC1MGCNX8\n" +
//                "NKAP9GZQ7HWH4\n" +
//                "R8WZHP8E7W65P\n" +
//                "45YHRNKTJ0TCW";
//
//        String[] meteredIdsList = meteredIds.split("\\n");
//
//        String cloverMerchantId = "XMPP9QXK1KF8J";
//        String cloverAccessToken = "83c6cdc2-7d9d-08f9-6434-3fa9213b0013";
//
//        CloverApiClient cloverApiClient = new CloverApiClient(CloverApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));
//
//        CloverApi.setAppId("9G7WJV36Z1TET");
//
//        for (String meteredId : meteredIdsList) {
//            try {
//                cloverApiClient.deleteMeteredBillingEvent(cloverMerchantId, "HCYM5R090ZN24", meteredId);
//
////                System.out.println(meteredBillingEvent.getCount());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
    }
}
