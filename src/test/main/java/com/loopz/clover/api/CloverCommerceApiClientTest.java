package com.loopz.clover.api;

import com.loopz.clover.api.client.*;
import com.loopz.clover.api.model.apikey.ApiKey;
import com.loopz.clover.api.model.ecommerce.charge.*;
import com.loopz.clover.api.model.ecommerce.order.PayForOrderRequest;
import com.loopz.clover.api.model.employee.EmployeeReference;
import com.loopz.clover.api.model.item.Item;
import com.loopz.clover.api.model.item.ItemList;
import com.loopz.clover.api.model.item.ItemReference;
import com.loopz.clover.api.model.lineitem.AddLineItemRequest;
import com.loopz.clover.api.model.lineitem.LineItem;
import com.loopz.clover.api.model.order.Order;
import com.loopz.clover.api.model.order.OrderState;
import com.loopz.clover.api.model.order.UpdateOrderRequest;
import com.loopz.clover.api.model.payment.OrderPaymentRequest;
import com.loopz.clover.api.model.token.Card;
import com.loopz.clover.api.model.token.CreateTokenRequest;
import com.loopz.clover.api.model.token.TokenizedCard;
import com.loopz.clover.api.util.JsonUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.loopz.clover.api.CloverTestConstants.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.fail;

public class CloverCommerceApiClientTest {

    private CloverApiClient cloverApiClient;
    private CloverTokenApiClient cloverTokenApiClient;
    private CloverCommerceApiClient cloverCommerceApiClient;

    @BeforeClass
    public static void setupClass() {
        CloverApi.setApiToken(SANDBOX_API_TOKEN);
        CloverApi.setAppId(SANDBOX_APP_ID);
        CloverApi.setMeteredId(SANDBOX_METERED_ID);
    }

    @Before
    public void setup() throws CloverApiException {
        cloverApiClient = new CloverApiClient(CloverApiUrls.SANDBOX, new ApiTokenAuthenticationStrategy(SANDBOX_ACCESS_TOKEN));

//        ApiKey apiKey = cloverApiClient.getApiKey(SANDBOX_MERCHANT_ID);
//
//        cloverTokenApiClient = new CloverTokenApiClient(CloverTokenApiUrls.SANDBOX, new ApiKeyAuthenticationStrategy(apiKey.getApiKey()));
        cloverCommerceApiClient = new CloverCommerceApiClient(CloverCommerceApiUrls.SANDBOX, new ApiTokenAuthenticationStrategy(SANDBOX_ACCESS_TOKEN));
    }

    @Test
    public void testPayForOrder() throws CloverApiException, InterruptedException {
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        ItemList items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);

        Item cardItem = null;
        for (Item item : items.getElements()) {
            if (item.getName().contains("Loopz Test Merchant")) {
                cardItem = item;
                break;
            }
        }
        if (cardItem == null) {
            fail("Expected certain item to be present.");
        }

        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
                .item(new ItemReference(cardItem.getId()))
                .price(5000L)
                .unitQty(1L)
                .build();

        LineItem lineItem = cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);

        String cardNumber = "6011361000006668";
        String first6 = cardNumber.substring(0, 6);
        String last4 = cardNumber.substring(cardNumber.length() -4, cardNumber.length());

        Card card = Card.builder()
                .number(cardNumber)
                .cvv("123")
                .expMonth("12")
                .expYear("2022")
                .last4(last4)
                .first6(first6)
                .brand("DISCOVER")
                .build();

        TokenizedCard tokenizedCard = cloverTokenApiClient.createToken(new CreateTokenRequest(card));

        PayForOrderRequest payForOrderRequest = PayForOrderRequest.builder()
                .source(tokenizedCard.getId())
                .inputType(ChargeInputType.CUSTOMER)
                .email("michael@loopz.io")
                .build();

        com.loopz.clover.api.model.ecommerce.order.Order paidOrder = cloverCommerceApiClient.payForOrder(order.getId(), payForOrderRequest);

        assertThat(paidOrder).isNotNull();

        String chargeId = paidOrder.getChargeId();
        Thread.sleep(2000);

        Charge charge = cloverCommerceApiClient.getCharge(chargeId, ChargeExpandableField.values());

        assertThat(charge).isNotNull();
        assertThat(charge.getOrderId()).isEqualTo(paidOrder.getId());

    }

    @Test
    public void testPayForOrderWithDeclinedCard() throws CloverApiException {
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        ItemList items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);

        Item cardItem = null;
        for (Item item : items.getElements()) {
            if (item.getName().contains("Loopz Test Merchant")) {
                cardItem = item;
                break;
            }
        }
        if (cardItem == null) {
            fail("Expected certain item to be present.");
        }

        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
                .item(new ItemReference(cardItem.getId()))
                .price(5000L)
                .unitQty(1L)
                .build();

        LineItem lineItem = cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);

        String cardNumber = "4005571702222222";
        String first6 = cardNumber.substring(0, 6);
        String last4 = cardNumber.substring(cardNumber.length() -4, cardNumber.length());

        Card card = Card.builder()
                .number(cardNumber)
                .cvv("123")
                .expMonth("12")
                .expYear("2022")
                .last4(last4)
                .first6(first6)
                .brand("VISA")
                .build();

        TokenizedCard tokenizedCard = cloverTokenApiClient.createToken(new CreateTokenRequest(card));

        PayForOrderRequest payForOrderRequest = PayForOrderRequest.builder()
                .source(tokenizedCard.getId())
                .inputType(ChargeInputType.CUSTOMER)
                .email("michael@loopz.io")
                .build();

        try {
            cloverCommerceApiClient.payForOrder(order.getId(), payForOrderRequest);
            fail("Expected exception to be thrown.");
        } catch(CloverApiException e) {
            assertThat(e.getStatusCode()).isEqualTo(402);
            assertThat(e.getErrorResponse().getCommerceErrorResponse().getCode()).isEqualTo("card_declined");
            assertThat(e.getErrorResponse().getCommerceErrorResponse().getDeclineCode()).isEqualTo("issuer_declined");
        }
    }

    @Test
    public void testChargeCardAndAddPaymentToOrder() throws CloverApiException{
        Order order = cloverApiClient.createOrder(SANDBOX_MERCHANT_ID);

        ItemList items = cloverApiClient.getItems(SANDBOX_MERCHANT_ID);

        Item cardItem = null;
        for (Item item : items.getElements()) {
            if (item.getName().contains("Loopz Test Merchant")) {
                cardItem = item;
                break;
            }
        }
        if (cardItem == null) {
            fail("Expected certain item to be present.");
        }

        AddLineItemRequest addLineItemRequest = AddLineItemRequest.builder()
                .item(new ItemReference(cardItem.getId()))
                .price(5000L)
                .unitQty(1L)
                .build();

        LineItem lineItem = cloverApiClient.addLineItemToOrder(SANDBOX_MERCHANT_ID, order.getId(), addLineItemRequest);

        String cardNumber = "6011361000006668";
        String first6 = cardNumber.substring(0, 6);
        String last4 = cardNumber.substring(cardNumber.length() -4, cardNumber.length());

        Card card = Card.builder()
                .number(cardNumber)
                .cvv("123")
                .expMonth("12")
                .expYear("2022")
                .last4(last4)
                .first6(first6)
                .brand("DISCOVER")
                .build();

        TokenizedCard tokenizedCard = cloverTokenApiClient.createToken(new CreateTokenRequest(card));

        CreateChargeRequest createChargeRequest = CreateChargeRequest.builder()
                .source(tokenizedCard.getId())
                .inputType(ChargeInputType.CUSTOMER)
                .receiptEmail("michael@loopz.io")
                .currency("USD")
                .capture(true)
                .taxAmount(0)
                .tipAmount(0)
                .amount(1000L)
                .build();

        Charge charge = cloverCommerceApiClient.createCharge(createChargeRequest);

        assertThat(charge).isNotNull();

    }

    @Test
    public void testGetCharge_Dev() throws CloverApiException {
        String chargeId = "WJT1F1NS0KTQA";

        Charge charge = cloverCommerceApiClient.getCharge(chargeId, ChargeExpandableField.values());

        System.out.println(JsonUtils.toJSON(charge));
    }

    @Test
    public void testGetCharge_Prod() throws CloverApiException {
        String cloverMerchantId = "9TR93SHVQN1S1";
        String cloverAccessToken = "5c545d28-09c4-8cc3-9226-415f374df0d7";
        String chargeId = "TKEYM6RYGNQBA";

        CloverCommerceApiClient cloverCommerceApiClientProd = new CloverCommerceApiClient(CloverCommerceApiUrls.PRODUCTION_US, new ApiTokenAuthenticationStrategy(cloverAccessToken));

        CloverApi.setAppId("9G7WJV36Z1TET");

        Charge charge = cloverCommerceApiClientProd.getCharge(chargeId, ChargeExpandableField.values());

        System.out.println(JsonUtils.toJSON(charge));
    }
}
