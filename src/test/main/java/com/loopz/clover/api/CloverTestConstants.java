package com.loopz.clover.api;

public class CloverTestConstants {

    // Do an oauth flow in the Loopz app on a Clover device and capture the access token
    public static final String SANDBOX_ACCESS_TOKEN = "b5a2686b-a127-6304-9703-66b92757c3eb";
    public static final String SANDBOX_API_TOKEN = "8f523017-b453-9057-2ca4-676040aad975";
    public static final String SANDBOX_MERCHANT_ID = "R1NESZ8955CV1";
    public static final String SANDBOX_APP_ID = "92Q7216ZWK66J";
    public static final String SANDBOX_METERED_ID = "B9M6CSYFWE0J8";

}
